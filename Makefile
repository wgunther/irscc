objects = \
	src/lexer.o src/parser.o \
	src/assemble.o src/ast.o src/compiler.o src/formdesc.o src/llist.o \
		src/rbtmap.o src/semantics.o src/stringset.o src/symtab.o \
		src/translate.o src/utility.o \
	src/latexConstants.o src/randomThings.o src/formElements.o src/controller.o src/main.o

irscc : $(objects)
	$(CXX) $(CXXFLAGS) -o $@ $(objects) -lm

src/irscc.tab.c src/irscc.tab.h : src/irscc.y
	cd src; bison -d irscc.y

src/irscc.lex.c : src/irscc.l
	flex -o $@ src/irscc.l

src/lexer.o : src/irscc.lex.c src/ast.h src/irscc.tab.h src/utility.h
	$(CC) $(CFLAGS) -c -o $@ src/irscc.lex.c

src/parser.o : src/irscc.tab.c src/irscc.tab.h \
		src/ast.h src/compiler.h src/llist.h src/semantics.h src/translate.h \
		src/utility.h
	$(CC) $(CFLAGS) -c -o $@ src/irscc.tab.c

src/assemble.o : src/assemble.c src/assemble.h src/formdesc.h src/llist.h \
		src/symtab.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/assemble.c

src/ast.o : src/ast.c src/ast.h src/llist.h src/stringset.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/ast.c

src/compiler.o : src/compiler.c src/assemble.h src/compiler.h src/formdesc.h \
		src/llist.h src/semantics.h src/translate.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/compiler.c

src/formdesc.o : src/formdesc.c src/formdesc.h src/llist.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/formdesc.c

src/llist.o : src/llist.c src/formdesc.h src/llist.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/llist.c

src/rbtmap.o : src/rbtmap.c src/rbtmap.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/rbtmap.c

src/semantics.o : src/semantics.c src/ast.h src/llist.h src/semantics.h \
		src/stringset.h src/symtab.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/semantics.c

src/stringset.o : src/stringset.c src/rbtmap.h src/stringset.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/stringset.c

src/symtab.o : src/symtab.c src/ast.h src/formdesc.h src/llist.h src/rbtmap.h \
		src/symtab.h src/translate.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/symtab.c

src/translate.o : src/translate.c src/ast.h src/formdesc.h src/llist.h \
		src/stringset.h src/symtab.h src/translate.h src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/translate.c

src/utility.o : src/utility.c src/utility.h
	$(CC) $(CFLAGS) -std=c99 -c -o $@ src/utility.c

src/latexConstants.cc : src/header.tex src/generateHeader.pl
	perl src/generateHeader.pl

src/latexConstants.o: src/latexConstants.cc src/latexConstants.hh
	$(CXX) $(CXXFLAGS) -std=c++11 -c -o $@ src/latexConstants.cc

src/randomThings.cc : src/words.txt src/randomThings.hh src/generateRandomThings.pl
	perl src/generateRandomThings.pl

src/formElements.o: src/formElements.cc src/formElements.hh src/formdesc.h src/randomThings.hh 
	$(CXX) $(CXXFLAGS) -std=c++11 -c -o $@ src/formElements.cc

src/randomThings.o: src/randomThings.cc src/randomThings.hh
	$(CXX) $(CXXFLAGS) -std=c++11 -c -o $@ src/randomThings.cc

src/main.o: src/main.cc src/formElements.hh src/latexConstants.hh src/controller.hh src/formdesc.h src/compiler.h src/llist.h
	$(CXX) $(CXXFLAGS) -std=c++11 -c -o $@ src/main.cc

src/controller.o: src/controller.cc src/controller.hh
	$(CXX) $(CXXFLAGS) -std=c++11 -c -o $@ src/controller.cc

.PHONY: clean
clean:
	rm -f src/*.o src/irscc.lex.c src/irscc.tab.c src/irscc.tab.h src/latexConstants.cc src/randomThings.cc irscc
