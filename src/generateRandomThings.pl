#!/usr/bin/env perl
use strict;
my $IN_FILENAME = 'src/words.txt';
my $OUT_FILENAME = 'src/randomThings.cc';


my @type = qw/qualifier attrib type/;
my $mode = 0;
my @accumulate;

open WORDS, $IN_FILENAME or die "Cannot open words file $IN_FILENAME $!\n";
open (CPPOUTFILE, '>', $OUT_FILENAME) or die "Cannot open output file\n";
print CPPOUTFILE "#include \"randomThings.hh\"\n";
foreach (<WORDS>) {
	chomp;
	next if /^"/;
	if (/^$/) {
		local $, = ", ";
		print CPPOUTFILE "std::vector<std::string> " . $type[$mode] . "{";
		print CPPOUTFILE @accumulate;
		print CPPOUTFILE "};\n";
		@accumulate = ();
		$mode++;
	}
	else {
		push @accumulate, '"' . lc($_) . '"';
	}
}

print CPPOUTFILE <<'END';
bool biasedCoin (double probability) {
	double roll = (double)(rand()%100000)/100000.0;
	return roll > probability ? false : true;
}
std::string capitalizeFirst (std::string s) {
	s[0] = std::toupper(s[0]);
	return s;
}
std::string makeRandomLabel() {
	shuffle(qualifier);
	double probability = useQualifier;
	int i = 0;
	std::string label{""};
	label.reserve(30);
	while (biasedCoin(probability)) {
		label.append(qualifier[i++]);
		label.append(" ");
		probability *= additionalQualifier;
	}
	label.append(getRandom(attrib));
	label.append(" ");
	label.append(getRandom(type));
	return label;
}

std::string makeRandomSectionTitle() {
	std::string label{""};
	label.reserve(30);
	label.append(capitalizeFirst(getRandom(attrib)));
	label.append(" ");
	label.append(capitalizeFirst(getRandom(type)));
	return label;
}

std::string makeRandomFormTitle() {
	shuffle(qualifier);
	double probability = useQualifier;
	int i = 0;
	std::string label{""};
	label.reserve(30);
	while (biasedCoin(probability)) {
		label.append(capitalizeFirst(qualifier[i++]));
		label.append(" ");
		probability *= additionalQualifier;
	}
	label.append(capitalizeFirst(getRandom(attrib)));
	label.append(" ");
	label.append(capitalizeFirst(getRandom(type)));
	return label;
}

END

close WORDS;
close CPPOUTFILE;
