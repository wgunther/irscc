#!/usr/bin/env perl
use strict;
use warnings;

my $IN_FILENAME = 'src/header.tex';
my $OUT_FILENAME = 'src/latexConstants.cc';



open (HEADERFILE, $IN_FILENAME) or die "Cannot open header file\n";
open (CPPOUTFILE, '>', $OUT_FILENAME) or die "Cannot open output file\n";
print CPPOUTFILE "#include \"latexConstants.hh\"\nstatic const std::string headerMacros =R\"(";

while (<HEADERFILE>) {
	chomp($_);
	last if $_ eq '%% CUT %%';
	print CPPOUTFILE $_, "\n";
}

print CPPOUTFILE ")\";\n\n";

print CPPOUTFILE <<'END';
auto latexConstant::headerOut (std::ostream& out) -> std::ostream& {
	out << headerMacros << std::endl;
	return out;
}
auto latexConstant::footerOut (std::ostream& out) -> std::ostream& {
	out << R"(\newFormBegin=1\bye)";
	return out;
}
END

close(CPPOUTFILE);
close(HEADERFILE);
