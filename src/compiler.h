#ifndef IRSCC_COMPILER_H
#define IRSCC_COMPILER_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdio.h>
#include "formdesc.h"

extern unsigned int identifier_hash;

extern struct linked_list *current_translation_unit;

/* returns a linked list of pointers to form_desc objects (see formdesc.h) */
struct linked_list *compile(FILE *file, int verbose);

void add_id_to_hash(const char *id);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_COMPILER_H */
