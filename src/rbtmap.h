/*	Red-black tree implementation of a map.
    Brian Kell, January/June 2013, March 2014.

	Each node has a color, red or black. "External nodes" (conceptually,
	sentinel nodes representing empty subtrees) are black. An edge to a black
	node is called a black edge. The black-length of a path is the number of
	black edges on that path. A path from a specified node to an external node
	is called an external path for the specified node. A red-black tree
	satisfies the following conditions:
	1.	The root is black.
	2.	No red node has a red child.
	3.	The black-length of all external paths from a given node u is the same;
		this value is called the black-height of u.
	
	In drawings of trees below, Black nodes, including those that are possibly
	null, are in [Brackets], and Red nodes are in (Round parentheses). Nodes of
	uncertain color are in {braces}. Red nodes are generally drawn on the same
	horizontal level as their parents, so that black-height is shown
	graphically.

	References:
	*	Baase and Van Gelder, Computer Algorithms, third edition, section 6.4.
	*	Cormen, Leiserson, Rivest, and Stein, Introduction to Algorithms, third
		edition, chapter 13.
*/

#ifndef RBTMAP_H
#define RBTMAP_H

#if defined(__cplusplus)
extern "C" {
#endif

struct rbtmap;
struct rbtmap_iter;

struct rbtmap_entry {
	void *key;
	void *value;
};

struct rbtmap *rbtmap_new(int (*compare)(const void *, const void *));
void rbtmap_free(struct rbtmap *rbtmap, void (*cleanup)(void *, void *));
void rbtmap_put(struct rbtmap *rbtmap, void *key, void *value,
		void (*cleanup)(void *, void *));
void *rbtmap_get(struct rbtmap *rbtmap, const void *key);
void rbtmap_delete(struct rbtmap *rbtmap, void *key,
		void (*cleanup)(void *, void *));
int rbtmap_size(struct rbtmap *rbtmap);
struct rbtmap_iter *rbtmap_iterator(struct rbtmap *rbtmap);
int rbtmap_has_next(struct rbtmap_iter *iter);
struct rbtmap_entry rbtmap_next(struct rbtmap_iter *iter);
void rbtmap_iterator_free(struct rbtmap_iter *iter);

#if defined(__cplusplus)
}
#endif

#endif  /* RBTMAP_H */
