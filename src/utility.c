#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utility.h"

void *checked_malloc(size_t size)
{
	void *ptr = malloc(size);
	if (ptr == 0) { fprintf(stderr, "Out of memory\n");  exit(EXIT_FAILURE); }
	return ptr;
}

char *copy_string(const char *s)
{
	assert(s != 0);  if (s == 0)  return 0;
	char *copy = checked_malloc(strlen(s) + 1);
	strcpy(copy, s);
	return copy;
}
