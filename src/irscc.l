%option noyywrap nodefault yylineno

%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "compiler.h"
#include "irscc.tab.h"
#include "utility.h"

void yyerror(char *s, ...);  /* defined in irscc.y */

%}

%x COMMENT

ISUFFIX ([Uu][Ll][Ll]?|[Ll][Ll]?[Uu])
EXP     ([Ee][-+]?[0-9]+)
UCN     (\\u[0-9a-fA-F]{4}|\\U[0-9a-fA-F]{8})

%%

 /* comments */
"/*"           { BEGIN(COMMENT); }
<COMMENT>"*/"  { BEGIN(INITIAL); }
<COMMENT>([^*]|\n)+|.
<COMMENT><<EOF>> {
		fprintf(stderr, "Unterminated comment\n");
		exit(EXIT_FAILURE);
	}
"//".*\n

 /* sophisticated handling of preprocessor directives :-) */
"#".*\n

 /* single-character tokens */
"!" |
"%" |
"&" |
"(" |
")" |
"*" |
"+" |
"," |
"-" |
"." |
"/" |
":" |
";" |
"<" |
"=" |
">" |
"?" |
"[" |
"]" |
"^" |
"{" |
"|" |
"}" |
"~"   { return yytext[0]; }

 /* multi-character operators */
"&&"  { return TOK_AND_AND;        }
"&="  { return TOK_AND_EQUALS;     }
"->"  { return TOK_ARROW;          }
"..." { return TOK_ELLIPSIS;       }
"=="  { return TOK_EQUALS_EQUALS;  }
">="  { return TOK_GREATER_EQUALS; }
"<="  { return TOK_LESS_EQUALS;    }
"<<"  { return TOK_LSHIFT;         }
"<<=" { return TOK_LSHIFT_EQUALS;  }
"-="  { return TOK_MINUS_EQUALS;   }
"--"  { return TOK_MINUS_MINUS;    }
"%="  { return TOK_MODULO_EQUALS;  }
"!="  { return TOK_NOT_EQUALS;     }
"|="  { return TOK_OR_EQUALS;      }
"||"  { return TOK_OR_OR;          }
"+="  { return TOK_PLUS_EQUALS;    }
"++"  { return TOK_PLUS_PLUS;      }
">>"  { return TOK_RSHIFT;         }
">>=" { return TOK_RSHIFT_EQUALS;  }
"/="  { return TOK_SLASH_EQUALS;   }
"*="  { return TOK_STAR_EQUALS;    }
"^="  { return TOK_XOR_EQUALS;     }

 /* keywords */
"auto"     { return TOK_AUTO;     }
"break"    { return TOK_BREAK;    }
"case"     { return TOK_CASE;     }
"char"     { return TOK_CHAR;     }
"const"    { return TOK_CONST;    }
"continue" { return TOK_CONTINUE; }
"default"  { return TOK_DEFAULT;  }
"do"       { return TOK_DO;       }
"double"   { return TOK_DOUBLE;   }
"else"     { return TOK_ELSE;     }
"enum"     { return TOK_ENUM;     }
"extern"   { return TOK_EXTERN;   }
"float"    { return TOK_FLOAT;    }
"for"      { return TOK_FOR;      }
"goto"     { return TOK_GOTO;     }
"if"       { return TOK_IF;       }
"int"      { return TOK_INT;      }
"long"     { return TOK_LONG;     }
"register" { return TOK_REGISTER; }
"return"   { return TOK_RETURN;   }
"short"    { return TOK_SHORT;    }
"signed"   { return TOK_SIGNED;   }
"sizeof"   { return TOK_SIZEOF;   }
"static"   { return TOK_STATIC;   }
"struct"   { return TOK_STRUCT;   }
"switch"   { return TOK_SWITCH;   }
"typedef"  { return TOK_TYPEDEF;  }
"union"    { return TOK_UNION;    }
"unsigned" { return TOK_UNSIGNED; }
"void"     { return TOK_VOID;     }
"volatile" { return TOK_VOLATILE; }
"while"    { return TOK_WHILE;    }

 /* integer constants */
0[0-7]*{ISUFFIX}? |
[1-9][0-9]*{ISUFFIX}? |
0[Xx][0-9A-Fa-f]+{ISUFFIX}? {
		yylval.integer = strtoul(yytext, (char **)0, 0);
		return TOK_INTEGER_CONSTANT;
	}

 /* decimal floating-point constants */
([0-9]*\.[0-9]+|[0-9]+\.){EXP}?[FLfl]? |
[0-9]+{EXP}[FLfl]? |
 /* hexadecimal floating-point constants */
0[Xx]([0-9A-Fa-f]*\.[0-9A-Fa-f]+|[0-9A-Fa-f]+\.?)[Pp][-+]?[0-9]+[FLfl]? {
		yylval.floating = strtod(yytext, (char **)0);
		return TOK_FLOATING_CONSTANT;
	}

 /* character constant */
\'([^'\\]|\\['"?\\abfnrtv]|\\[0-7]{1,3}|\\[Xx][0-9a-fA-F]+|{UCN})+\' {
		/* TODO */
		return TOK_CHARACTER_CONSTANT;
	}

 /* string literal */
L?\"([^"\\]|\\['"?\\abfnrtv]|\\[0-7]{1,3}|\\[Xx][0-9a-fA-F]+|{UCN})*\" {
		/* TODO */
		return TOK_STRING;
	}

 /* identifiers */
[A-Za-z_][A-Za-z0-9_]* {
		yylval.string = copy_string(yytext);
		add_id_to_hash(yylval.string);
		return TOK_IDENTIFIER;
	}

[ \t\n] { /* ignore whitespace */ }

.       { yyerror("Invalid character %c\n", *yytext); }

%%
