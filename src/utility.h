#ifndef IRSCC_UTILITY_H
#define IRSCC_UTILITY_H

#include <stddef.h>

#if defined(__cplusplus)
extern "C" {
#endif

void *checked_malloc(size_t size);
char *copy_string(const char *s);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_UTILITY_H */
