#include <string>
#include <iostream>

struct latexConstant {
	static std::string rule (void);
	static std::ostream& headerOut (std::ostream&);
	static std::ostream& footerOut (std::ostream&);
};


