#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "formdesc.h"
#include "llist.h"
#include "stringset.h"
#include "symtab.h"
#include "translate.h"
#include "utility.h"

static struct form_desc *current_form = 0;
static struct linked_list *current_form_list = 0;

int next_qtyid = 1;

/* ===== Helper functions to work with line_desc objects =================== */

static struct line_desc *new_line(enum opcode op)
{
	struct line_desc *line = checked_malloc(sizeof(*line));
	line->lineno.form = 0;
	line->lineno.line = 0;
	line->lineno.subline = '\0';
	line->op = op;
	line->args = ll_new();
	line->qtyid = 0;
	line->enter_also.lineptr = 0;
	line->goto_after.lineptr = 0;
	line->is_jump_target = 0;
	return line;
}

static struct line_desc *decorate(struct line_desc *line, int qtyid,
		struct line_desc *enter_also, struct line_desc *goto_after)
{
	if (qtyid != 0)       line->qtyid = qtyid;
	if (enter_also != 0)  line->enter_also.lineptr = enter_also;
	if (goto_after != 0)  line->goto_after.lineptr = goto_after;
	return line;
}

static inline struct line_desc *jt(struct line_desc *line)
{
	line->is_jump_target = 1;
	return line;
}

static struct line_desc *earliest_jump_target(struct linked_list *L)
{
	struct line_desc *target = 0;
	struct ll_iter *iter = ll_iterator(L);
	while (ll_has_next(iter)) {
		struct line_desc * const line = ll_next_ptr(iter);
		switch (line->op) {
			case OP_ASSIGN:
			case OP_SYMTAB:
				break;  /* out of switch; keep looking */
			case OP_RECONCILE:
				/* this shouldn't happen */
				fprintf(stderr, "Earliest jump target is OP_RECONCILE\n");
				exit(EXIT_FAILURE);
			case OP_NOP:
				target = line;
				break;  /* out of switch; keep looking */
			default:
				target = line;
				goto FOUND_JUMP_TARGET;
		}
	}
FOUND_JUMP_TARGET:
	ll_iterator_free(iter);
	assert(target != 0);
	return target;
}

/* ===== Functions to push instructions onto lists ========================= */

static struct line_desc *push_DOLLARS_CONSTANT(struct linked_list *L,
		unsigned long n)
{
	struct line_desc *line = new_line(OP_DOLLARS_CONSTANT);
	ll_push_int(line->args, (int)n);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_CENTS_CONSTANT(struct linked_list *L, double d)
{
	struct line_desc *line = new_line(OP_CENTS_CONSTANT);
	ll_push_float(line->args, (float)d);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_COPY(struct linked_list *L, struct line_desc *l)
{
	struct line_desc *line = new_line(OP_COPY);
	ll_push_ptr(line->args, l);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_COPY_FROM_ATTACHED(struct linked_list *L,
		struct line_desc *l)
{
	struct line_desc *line = new_line(OP_COPY_FROM_ATTACHED);
	ll_push_ptr(line->args, l);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_ADD(struct linked_list *L,
		struct line_desc *augend, struct line_desc *addend)
{
	struct line_desc *line = new_line(OP_ADD);
	ll_push_ptr(line->args, augend);
	ll_push_ptr(line->args, addend);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_SUBTRACT(struct linked_list *L,
		struct line_desc *minuend, struct line_desc *subtrahend)
{
	struct line_desc *line = new_line(OP_SUBTRACT);
	ll_push_ptr(line->args, minuend);
	ll_push_ptr(line->args, subtrahend);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_MULTIPLY(struct linked_list *L,
		struct line_desc *multiplicand, struct line_desc *multiplier)
{
	struct line_desc *line = new_line(OP_MULTIPLY);
	ll_push_ptr(line->args, multiplicand);
	ll_push_ptr(line->args, multiplier);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_DIVIDE(struct linked_list *L,
		struct line_desc *dividend, struct line_desc *divisor)
{
	struct line_desc *line = new_line(OP_DIVIDE);
	ll_push_ptr(line->args, dividend);
	ll_push_ptr(line->args, divisor);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_INTEGER_DIVIDE(struct linked_list *L,
		struct line_desc *dividend, struct line_desc *divisor)
{
	struct line_desc *line = new_line(OP_INTEGER_DIVIDE);
	ll_push_ptr(line->args, dividend);
	ll_push_ptr(line->args, divisor);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_NEGATE(struct linked_list *L,
		struct line_desc *l)
{
	struct line_desc *line = new_line(OP_NEGATE);
	ll_push_ptr(line->args, l);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_IF_ZERO_GOTO(struct linked_list *L,
		struct line_desc *cond, struct line_desc *jump_target)
{
	struct line_desc *line = new_line(OP_IF_ZERO_GOTO);
	ll_push_ptr(line->args, cond);
	ll_push_ptr(line->args, jump_target);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_IF_NONZERO_GOTO(struct linked_list *L,
		struct line_desc *cond, struct line_desc *jump_target)
{
	struct line_desc *line = new_line(OP_IF_NONZERO_GOTO);
	ll_push_ptr(line->args, cond);
	ll_push_ptr(line->args, jump_target);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_IF_POSITIVE_GOTO(struct linked_list *L,
		struct line_desc *cond, struct line_desc *jump_target)
{
	struct line_desc *line = new_line(OP_IF_POSITIVE_GOTO);
	ll_push_ptr(line->args, cond);
	ll_push_ptr(line->args, jump_target);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_IF_NEGATIVE_GOTO(struct linked_list *L,
		struct line_desc *cond, struct line_desc *jump_target)
{
	struct line_desc *line = new_line(OP_IF_NEGATIVE_GOTO);
	ll_push_ptr(line->args, cond);
	ll_push_ptr(line->args, jump_target);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_IF_NONNEGATIVE_GOTO(struct linked_list *L,
		struct line_desc *cond, struct line_desc *jump_target)
{
	struct line_desc *line = new_line(OP_IF_NONNEGATIVE_GOTO);
	ll_push_ptr(line->args, cond);
	ll_push_ptr(line->args, jump_target);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_IF_NONPOSITIVE_GOTO(struct linked_list *L,
		struct line_desc *cond, struct line_desc *jump_target)
{
	struct line_desc *line = new_line(OP_IF_NONPOSITIVE_GOTO);
	ll_push_ptr(line->args, cond);
	ll_push_ptr(line->args, jump_target);
	ll_push_ptr(L, line);
	return line;
}

/*
static struct line_desc *push_IF_CHECKED_COPY_ELSE_COPY(struct linked_list *L,
		struct line_desc *checkbox, struct line_desc *copy_if_checked,
		struct line_desc *copy_if_unchecked)
{
	struct line_desc *line = new_line(OP_IF_CHECKED_COPY_ELSE_COPY);
	ll_push_ptr(line->args, checkbox);
	ll_push_ptr(line->args, copy_if_checked);
	ll_push_ptr(line->args, copy_if_unchecked);
	ll_push_ptr(L, line);
	return line;
}
*/

static struct line_desc *push_NOP(struct linked_list *L)
{
	struct line_desc *line = new_line(OP_NOP);
	ll_push_ptr(L, line);
	return line;
}

/*
static struct line_desc *push_STOP(struct linked_list *L)
{
	struct line_desc *line = new_line(OP_STOP);
	ll_push_ptr(L, line);
	return line;
}
*/

static struct line_desc *push_BEGIN_CALL_BLOCK(struct linked_list *L)
{
	struct line_desc *line = new_line(OP_BEGIN_CALL_BLOCK);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_CALL(struct linked_list *L, struct form_desc *fn)
{
	struct line_desc *line = new_line(OP_CALL);
	ll_push_ptr(line->args, fn);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_END_CALL_BLOCK(struct linked_list *L)
{
	struct line_desc *line = new_line(OP_END_CALL_BLOCK);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_ASSIGN(struct linked_list *L,
		struct symtab_node *context, const char *id, struct line_desc *value)
{
	struct line_desc *line = new_line(OP_ASSIGN);
	ll_push_ptr(line->args, context);
	ll_push_ptr(line->args, copy_string(id));
	ll_push_ptr(line->args, value);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_SYMTAB(struct linked_list *L,
		struct symtab_node *context, const char *id)
{
	struct line_desc *line = new_line(OP_SYMTAB);
	ll_push_ptr(line->args, context);
	ll_push_ptr(line->args, copy_string(id));
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_RECONCILE(struct linked_list *L,
		struct symtab_node *context, const char *id, struct line_desc *checkbox,
		struct line_desc *stash)
{
	struct line_desc *line = new_line(OP_RECONCILE);
	ll_push_ptr(line->args, context);
	ll_push_ptr(line->args, copy_string(id));
	ll_push_ptr(line->args, checkbox);
	ll_push_ptr(line->args, stash);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_GOTO(struct linked_list *L,
		struct line_desc *jump_target)
{
	struct line_desc *line = decorate(new_line(OP_GOTO), 0, 0, jump_target);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_FN_ARG(struct linked_list *L,
		struct line_desc *l, struct symtab_node *context, const char *id,
		int argno)
{
	struct line_desc *line = new_line(OP_FN_ARG);
	ll_push_ptr(line->args, l);
	ll_push_ptr(line->args, context);
	ll_push_ptr(line->args, copy_string(id));
	ll_push_int(line->args, argno);
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_FN_CALL(struct linked_list *L,
		struct symtab_node *context, const char *id)
{
	struct line_desc *line = new_line(OP_FN_CALL);
	ll_push_ptr(line->args, context);
	ll_push_ptr(line->args, copy_string(id));
	ll_push_ptr(L, line);
	return line;
}

static struct line_desc *push_FN_RET(struct linked_list *L,
		struct symtab_node *context, const char *id)
{
	struct line_desc *line = new_line(OP_FN_RET);
	ll_push_ptr(line->args, context);
	ll_push_ptr(line->args, copy_string(id));
	ll_push_ptr(L, line);
	return line;
}

/* ===== Function definition nodes ========================================= */

void tr_fn(struct ast_fn_node *fn, struct linked_list *form_list)
{
	assert(fn != 0);  assert(form_list != 0);
	assert(fn->dxr != 0);  assert(fn->dxr->type == DXR_FN);
	assert(fn->body != 0);
	/* start a new form */
	struct linked_list *this_form_and_schedules = ll_new();
	struct form_desc *form = checked_malloc(sizeof(*form));
	form->name = copy_string(fn->dxr->id);  /* TODO? */
	form->parent = 0;
	form->lines = ll_new();
	ll_push_ptr(this_form_and_schedules, form);
	current_form = form;
	current_form_list = this_form_and_schedules;
	struct symtab_entry *entry = symtab_search(fn->symtab_context, fn->dxr->id);
	assert(entry != 0);  assert(entry->type->meta == TYPE_FN);
	entry->form = form;
	/* lines to receive function arguments */
	assert(fn->dxr->u.fn.args != 0);  assert(entry->first_arg_qtyid != 0);
	int qtyid = entry->first_arg_qtyid;
	struct symtab_node * const body_context = fn->body->symtab_context;
	struct ll_iter *iter = ll_iterator(fn->dxr->u.fn.args);
	while (ll_has_next(iter)) {
		const struct ast_decl_node *decl = ll_next_ptr(iter);
		assert(decl != 0);  assert(decl->idxrs != 0);
		assert(ll_size(decl->idxrs) == 1);
		const struct ast_idxr_node *idxr = ll_head_ptr(decl->idxrs);
		assert(idxr != 0);  assert(idxr->dxr != 0);  assert(idxr->init == 0);
		struct line_desc *nop = decorate(push_NOP(form->lines), qtyid, 0, 0);
		push_ASSIGN(form->lines, body_context, idxr->dxr->id, nop);
		++qtyid;
	}
	ll_iterator_free(iter);
	/* translate body */
	ll_concat_and_free(form->lines, tr_stmt(fn->body));
	entry->ret = ll_tail_ptr(form->lines);
	/* put this form and schedules into form_list */
	if (strcmp(fn->dxr->id, "main") == 0) {
		/* main should go first in form_list */
		while (ll_size(this_form_and_schedules) > 0)
			ll_unshift_ptr(form_list, ll_pop_ptr(this_form_and_schedules));
		ll_free(this_form_and_schedules);
	} else
		ll_concat_and_free(form_list, this_form_and_schedules);
	/* clear globals */
	current_form_list = 0;
	current_form = 0;
}

/* ===== Statement nodes =================================================== */

static struct linked_list *tr_stmt_EMPTY(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_CONTINUE(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_BREAK(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_RETURN(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_GOTO(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_DEFAULT(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_EXPR(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_RETURN_EXPR(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_LABELED(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_CASE(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_IF(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_SWITCH(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_WHILE(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_DO_WHILE(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_IF_ELSE(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_COMPOUND(struct ast_stmt_node *stmt);
static struct linked_list *tr_stmt_FOR(struct ast_stmt_node *stmt);

struct linked_list *tr_stmt(struct ast_stmt_node *stmt)
{
	assert(stmt != 0);
	switch (stmt->stmt_code) {
		case EMPTY_STMT:        return tr_stmt_EMPTY(stmt);        break;
		case CONTINUE_STMT:     tr_stmt_CONTINUE(stmt);  goto NOT_YET;
		case BREAK_STMT:        tr_stmt_BREAK(stmt);  goto NOT_YET;
		case RETURN_STMT:       return tr_stmt_RETURN(stmt);       break;
		case GOTO_STMT:         tr_stmt_GOTO(stmt);  goto NOT_YET;
		case DEFAULT_STMT:      tr_stmt_DEFAULT(stmt);  goto NOT_YET;
		case EXPR_STMT:         return tr_stmt_EXPR(stmt);         break;
		case RETURN_EXPR_STMT:  return tr_stmt_RETURN_EXPR(stmt);  break;
		case LABELED_STMT:      tr_stmt_LABELED(stmt);  goto NOT_YET;
		case CASE_STMT:         tr_stmt_CASE(stmt);  goto NOT_YET;
		case IF_STMT:           return tr_stmt_IF(stmt);           break;
		case SWITCH_STMT:       tr_stmt_SWITCH(stmt);  goto NOT_YET;
		case WHILE_STMT:        return tr_stmt_WHILE(stmt);        break;
		case DO_WHILE_STMT:     return tr_stmt_DO_WHILE(stmt);     break;
		case IF_ELSE_STMT:      return tr_stmt_IF_ELSE(stmt);      break;
		case COMPOUND_STMT:     return tr_stmt_COMPOUND(stmt);     break;
		case FOR_STMT:          tr_stmt_FOR(stmt);  goto NOT_YET;
		NOT_YET:
			fprintf(stderr, "Can't translate stmt_code %ld yet. How did you "
					"get here, anyway?\n", (long)(stmt->stmt_code));
			exit(EXIT_FAILURE);
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in tr_stmt: %ld\n",
					(long)(stmt->stmt_code));
			exit(EXIT_FAILURE);
	}
}

static struct linked_list *tr_stmt_EMPTY(struct ast_stmt_node *stmt)
{
	(void)stmt;
	struct linked_list *L = ll_new();
	push_NOP(L);  /* ensure that there's an instruction here */
	return L;
}

static struct linked_list *tr_stmt_CONTINUE(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_CONTINUE\n");  return 0; }

static struct linked_list *tr_stmt_BREAK(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_BREAK\n");  return 0; }

static struct linked_list *tr_stmt_RETURN(struct ast_stmt_node *stmt)
{
	(void)stmt;
	struct linked_list *L = ll_new();
	push_NOP(L);  /* ensure that there's an instruction here */
	return L;
}

static struct linked_list *tr_stmt_GOTO(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_GOTO\n");  return 0; }

static struct linked_list *tr_stmt_DEFAULT(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_DEFAULT\n");  return 0; }

static struct linked_list *tr_stmt_EXPR(struct ast_stmt_node *stmt)
{
	struct linked_list *L = tr_expr(stmt->u.expr);
	push_NOP(L);  /* ensure that there's a real instruction here */
	return L;
}

static struct linked_list *tr_stmt_RETURN_EXPR(struct ast_stmt_node *stmt)
{
	struct linked_list *L = tr_expr(stmt->u.expr);
	assert(ll_size(L) > 0);
	assert(stmt->fn != 0);
	struct symtab_entry * const entry = symtab_search(stmt->fn->symtab_context,
			stmt->fn->dxr->id);
	assert(entry != 0);
	struct line_desc * const tail = ll_tail_ptr(L);
	switch (tail->op) {
		case OP_ASSIGN:
		case OP_SYMTAB:
			push_COPY(L, tail);
			break;
		default:
			assert(tail->op != OP_RECONCILE);
			assert(tail->op != OP_GOTO);
			/* nothing to be done */
	}
	decorate(ll_tail_ptr(L), entry->return_qtyid, 0, 0);
	return L;
}

static struct linked_list *tr_stmt_LABELED(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_LABELED\n");  return 0; }

static struct linked_list *tr_stmt_CASE(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_CASE\n");  return 0; }

/*
	Translates a condition (for an 'if' statement, for example) into a sequence
	of calculations, which are pushed onto cond_list, and an appropriate
	conditional jump, which is pushed onto jump_list and returned.
*/
static struct line_desc *condition_and_jump(struct ast_expr_node *condition,
		struct line_desc *jump_target_if_false, struct linked_list *cond_list,
		struct linked_list *jump_list)
{
	assert(condition != 0);  assert(jump_target_if_false != 0);
	assert(cond_list != 0);  assert(jump_list != 0);
	assert(jump_target_if_false->is_jump_target);
	if (condition->expr_code == BINARY_OP && (  /* special cases */
		   condition->u.binary.op == LESS
		|| condition->u.binary.op == LESS_EQUALS
		|| condition->u.binary.op == GREATER
		|| condition->u.binary.op == GREATER_EQUALS
		|| condition->u.binary.op == EQUALS_EQUALS
		|| condition->u.binary.op == NOT_EQUALS
	)) {
		ll_concat_and_free(cond_list, tr_expr(condition->u.binary.left));
		struct line_desc *lhs = ll_tail_ptr(cond_list);
		ll_concat_and_free(cond_list, tr_expr(condition->u.binary.right));
		struct line_desc *rhs = ll_tail_ptr(cond_list);
		struct line_desc *t = push_SUBTRACT(cond_list, lhs, rhs);
		switch (condition->u.binary.op) {
			case LESS:
				return push_IF_NONNEGATIVE_GOTO(jump_list, t,
						jump_target_if_false);
			case LESS_EQUALS:
				return push_IF_POSITIVE_GOTO(jump_list, t,
						jump_target_if_false);
			case GREATER:
				return push_IF_NONPOSITIVE_GOTO(jump_list, t,
						jump_target_if_false);
			case GREATER_EQUALS:
				return push_IF_NEGATIVE_GOTO(jump_list, t,
						jump_target_if_false);
			case EQUALS_EQUALS:
				return push_IF_NONZERO_GOTO(jump_list, t,
						jump_target_if_false);
			case NOT_EQUALS:
				return push_IF_ZERO_GOTO(jump_list, t, jump_target_if_false);
			default:  /* this shouldn't happen */
				fprintf(stderr, "Forgot a case in condition_and_jump\n");
				exit(EXIT_FAILURE);
		}
	} else {  /* no special case */
		ll_concat_and_free(cond_list, tr_expr(condition));
		return push_IF_ZERO_GOTO(jump_list, ll_tail_ptr(cond_list),
				jump_target_if_false);
	}
}

/*
	Translation of a conditional statement, with dependencies.

		1. Translation of condition.
		2. Stash: OP_SYMTAB for all variables written in body.
		3. Conditional jump (checkbox line).  [Dep: 1, 5.]
		4. Body.
		5. OP_NOP: jump target.
		6. OP_RECONCILE/OP_ASSIGN pairs for all variables written in body.
		   [Dep: 2, 3.]

	Construction order:

		5. Jump target.  [Push to reconcile.]
		1,3. Condition and conditional jump, using condition_and_jump.
		   [Push condition to L, push jump to jump_list.]
		For each variable written in body:
			2. OP_SYMTAB (stash).  [Push to L.]
			6. OP_RECONCILE/OP_ASSIGN.  [Push to reconcile.]
		Concatenate jump_list to L.
		4. Body.  [Concatenate to L.]
		Concatenate reconcile to L.
*/
static struct linked_list *conditional(struct ast_expr_node *condition,
		struct linked_list *body, struct stringset *vars_written_in_body,
		struct symtab_node *symtab_context)
{
	struct linked_list *L = ll_new();
	/* start list of reconciliation instructions (need jump_target) */
	struct linked_list *reconcile = ll_new();
	struct line_desc *jump_target = jt(push_NOP(reconcile));
	/* translate condition and conditional jump (checkbox line) */
	struct linked_list *jump_list = ll_new();
	struct line_desc *checkbox = condition_and_jump(condition, jump_target, L,
			jump_list);
	/* stash and reconcile all variables written in body */
	struct ss_iter *var_iter = stringset_iterator(vars_written_in_body);
	while (stringset_has_next(var_iter)) {
		const char * const id = stringset_next(var_iter);
		struct line_desc *stash = push_SYMTAB(L, symtab_context, id);
		struct line_desc *reconciled_value = push_RECONCILE(reconcile,
				symtab_context, id, checkbox, stash);
		push_ASSIGN(reconcile, symtab_context, id, reconciled_value);
	}
	stringset_iterator_free(var_iter);
	/* concatenate lists and return */
	ll_concat_and_free(L, jump_list);
	ll_concat_and_free(L, body);
	ll_concat_and_free(L, reconcile);
	return L;
}

static struct linked_list *tr_stmt_IF(struct ast_stmt_node *stmt)
{
	return conditional(stmt->u.cond_stmt.cond, tr_stmt(stmt->u.cond_stmt.body),
			stmt->u.cond_stmt.body->vars_written, stmt->symtab_context);
}

static struct linked_list *tr_stmt_SWITCH(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_SWITCH\n");  return 0; }

/*
	Translation of a 'while' statement, with dependencies.

	In "calling" form:
		1.  OP_BEGIN_CALL_BLOCK.
		2.  OP_CALL.  [Dep: creation of subform.]
		3.  OP_COPY all variables read in condition and body, and those written
		    in body, with nonzero qtyid and enter_also.  [Dep: 6.]
		4.  OP_COPY_FROM_ATTACHED/OP_ASSIGN pairs for all variables written in
		    condition and body.  [Dep: 18.]
		5.  OP_END_CALL_BLOCK.
	
	In schedule (subform):
		6.  OP_NOP/OP_ASSIGN pairs for all variables read in condition and
		    body, and those written in body, with nonzero qtyid for OP_NOPs.
		7.  Translation of condition.
		8.  Stash: OP_SYMTAB for all variables written in body.
		9.  Conditional jump (checkbox line).  [Dep: 7, 16.]
		10. Translation of body.
		11. OP_BEGIN_CALL_BLOCK.
		12. OP_CALL.  [Dep: creation of subform.]
		13. OP_COPY all variables read in condition and body, with nonzero
		    qtyid and enter_also.  [Dep: 6.]
		14. OP_COPY_FROM_ATTACHED/OP_ASSIGN pairs for all variables written in
		    condition and body.  [Dep: 18.]
		15. OP_END_CALL_BLOCK.
		16. OP_NOP: jump target.
		17. OP_RECONCILE/OP_ASSIGN pairs for all variables written in body.
		    [Dep: 8, 9.]
		18. OP_SYMTAB for all variables written in condition and body.

	Construction order:

		Create subform and add to form list.  [Store pointer in sched.]
		1.  OP_BEGIN_CALL_BLOCK.  [Push to L.]
		11. OP_BEGIN_CALL_BLOCK.  [Push to sched_call.]
		2.  OP_CALL.  [Push to L.]
		12. OP_CALL.  [Push to sched_call.]
		For each variable read in condition and body, or written in body:
			6.  OP_NOP/OP_ASSIGN pairs.  [Push to sched->lines.]
			3.  OP_COPY with enter_also.  [Push to L.]
			13. OP_COPY with enter_also.  [Push to sched_call.]
		For each variable written in condition and body:
			18. OP_SYMTAB (stash).  [Push to return_symtabs.]
			4.  OP_COPY_FROM_ATTACHED/OP_ASSIGN.  [Push to L.]
			14. OP_COPY_FROM_ATTACHED/OP_ASSIGN.  [Push to sched_call.]
		5.  OP_END_CALL_BLOCK.  [Push to L.]
		15. OP_END_CALL_BLOCK.  [Push to sched_call.]
		10. Body.  [Save as body_list.]
		Concatenate sched_call to body_list.

		At this point L is done, and the remaining parts of the translation
		(7, 8, 9, 16, and 17) can be translated with a call to conditional().
		The returned list is concatenated to sched->lines, and then
		return_symtabs is concatenated to that.
*/
static struct linked_list *tr_stmt_WHILE(struct ast_stmt_node *stmt)
{
	struct linked_list *L = ll_new();
	/* create subform and add to current_form_list */
	struct form_desc *sched = checked_malloc(sizeof(*sched));
	sched->name = 0;
	sched->parent = current_form;
	sched->lines = ll_new();
	ll_push_ptr(current_form_list, sched);
	/* begin call blocks */
	push_BEGIN_CALL_BLOCK(L);
	struct linked_list *sched_call = ll_new();
	push_BEGIN_CALL_BLOCK(sched_call);
	/* function call */
	push_CALL(L, sched);
	push_CALL(sched_call, sched);
	/* copy to schedule all variables read in loop condition and body, or
	   written in body */
	struct symtab_node *context = stmt->symtab_context;
	struct stringset *input_vars = stringset_new();
	stringset_add_all(input_vars, stmt->vars_read);
	stringset_add_all(input_vars, stmt->u.cond_stmt.body->vars_written);
	struct ss_iter *var_iter = stringset_iterator(input_vars);
	while (stringset_has_next(var_iter)) {
		const char *id = stringset_next(var_iter);
		struct line_desc *nop = decorate(push_NOP(sched->lines),next_qtyid,0,0);
		push_ASSIGN(sched->lines, context, id, nop);
		struct line_desc *parent_symtab = push_SYMTAB(L, context, id);
		decorate(push_COPY(L, parent_symtab), next_qtyid, nop, 0);
		struct line_desc *sched_symtab = push_SYMTAB(sched_call, context, id);
		decorate(push_COPY(sched_call, sched_symtab), next_qtyid, nop, 0);
		++next_qtyid;
	}
	stringset_iterator_free(var_iter);
	stringset_free(input_vars);
	/* copy all variables written in condition and body back to calling form */
	struct linked_list *return_symtabs = ll_new();
	var_iter = stringset_iterator(stmt->vars_written);
	while (stringset_has_next(var_iter)) {
		const char *id = stringset_next(var_iter);
		struct line_desc *symtab = push_SYMTAB(return_symtabs, context, id);
		struct line_desc *copy = push_COPY_FROM_ATTACHED(L, symtab);
		push_ASSIGN(L, context, id, copy);
		copy = push_COPY_FROM_ATTACHED(sched_call, symtab);
		push_ASSIGN(sched_call, context, id, copy);
	}
	stringset_iterator_free(var_iter);
	/* end call blocks */
	push_END_CALL_BLOCK(L);
	push_END_CALL_BLOCK(sched_call);
	/* translate body and append sched_call */
	struct linked_list *body_list = tr_stmt(stmt->u.cond_stmt.body);
	ll_concat_and_free(body_list, sched_call);
	/* conditional evaluation of body_list */
	ll_concat_and_free(sched->lines, conditional(stmt->u.cond_stmt.cond,
			body_list, stmt->u.cond_stmt.body->vars_written, context));
	/* append return_symtabs */
	ll_concat_and_free(sched->lines, return_symtabs);
	return L;
}

static struct linked_list *tr_stmt_DO_WHILE(struct ast_stmt_node *stmt)
{
	struct linked_list *L = ll_new();
	/* create subform and add to current_form_list */
	struct form_desc *sched = checked_malloc(sizeof(*sched));
	sched->name = 0;
	sched->parent = current_form;
	sched->lines = ll_new();
	ll_push_ptr(current_form_list, sched);
	/* begin call blocks */
	push_BEGIN_CALL_BLOCK(L);
	struct linked_list *sched_call = ll_new();
	push_BEGIN_CALL_BLOCK(sched_call);
	/* function call */
	push_CALL(L, sched);
	push_CALL(sched_call, sched);
	/* copy to schedule all variables read in loop condition and body */
	struct symtab_node *context = stmt->symtab_context;
	struct ss_iter *var_iter = stringset_iterator(stmt->vars_read);
	while (stringset_has_next(var_iter)) {
		const char *id = stringset_next(var_iter);
		struct line_desc *nop = decorate(push_NOP(sched->lines),next_qtyid,0,0);
		push_ASSIGN(sched->lines, context, id, nop);
		struct line_desc *parent_symtab = push_SYMTAB(L, context, id);
		decorate(push_COPY(L, parent_symtab), next_qtyid, nop, 0);
		struct line_desc *sched_symtab = push_SYMTAB(sched_call, context, id);
		decorate(push_COPY(sched_call, sched_symtab), next_qtyid, nop, 0);
		++next_qtyid;
	}
	stringset_iterator_free(var_iter);
	/* copy all variables written in condition and body back to calling form */
	struct linked_list *return_symtabs = ll_new();
	var_iter = stringset_iterator(stmt->vars_written);
	while (stringset_has_next(var_iter)) {
		const char *id = stringset_next(var_iter);
		struct line_desc *symtab = push_SYMTAB(return_symtabs, context, id);
		struct line_desc *copy = push_COPY_FROM_ATTACHED(L, symtab);
		push_ASSIGN(L, context, id, copy);
		copy = push_COPY_FROM_ATTACHED(sched_call, symtab);
		push_ASSIGN(sched_call, context, id, copy);
	}
	stringset_iterator_free(var_iter);
	/* end call blocks */
	push_END_CALL_BLOCK(L);
	push_END_CALL_BLOCK(sched_call);
	/* translate body and append sched_call */
	ll_concat_and_free(sched->lines, tr_stmt(stmt->u.cond_stmt.body));
	/* conditional evaluation of body_list */
	ll_concat_and_free(sched->lines, conditional(stmt->u.cond_stmt.cond,
			sched_call, stmt->u.cond_stmt.body->vars_written, context));
	/* append return_symtabs */
	ll_concat_and_free(sched->lines, return_symtabs);
	return L;
}

/*
	Translation of an 'if/else' statement, with dependencies.

		1.  Translation of condition.
		2.  Stash for 'else' body: OP_SYMTAB for all variables written in 'if'
		    body and read in 'else' body.
		3.  Conditional jump (checkbox line).  [Dep: 1, 8.]
		4.  'if' body.
		5.  OP_SYMTAB for all variables written in 'if' body or 'else' body.
		6.  OP_GOTO, pointing to 9.  [Dep: 9.]
		7.  Restore variables: OP_ASSIGN all variables written in 'if' body and
		    read in 'else' body.  [Dep: 2.]
		8.  'else' body. (Jump target for conditional jump.)
		9.  OP_NOP: jump target from 6.
		10. OP_RECONCILE/OP_ASSIGN pairs for all variables written in 'if' body
		    or 'else' body.  [Dep: 5.]

	Construction order:

		8.  'else' body.  [Save as else_list.]
		1,3. Condition and conditional jump, using condition_and_jump.
		   [Push condition to L, push jump to jump_list.]
		For all variables written in 'if' body and read in 'else' body:
			2.  OP_SYMTAB (stash for 'else').  [Push to L.]
			7.  OP_ASSIGN (restore).  [Push to pre_else_list.]
		Concatenate jump_list to L.
		4.  'if' body.  [Push to L.]
		9.  Jump target for end of 'if' body.  [Push to reconcile.]
		For all variables written in 'if' body or 'else' body:
			5.  OP_SYMTAB.  [Push to L.]
			10. OP_RECONCILE/OP_ASSIGN.  [Push to reconcile.]
		6.  OP_GOTO.  [Push to L.]
		Concatenate pre_else_list to L.
		Concatenate else_list to L.
		Concatenate reconcile to L.

*/
static struct linked_list *tr_stmt_IF_ELSE(struct ast_stmt_node *stmt)
{
	struct ast_stmt_node * const if_body = stmt->u.if_else.if_body;
	struct ast_stmt_node * const else_body = stmt->u.if_else.else_body;
	struct symtab_node * const symtab_context = stmt->symtab_context;
	/* 'else' body */
	struct linked_list *else_list = tr_stmt(else_body);
	struct line_desc *jump_target_if_false =
			jt(earliest_jump_target(else_list));
	/* condition and conditional jump */
	struct linked_list *L = ll_new();
	struct linked_list *jump_list = ll_new();
	struct line_desc *checkbox = condition_and_jump(stmt->u.if_else.cond,
			jump_target_if_false, L, jump_list);
	/* stash and restore vars written in 'if' body and read in 'else' body */
	struct linked_list *pre_else_list = ll_new();
	struct stringset *ss = stringset_intersection(if_body->vars_written,
			else_body->vars_read);
	struct ss_iter *i = stringset_iterator(ss);
	while (stringset_has_next(i)) {
		const char * const id = stringset_next(i);
		struct line_desc *else_stash = push_SYMTAB(L, symtab_context, id);
		push_ASSIGN(pre_else_list, symtab_context, id, else_stash);
	}
	stringset_iterator_free(i);
	stringset_free(ss);
	/* concatenate jump_list to L */
	ll_concat_and_free(L, jump_list);
	/* 'if' body */
	ll_concat_and_free(L, tr_stmt(if_body));
	/* jump target for end of 'if' body */
	struct linked_list *reconcile = ll_new();
	struct line_desc *end_of_if_jump_target = jt(push_NOP(reconcile));
	/* reconcile variables written in 'if' body or 'else' body */
	ss = stringset_copy(if_body->vars_written);
	stringset_add_all(ss, else_body->vars_written);
	i = stringset_iterator(ss);
	while (stringset_has_next(i)) {
		const char * const id = stringset_next(i);
		struct line_desc *if_symtab = push_SYMTAB(L, symtab_context, id);
		/* kludge: set qtyid to reverse the meaning of OP_RECONCILE */
		struct line_desc *reconciled_value = decorate(push_RECONCILE(reconcile,
				symtab_context, id, checkbox, if_symtab), 1, 0, 0);
		push_ASSIGN(reconcile, symtab_context, id, reconciled_value);
	}
	stringset_iterator_free(i);
	stringset_free(ss);
	/* jump at the end of 'if' body */
	push_GOTO(L, end_of_if_jump_target);
	/* concatenate lists and return */
	ll_concat_and_free(L, pre_else_list);
	ll_concat_and_free(L, else_list);
	ll_concat_and_free(L, reconcile);
	return L;
}

static struct linked_list *tr_stmt_COMPOUND(struct ast_stmt_node *stmt)
{
	struct linked_list *L = ll_new();
	struct ll_iter *iter = ll_iterator(stmt->u.cpd.decls);
	while (ll_has_next(iter)) {
		struct ast_decl_node * const decl = ll_next_ptr(iter);
		struct ll_iter *idxr_iter = ll_iterator(decl->idxrs);
		while (ll_has_next(idxr_iter)) {
			struct ast_idxr_node * const idxr = ll_next_ptr(idxr_iter);
			if (idxr->init != 0) {
				ll_concat_and_free(L, tr_expr(idxr->init));
				push_ASSIGN(L, stmt->symtab_context, idxr->dxr->id,
						ll_tail_ptr(L));
			}
		}
		ll_iterator_free(idxr_iter);
	}
	ll_iterator_free(iter);
	iter = ll_iterator(stmt->u.cpd.stmts);
	while (ll_has_next(iter))
		ll_concat_and_free(L, tr_stmt(ll_next_ptr(iter)));
	ll_iterator_free(iter);
	if (ll_size(L) == 0)  push_NOP(L);  /* to ensure an instruction */
	return L;
}

static struct linked_list *tr_stmt_FOR(struct ast_stmt_node *stmt)
	{ (void)stmt;  fprintf(stderr, "tr_stmt_FOR\n");  return 0; }

/* ===== Expression nodes ================================================== */

static struct linked_list *tr_expr_INTEGER_CONSTANT(struct ast_expr_node *expr);
static struct linked_list *tr_expr_FLOATING_CONSTANT(struct ast_expr_node*expr);
static struct linked_list *tr_expr_IDENTIFIER(struct ast_expr_node *expr);
/* unary operators */
static struct linked_list *tr_expr_UNARY_OP(struct ast_expr_node *expr);
static struct linked_list *tr_expr_UPLUS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_UMINUS(struct ast_expr_node *expr);
/* increment and decrement operators */
static struct linked_list *tr_expr_INC_DEC(struct ast_expr_node *expr);
static struct linked_list *tr_expr_PRE_INC(struct ast_expr_node *expr);
static struct linked_list *tr_expr_PRE_DEC(struct ast_expr_node *expr);
/* binary operators */
static struct linked_list *tr_expr_BINARY_OP(struct ast_expr_node *expr);
static struct linked_list *tr_expr_BSTAR(struct ast_expr_node *expr);
static struct linked_list *tr_expr_SLASH(struct ast_expr_node *expr);
static struct linked_list *tr_expr_MODULO(struct ast_expr_node *expr);
static struct linked_list *tr_expr_BPLUS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_BMINUS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_LESS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_LESS_EQUALS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_GREATER(struct ast_expr_node *expr);
static struct linked_list *tr_expr_GREATER_EQUALS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_EQUALS_EQUALS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_NOT_EQUALS(struct ast_expr_node *expr);
static struct linked_list *tr_expr_COMMA(struct ast_expr_node *expr);
/* assignment operators */
static struct linked_list *tr_expr_ASSIGNMENT(struct ast_expr_node *expr);
static struct linked_list *tr_expr_ASSIGN(struct ast_expr_node *expr);
static struct linked_list *tr_expr_PLUS_ASSIGN(struct ast_expr_node *expr);
static struct linked_list *tr_expr_MINUS_ASSIGN(struct ast_expr_node *expr);
static struct linked_list *tr_expr_STAR_ASSIGN(struct ast_expr_node *expr);
static struct linked_list *tr_expr_SLASH_ASSIGN(struct ast_expr_node *expr);
static struct linked_list *tr_expr_MODULO_ASSIGN(struct ast_expr_node *expr);
/* function call */
static struct linked_list *tr_expr_FUNCTION_CALL(struct ast_expr_node *expr);

struct linked_list *tr_expr(struct ast_expr_node *expr)
{
	assert(expr != 0);
	switch (expr->expr_code) {
		case INTEGER_CONSTANT:  return tr_expr_INTEGER_CONSTANT(expr);   break;
		case FLOATING_CONSTANT: return tr_expr_FLOATING_CONSTANT(expr);  break;
		case IDENTIFIER:        return tr_expr_IDENTIFIER(expr);         break;
		case UNARY_OP:          return tr_expr_UNARY_OP(expr);           break;
		case INC_DEC:           return tr_expr_INC_DEC(expr);            break;
		case BINARY_OP:         return tr_expr_BINARY_OP(expr);          break;
		case ASSIGNMENT:        return tr_expr_ASSIGNMENT(expr);         break;
		case FUNCTION_CALL:     return tr_expr_FUNCTION_CALL(expr);      break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in translate\n");
			exit(EXIT_FAILURE);
	}
}

static struct linked_list *tr_expr_INTEGER_CONSTANT(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	push_DOLLARS_CONSTANT(L, expr->u.n);
	return L;
}

static struct linked_list *tr_expr_FLOATING_CONSTANT(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	push_CENTS_CONSTANT(L, expr->u.d);
	return L;
}

static struct linked_list *tr_expr_IDENTIFIER(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	push_SYMTAB(L, expr->symtab_context, expr->u.id);
	return L;
}

static struct linked_list *tr_expr_UNARY_OP(struct ast_expr_node *expr)
{
	switch (expr->u.unary.op) {
		case UPLUS:   return tr_expr_UPLUS(expr);   break;
		case UMINUS:  return tr_expr_UMINUS(expr);  break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in tr_expr_UNARY_OP\n");
			exit(EXIT_FAILURE);
	}
}

static struct linked_list *tr_expr_INC_DEC(struct ast_expr_node *expr)
{
	switch (expr->u.inc_dec.op) {
		case PRE_INC:  return tr_expr_PRE_INC(expr);  break;
		case PRE_DEC:  return tr_expr_PRE_DEC(expr);  break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in tr_expr_INC_DEC\n");
			exit(EXIT_FAILURE);
	}
}

static struct linked_list *tr_expr_BINARY_OP(struct ast_expr_node *expr)
{
	switch (expr->u.binary.op) {
		case BSTAR:           return tr_expr_BSTAR(expr);           break;
		case SLASH:           return tr_expr_SLASH(expr);           break;
		case MODULO:          return tr_expr_MODULO(expr);          break;
		case BPLUS:           return tr_expr_BPLUS(expr);           break;
		case BMINUS:          return tr_expr_BMINUS(expr);          break;
		case LESS:            return tr_expr_LESS(expr);            break;
		case LESS_EQUALS:     return tr_expr_LESS_EQUALS(expr);     break;
		case GREATER:         return tr_expr_GREATER(expr);         break;
		case GREATER_EQUALS:  return tr_expr_GREATER_EQUALS(expr);  break;
		case EQUALS_EQUALS:   return tr_expr_EQUALS_EQUALS(expr);   break;
		case NOT_EQUALS:      return tr_expr_NOT_EQUALS(expr);      break;
		case COMMA:           return tr_expr_COMMA(expr);           break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in tr_expr_BINARY_OP\n");
			exit(EXIT_FAILURE);
	}
}

static struct linked_list *tr_expr_ASSIGNMENT(struct ast_expr_node *expr)
{
	switch (expr->u.assign.op) {
		case ASSIGN:         return tr_expr_ASSIGN(expr);         break;
		case PLUS_ASSIGN:    return tr_expr_PLUS_ASSIGN(expr);    break;
		case MINUS_ASSIGN:   return tr_expr_MINUS_ASSIGN(expr);   break;
		case STAR_ASSIGN:    return tr_expr_STAR_ASSIGN(expr);    break;
		case SLASH_ASSIGN:   return tr_expr_SLASH_ASSIGN(expr);   break;
		case MODULO_ASSIGN:  return tr_expr_MODULO_ASSIGN(expr);  break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in tr_expr_ASSIGNMENT\n");
			exit(EXIT_FAILURE);
	}
}

static struct linked_list *tr_expr_PRE_INC(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	struct line_desc *lhs = push_SYMTAB(L, expr->symtab_context,
			expr->u.inc_dec.id);
	struct line_desc *one = push_DOLLARS_CONSTANT(L, 1);
	struct line_desc *sum = push_ADD(L, lhs, one);
	push_ASSIGN(L, expr->symtab_context, expr->u.inc_dec.id, sum);
	return L;
}

static struct linked_list *tr_expr_PRE_DEC(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	struct line_desc *lhs = push_SYMTAB(L, expr->symtab_context,
			expr->u.inc_dec.id);
	struct line_desc *one = push_DOLLARS_CONSTANT(L, 1);
	struct line_desc *difference = push_SUBTRACT(L, lhs, one);
	push_ASSIGN(L, expr->symtab_context, expr->u.inc_dec.id, difference);
	return L;
}

static struct linked_list *tr_expr_UPLUS(struct ast_expr_node *expr)
{
	return tr_expr(expr->u.unary.arg);
}

static struct linked_list *tr_expr_UMINUS(struct ast_expr_node *expr)
{
	struct linked_list *L = tr_expr(expr->u.unary.arg);
	push_NEGATE(L, ll_tail_ptr(L));
	return L;
}

static struct linked_list *tr_expr_BSTAR(struct ast_expr_node *expr)
{
	struct linked_list *L = tr_expr(expr->u.binary.left);
	struct line_desc *multiplicand = ll_tail_ptr(L);
	ll_concat_and_free(L, tr_expr(expr->u.binary.right));
	struct line_desc *multiplier = ll_tail_ptr(L);
	push_MULTIPLY(L, multiplicand, multiplier);
	return L;
}

static struct linked_list *tr_expr_SLASH(struct ast_expr_node *expr)
{
	struct linked_list *L = tr_expr(expr->u.binary.left);
	struct line_desc *dividend = ll_tail_ptr(L);
	ll_concat_and_free(L, tr_expr(expr->u.binary.right));
	struct line_desc *divisor = ll_tail_ptr(L);
	if (ast_type_is_integer(expr->type))
		push_INTEGER_DIVIDE(L, dividend, divisor);
	else
		push_DIVIDE(L, dividend, divisor);
	return L;
}

static struct linked_list *tr_expr_MODULO(struct ast_expr_node *expr)
{
	struct linked_list *L = tr_expr(expr->u.binary.left);
	struct line_desc *dividend = ll_tail_ptr(L);
	ll_concat_and_free(L, tr_expr(expr->u.binary.right));
	struct line_desc *divisor = ll_tail_ptr(L);
	struct line_desc *quotient = push_INTEGER_DIVIDE(L, dividend, divisor);
	struct line_desc *product = push_MULTIPLY(L, divisor, quotient);
	push_SUBTRACT(L, dividend, product);
	return L;
}

static struct linked_list *tr_expr_BPLUS(struct ast_expr_node *expr)
{
	struct linked_list *L = tr_expr(expr->u.binary.left);
	struct line_desc *augend = ll_tail_ptr(L);
	ll_concat_and_free(L, tr_expr(expr->u.binary.right));
	struct line_desc *addend = ll_tail_ptr(L);
	push_ADD(L, augend, addend);
	return L;
}

static struct linked_list *tr_expr_BMINUS(struct ast_expr_node *expr)
{
	struct linked_list *L = tr_expr(expr->u.binary.left);
	struct line_desc *minuend = ll_tail_ptr(L);
	ll_concat_and_free(L, tr_expr(expr->u.binary.right));
	struct line_desc *subtrahend = ll_tail_ptr(L);
	push_SUBTRACT(L, minuend, subtrahend);
	return L;
}

static struct linked_list *tr_expr_LESS(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Operator < not supported except as top level of 'if' condition. Sorry!\n");
	exit(EXIT_FAILURE);
}

static struct linked_list *tr_expr_LESS_EQUALS(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Operator <= not supported except as top level of 'if' condition. Sorry!\n");
	exit(EXIT_FAILURE);
}

static struct linked_list *tr_expr_GREATER(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Operator > not supported except as top level of 'if' condition. Sorry!\n");
	exit(EXIT_FAILURE);
}

static struct linked_list *tr_expr_GREATER_EQUALS(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Operator >= not supported except as top level of 'if' condition. Sorry!\n");
	exit(EXIT_FAILURE);
}

static struct linked_list *tr_expr_EQUALS_EQUALS(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Operator == not supported except as top level of 'if' condition. Sorry!\n");
	exit(EXIT_FAILURE);
}

static struct linked_list *tr_expr_NOT_EQUALS(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Operator != not supported except as top level of 'if' condition. Sorry!\n");
	exit(EXIT_FAILURE);
}

static struct linked_list *tr_expr_COMMA(struct ast_expr_node *expr)
{
	struct linked_list *L = tr_expr(expr->u.binary.left);
	ll_concat_and_free(L, tr_expr(expr->u.binary.right));
	return L;
}

static struct linked_list *tr_expr_ASSIGN(struct ast_expr_node *expr)
{
	/* don't forget to mirror changes in tr_stmt_COMPOUND */
	struct linked_list *L = tr_expr(expr->u.assign.value);
	push_ASSIGN(L, expr->symtab_context, expr->u.assign.id, ll_tail_ptr(L));
	return L;
}

static struct linked_list *tr_expr_PLUS_ASSIGN(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	struct line_desc *lhs = push_SYMTAB(L, expr->symtab_context,
			expr->u.assign.id);
	ll_concat_and_free(L, tr_expr(expr->u.assign.value));
	struct line_desc *rhs = ll_tail_ptr(L);
	struct line_desc *sum = push_ADD(L, lhs, rhs);
	push_ASSIGN(L, expr->symtab_context, expr->u.assign.id, sum);
	return L;
}

static struct linked_list *tr_expr_MINUS_ASSIGN(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	struct line_desc *lhs = push_SYMTAB(L, expr->symtab_context,
			expr->u.assign.id);
	ll_concat_and_free(L, tr_expr(expr->u.assign.value));
	struct line_desc *rhs = ll_tail_ptr(L);
	struct line_desc *difference = push_SUBTRACT(L, lhs, rhs);
	push_ASSIGN(L, expr->symtab_context, expr->u.assign.id, difference);
	return L;
}

static struct linked_list *tr_expr_STAR_ASSIGN(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	struct line_desc *lhs = push_SYMTAB(L, expr->symtab_context,
			expr->u.assign.id);
	ll_concat_and_free(L, tr_expr(expr->u.assign.value));
	struct line_desc *rhs = ll_tail_ptr(L);
	struct line_desc *product = push_MULTIPLY(L, lhs, rhs);
	push_ASSIGN(L, expr->symtab_context, expr->u.assign.id, product);
	return L;
}

static struct linked_list *tr_expr_SLASH_ASSIGN(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	struct line_desc *lhs = push_SYMTAB(L, expr->symtab_context,
			expr->u.assign.id);
	ll_concat_and_free(L, tr_expr(expr->u.assign.value));
	struct line_desc *rhs = ll_tail_ptr(L);
	struct line_desc *quotient;
	if (ast_type_is_integer(expr->type))
		quotient = push_INTEGER_DIVIDE(L, lhs, rhs);
	else
		quotient = push_DIVIDE(L, lhs, rhs);
	push_ASSIGN(L, expr->symtab_context, expr->u.assign.id, quotient);
	return L;
}

static struct linked_list *tr_expr_MODULO_ASSIGN(struct ast_expr_node *expr)
{
	struct linked_list *L = ll_new();
	struct line_desc *lhs = push_SYMTAB(L, expr->symtab_context,
			expr->u.assign.id);
	ll_concat_and_free(L, tr_expr(expr->u.assign.value));
	struct line_desc *rhs = ll_tail_ptr(L);
	struct line_desc *quotient = push_INTEGER_DIVIDE(L, lhs, rhs);
	struct line_desc *product = push_MULTIPLY(L, rhs, quotient);
	struct line_desc *remainder = push_SUBTRACT(L, lhs, product);
	push_ASSIGN(L, expr->symtab_context, expr->u.assign.id, remainder);
	return L;
}

static struct linked_list *tr_expr_FUNCTION_CALL(struct ast_expr_node *expr)
{
	struct symtab_node * const context = expr->symtab_context;
	char * const id = expr->u.fn_call.id;
	struct symtab_entry *entry = symtab_search(context, id);
	assert(entry != 0);  assert(entry->type->meta == TYPE_FN);
	struct linked_list *L = ll_new(), *arg_list = ll_new();
	struct ll_iter *iter = ll_iterator(expr->u.fn_call.args);
	int next_argno = 1;
	while (ll_has_next(iter)) {
		ll_concat_and_free(L, tr_expr(ll_next_ptr(iter)));
		decorate(push_FN_ARG(arg_list, ll_tail_ptr(L), context, id, next_argno),
				entry->first_arg_qtyid + (next_argno - 1), 0, 0);
		++next_argno;
	}
	ll_iterator_free(iter);
	push_BEGIN_CALL_BLOCK(L);
	push_FN_CALL(L, context, id);
	ll_concat_and_free(L, arg_list);
	if (!ast_type_is_void(entry->type->u.fn.ret))
		decorate(push_FN_RET(L, context, id), entry->return_qtyid, 0, 0);
	push_END_CALL_BLOCK(L);
	return L;
}

/* ========================================================================= */

static void print_args(const char *arg_format, struct linked_list *args)
{
	struct ll_iter *iter = ll_iterator(args);
	for (const char *c = arg_format; *c != '\0'; ++c) {
		switch (*c) {
			case 'i':
				assert(ll_has_next(iter));
				printf("%d", ll_next_int(iter));
				break;
			case 'f':
				assert(ll_has_next(iter));
				printf("%f", ll_next_float(iter));
				break;
			case 'p':
				assert(ll_has_next(iter));
				printf("%p", ll_next_ptr(iter));
				break;
			case 's':
				assert(ll_has_next(iter));
				printf("\"%s\"", (char *)ll_next_ptr(iter));
				break;
			default:
				printf("%c", *c);
				break;
		}
	}
	assert(!ll_has_next(iter));
	ll_iterator_free(iter);
}

void print_translated_line_list(const char *prefix, struct linked_list *L)
{
	struct ll_iter *iter = ll_iterator(L);
	while (ll_has_next(iter)) {
		struct line_desc *line = ll_next_ptr(iter);
		void *v = line;
		printf("%s", prefix);
		if (line->lineno.form != 0) {
			const char *form_name = line->lineno.form->name;
			printf("[%s] ", form_name ? form_name : "");
		}
		printf("%p: ", v);
		switch (line->op) {
			case OP_DOLLARS_CONSTANT:
				print_args("OP_DOLLARS_CONSTANT i", line->args);
				break;
			case OP_CENTS_CONSTANT:
				print_args("OP_CENTS_CONSTANT f", line->args);
				break;
			case OP_COPY:
				print_args("OP_COPY p", line->args);
				break;
			case OP_COPY_FROM_ATTACHED:
				print_args("OP_COPY_FROM_ATTACHED p", line->args);
				break;
			case OP_ADD:
				print_args("OP_ADD p, p", line->args);
				break;
			case OP_SUBTRACT:
				print_args("OP_SUBTRACT p, p", line->args);
				break;
			case OP_MULTIPLY:
				print_args("OP_MULTIPLY p, p", line->args);
				break;
			case OP_DIVIDE:
				print_args("OP_DIVIDE p, p", line->args);
				break;
			case OP_INTEGER_DIVIDE:
				print_args("OP_INTEGER_DIVIDE p, p", line->args);
				break;
			case OP_NEGATE:
				print_args("OP_NEGATE p", line->args);
				break;
			case OP_IF_ZERO_GOTO:
				print_args("OP_IF_ZERO_GOTO p, p", line->args);
				break;
			case OP_IF_NONZERO_GOTO:
				print_args("OP_IF_NONZERO_GOTO p, p", line->args);
				break;
			case OP_IF_POSITIVE_GOTO:
				print_args("OP_IF_POSITIVE_GOTO p, p", line->args);
				break;
			case OP_IF_NEGATIVE_GOTO:
				print_args("OP_IF_NEGATIVE_GOTO p, p", line->args);
				break;
			case OP_IF_NONNEGATIVE_GOTO:
				print_args("OP_IF_NONNEGATIVE_GOTO p, p", line->args);
				break;
			case OP_IF_NONPOSITIVE_GOTO:
				print_args("OP_IF_NONPOSITIVE_GOTO p, p", line->args);
				break;
			case OP_IF_CHECKED_COPY_ELSE_COPY:
				print_args("OP_IF_CHECKED_COPY_ELSE_COPY p, p, p",line->args);
				break;
			case OP_NOP:
				printf("OP_NOP");
				break;
			case OP_STOP:
				printf("OP_STOP");
				break;
			case OP_BEGIN_CALL_BLOCK:
				printf("OP_BEGIN_CALL_BLOCK");
				break;
			case OP_CALL:
				print_args("OP_CALL p", line->args);
				break;
			case OP_END_CALL_BLOCK:
				printf("OP_END_CALL_BLOCK");
				break;
			case OP_ASSIGN:
				print_args("OP_ASSIGN p, s, p", line->args);
				break;
			case OP_SYMTAB:
				print_args("OP_SYMTAB p, s", line->args);
				break;
			case OP_RECONCILE:
				print_args("OP_RECONCILE p, s, p, p", line->args);
				break;
			case OP_GOTO:
				printf("OP_GOTO");  /* no args; uses goto_after */
				break;
			case OP_FN_ARG:
				print_args("OP_FN_ARG p, p, s, i", line->args);
				break;
			case OP_FN_CALL:
				print_args("OP_FN_CALL p, s", line->args);
				break;
			case OP_FN_RET:
				print_args("OP_FN_RET p, s", line->args);
				break;
			default:
				printf("Unknown opcode: %lu", (unsigned long)(line->op));
				break;
		}
		if (line->qtyid != 0)  printf("; qtyid = %d", line->qtyid);
		if (line->enter_also.lineptr != 0)
			printf("; enter_also = %p", (void *)line->enter_also.lineptr);
		if (line->goto_after.lineptr != 0)
			printf("; goto_after = %p", (void *)line->goto_after.lineptr);
		printf("\n");
	}
	ll_iterator_free(iter);
}
