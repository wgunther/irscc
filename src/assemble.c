#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "assemble.h"
#include "formdesc.h"
#include "llist.h"
#include "symtab.h"
#include "utility.h"

/* Check various assertions about the lists of instructions we are given. */
static void check_preconditions(struct form_desc *form);

/* Preliminary run through symbol table operations to identify OP_SYMTAB lines
   that refer to uninitialized variables. Sets all line numbers to 0, except
   line numbers for OP_ASSIGN lines that refer to *initialized* variables and
   OP_SYMTAB lines that refer to *uninitialized* variables are set to -1. Also
   finds OP_COPY lines that attempt to read uninitialized variables and changes
   them to OP_DOLLARS_CONSTANT lines. (This can happen, for example, when an
   externally visible variable is both written and read within the body of a
   loop, but hasn't been initialized before the loop begins; the OP_COPY that
   passes the value of this variable to the subform should be removed.) */
static void preliminary_symtab_run(struct form_desc *form);

/* Change OP_GOTO instructions to OP_NOP, and transfer goto_after decoration to
   the preceding (non-intermediate) instruction when possible. */
static void hoist_gotos(struct form_desc *form);

/* Assign line numbers and insert section breaks. */
static void assign_line_numbers(struct form_desc *form);

/* Resolve symbol table references. Along the way, assign "line numbers" to
   OP_ASSIGN and OP_SYMTAB lines: the line numbers of the values these lines
   refer to. */
static void resolve_symtab(struct form_desc *form);

/* Resolve pointers. */
static void resolve_pointers(struct form_desc *form);

/* Remove intermediate instructions and OP_NEW_SECTION instructions that are
   too near the end of the form. */
static void remove_intermediate_instr(struct form_desc *form);

/* ========================================================================= */

void assemble(struct linked_list *L)
{
	/* check preconditions */
	struct ll_iter *iter = ll_iterator(L);
	while (ll_has_next(iter))  check_preconditions(ll_next_ptr(iter));
	ll_iterator_free(iter);

	/* preliminary symbol table run */
	iter = ll_iterator(L);
	while (ll_has_next(iter))  preliminary_symtab_run(ll_next_ptr(iter));
	ll_iterator_free(iter);

	/* replace OP_GOTO with OP_NOP */
	iter = ll_iterator(L);
	while (ll_has_next(iter))  hoist_gotos(ll_next_ptr(iter));
	ll_iterator_free(iter);

	/* assign line numbers */
	iter = ll_iterator(L);
	while (ll_has_next(iter))  assign_line_numbers(ll_next_ptr(iter));
	ll_iterator_free(iter);

	/* resolve symbol table references */
	iter = ll_iterator(L);
	while (ll_has_next(iter))  resolve_symtab(ll_next_ptr(iter));
	ll_iterator_free(iter);

	/* resolve pointers */
	iter = ll_iterator(L);
	while (ll_has_next(iter))  resolve_pointers(ll_next_ptr(iter));
	ll_iterator_free(iter);

	/* remove intermediate instructions */
	iter = ll_iterator(L);
	while (ll_has_next(iter))  remove_intermediate_instr(ll_next_ptr(iter));
	ll_iterator_free(iter);
}

/* ========================================================================= */

static void check_preconditions(struct form_desc *form)
{
	struct ll_iter *iter = ll_iterator(form->lines);
	while (ll_has_next(iter)) {
		struct line_desc * const line = ll_next_ptr(iter);
		switch (line->op) {
			case OP_IF_ZERO_GOTO:
			case OP_IF_NONZERO_GOTO:
			case OP_IF_POSITIVE_GOTO:
			case OP_IF_NEGATIVE_GOTO:
			case OP_IF_NONNEGATIVE_GOTO:
			case OP_IF_NONPOSITIVE_GOTO:
			{
				assert(ll_size(line->args) == 2);
				struct line_desc * const jump_target = ll_tail_ptr(line->args);
				assert(jump_target->is_jump_target);
				break;
			}
			/* OP_GOTO uses goto_after, not args */
			default:
				break;
		}
		if (line->goto_after.lineptr != 0)
			assert(line->goto_after.lineptr->is_jump_target);
	}
	ll_iterator_free(iter);
}

/* ========================================================================= */

static void preliminary_symtab_run(struct form_desc *form)
{
	struct ll_iter *iter = ll_iterator(form->lines);
	while (ll_has_next(iter)) {
		struct line_desc * const line = ll_next_ptr(iter);
		switch (line->op) {
			case OP_ASSIGN:
			case OP_SYMTAB:
			{
				assert(ll_size(line->args) == 2 + (line->op == OP_ASSIGN));
				struct ll_iter *arg_iter = ll_iterator(line->args);
				struct symtab_node *context = ll_next_ptr(arg_iter);
				const char *id = ll_next_ptr(arg_iter);
				ll_iterator_free(arg_iter);
				struct symtab_entry *entry = symtab_search(context, id);
				assert(entry != 0);
				if (line->op == OP_ASSIGN) {
					entry->lineno.line = -1;  /* mark as initialized */
					line->lineno.line = 0;
				} else {
					/* OP_SYMTAB with lineno.line == -1 means uninitialized */
					line->lineno.line = (entry->lineno.line < 0 ? 0 : -1);
				}
				break;
			}
			case OP_COPY:
			{
				assert(ll_size(line->args) == 1);
				struct line_desc * const from = ll_head_ptr(line->args);
				if (from->op == OP_SYMTAB && from->lineno.line < 0) {
					/* attempt to copy an uninitialized variable */
					line->op = OP_DOLLARS_CONSTANT;
					ll_shift_ptr(line->args);
					ll_push_int(line->args, 0);
				}
				break;
			}
			default:
				line->lineno.line = 0;
				break;
		}
	}
	ll_iterator_free(iter);
}

/* ========================================================================= */

static void hoist_gotos(struct form_desc *form)
{
	/* go backwards */
	struct ll_iter *iter = ll_reverse_iterator(form->lines);
	while (ll_has_next(iter)) {
		struct line_desc * const line = ll_next_ptr(iter);
		if (line->op == OP_GOTO) {
			assert(ll_size(line->args) == 0);
			assert(line->qtyid == 0);
			assert(line->enter_also.lineptr == 0);
			assert(line->goto_after.lineptr != 0);
			line->op = OP_NOP;
			if (line->is_jump_target)  continue;
			struct line_desc *move_to = 0;
			while (ll_has_next(iter)) {
				struct line_desc * const l = ll_next_ptr(iter);
				switch (l->op) {
					case OP_IF_ZERO_GOTO:
					case OP_IF_NONZERO_GOTO:
					case OP_IF_POSITIVE_GOTO:
					case OP_IF_NEGATIVE_GOTO:
					case OP_IF_NONNEGATIVE_GOTO:
					case OP_IF_NONPOSITIVE_GOTO:
						/* can't move goto_after to these instructions */
						goto END_MOVE_TO_LOOP;
					case OP_NOP:
						move_to = l;
						break;  /* out of switch; keep looking backward */
					case OP_STOP:
					case OP_BEGIN_CALL_BLOCK:
					case OP_GOTO:
						/* this doesn't make sense? */
						fprintf(stderr,"Weird thing happened in hoist_gotos\n");
						exit(EXIT_FAILURE);
					case OP_END_CALL_BLOCK:
					case OP_ASSIGN:
					case OP_SYMTAB:
						/* skip */
						break;  /* out of switch; keep looking backward */
					case OP_RECONCILE:
					{
						struct line_desc * const stash = ll_tail_ptr(l->args);
						assert(stash->op == OP_SYMTAB);
						if (stash->lineno.line >= 0) {
							move_to = l;
							goto END_MOVE_TO_LOOP;
						} else {
							/* refers to uninitialized var; will disappear */
							continue;
						}
					}
					default:
						move_to = l;
						goto END_MOVE_TO_LOOP;
				}
				if (l->is_jump_target)  /* can't move past a jump target */
					goto END_MOVE_TO_LOOP;
			}
END_MOVE_TO_LOOP:
			if (move_to != 0) {
				assert(move_to->goto_after.lineptr == 0);
				move_to->goto_after.lineptr = line->goto_after.lineptr;
				line->goto_after.lineptr = 0;
			}
		}
	}
	ll_iterator_free(iter);
}

/* ========================================================================= */

static int target_section_length(void)
{
	/* for now, just generate a uniformly random integer in [5, 12] */
	const int bucket_size = RAND_MAX / 8;
	int r;
	do {
		r = rand() / bucket_size;
	} while (r >= 8);
	return r + 5;
}

static void assign_line_numbers(struct form_desc *form)
{
	int prev_lineno;
	if (form->name != 0 && strcmp(form->name, "main") == 0)
		prev_lineno = 6;
	else
		prev_lineno = 0;
	int in_call_block = 0;
	struct line_desc *subline_a = 0;
	char prev_subline = '\0';
	int have_started_copy_from_attached = 0;
	int section_length = 0;
	int target_length = target_section_length();
	struct ll_iter *iter = ll_iterator(form->lines);
	while (ll_has_next(iter)) {
		if (!in_call_block && section_length >= target_length) {
			struct line_desc *newsec = checked_malloc(sizeof(*newsec));
			newsec->lineno.form = form;
			newsec->lineno.line = prev_lineno + 1;
			newsec->lineno.subline = '\0';
			newsec->op = OP_NEW_SECTION;
			newsec->args = ll_new();
			newsec->qtyid = 0;
			newsec->enter_also.lineno.form = 0;
			newsec->enter_also.lineno.line = 0;
			newsec->enter_also.lineno.subline = '\0';
			newsec->goto_after.lineno.form = 0;
			newsec->goto_after.lineno.line = 0;
			newsec->goto_after.lineno.subline = '\0';
			newsec->is_jump_target = 0;
			ll_insert_ptr_before(iter, newsec);
			section_length = 0;
			target_length = target_section_length();
		}
		struct line_desc * const line = ll_next_ptr(iter);
		line->lineno.form = form;
		switch (line->op) {
			case OP_ASSIGN:
			case OP_SYMTAB:
				/* don't change line->lineno.line here */
				line->lineno.subline = '\0';
				break;
			case OP_NOP:
				if (line->qtyid != 0)
					goto DEFAULT;
				line->lineno.line = prev_lineno + 1;  /* no increment */
				line->lineno.subline = '\0';
				break;
			case OP_BEGIN_CALL_BLOCK:
				line->lineno.line = prev_lineno + 1;  /* no increment */
				line->lineno.subline = '\0';
				in_call_block = 1;  subline_a = 0;  prev_subline = '\0';
				have_started_copy_from_attached = 0;
				break;
			case OP_CALL:
			case OP_FN_CALL:
				line->lineno.line = ++prev_lineno;
				line->lineno.subline = '\0';
				++section_length;
				break;
			case OP_END_CALL_BLOCK:
				line->lineno.line = prev_lineno;  /* no increment, no '+1' */
				line->lineno.subline = '\0';
				if (prev_subline == 'a') {
					assert(subline_a != 0);
					subline_a->lineno.subline = '\0';
				}
				in_call_block = 0;  subline_a = 0;  prev_subline = '\0';
				break;
			case OP_RECONCILE:
			{
				struct line_desc * const stash = ll_tail_ptr(line->args);
				if (stash->lineno.line >= 0) {
					/* this line will become OP_IF_CHECKED_COPY_ELSE_COPY */
					line->lineno.line = ++prev_lineno;
					line->lineno.subline = '\0';
					++section_length;
				} else {
					/* this line will become OP_ASSIGN */
					line->lineno.line = 0;
					line->lineno.subline = '\0';
				}
				break;
			}
			case OP_COPY_FROM_ATTACHED:
			case OP_FN_RET:
				if (!have_started_copy_from_attached) {
					/* reset subline counting */
					if (prev_subline == 'a') {
						assert(subline_a != 0);
						subline_a->lineno.subline = '\0';
					}
					subline_a = 0;  prev_subline = '\0';
					have_started_copy_from_attached = 1;
				}
				/* fall through */
			DEFAULT:
			default:
				if (in_call_block) {
					if (subline_a == 0) {
						subline_a = line;
						line->lineno.line = ++prev_lineno;
						prev_subline = line->lineno.subline = 'a';
					} else {
						line->lineno.line = prev_lineno;
						line->lineno.subline = ++prev_subline;
					}
				} else {
					line->lineno.line = ++prev_lineno;
					line->lineno.subline = '\0';
				}
				++section_length;
				break;
		}
	}
	ll_iterator_free(iter);
}

/* ========================================================================= */

static void resolve_symtab(struct form_desc *form)
{
	struct ll_iter *iter = ll_iterator(form->lines);
	while (ll_has_next(iter))
	{
		struct line_desc * const line = ll_next_ptr(iter);
		switch (line->op) {
			case OP_ASSIGN:
			{
				assert(ll_size(line->args) == 3);
				struct symtab_node *context = ll_shift_ptr(line->args);
				char *id = ll_shift_ptr(line->args);
				struct line_desc *value = ll_shift_ptr(line->args);
				assert(value->lineno.line > 0);
				struct symtab_entry *entry = symtab_search(context, id);
				assert(entry != 0);
				line->lineno = entry->lineno = value->lineno;
				assert(line->lineno.line > 0);
				free(id);
				break;
			}
			case OP_SYMTAB:
			{
				assert(ll_size(line->args) == 2);
				struct symtab_node *context = ll_shift_ptr(line->args);
				char *id = ll_shift_ptr(line->args);
				struct symtab_entry *entry = symtab_search(context, id);
				assert(entry != 0);
				line->lineno = entry->lineno;
				/* N.B.: line->lineno.line may be 0 here. */
				free(id);
				break;
			}
			case OP_RECONCILE:
			{
				assert(ll_size(line->args) == 4);
				struct symtab_node *context = ll_shift_ptr(line->args);
				char *id = ll_shift_ptr(line->args);
				struct line_desc *checkbox = ll_shift_ptr(line->args);
				struct line_desc *stash = ll_shift_ptr(line->args);
				assert(checkbox->lineno.line > 0);
				struct symtab_entry *entry = symtab_search(context, id);
				assert(entry != 0);
				assert(entry->lineno.line > 0);
				if (stash->lineno.line > 0) {
					line->op = OP_IF_CHECKED_COPY_ELSE_COPY;
					ll_push_lineno(line->args, checkbox->lineno);
					if (line->qtyid) {  /* kludge: reversed meaning */
						ll_push_lineno(line->args, stash->lineno);
						ll_push_lineno(line->args, entry->lineno);
						line->qtyid = 0;
					} else {
						ll_push_lineno(line->args, entry->lineno);
						ll_push_lineno(line->args, stash->lineno);
					}
				} else {
					/* The line number of the stash is zero, which means the
					   variable was uninitialized when it was stashed. It would
					   be undefined behavior to access this stashed value, so
					   we can assume that the value of the variable at this
					   point in the code is the amount on the line specified by
					   the symbol table entry. */
					line->op = OP_ASSIGN;
					line->lineno = entry->lineno;
				}
				free(id);
				break;
			}
			default:  /* nothing */ ;
		}
	}
	ll_iterator_free(iter);
}

/* ========================================================================= */

static void resolve_pointers(struct form_desc *form)
{
	struct ll_iter *iter = ll_iterator(form->lines);
	while (ll_has_next(iter))
	{
		struct line_desc * const line = ll_next_ptr(iter);
		switch (line->op) {
			case OP_DOLLARS_CONSTANT:
			case OP_CENTS_CONSTANT:
			case OP_NOP:
			case OP_STOP:
			case OP_BEGIN_CALL_BLOCK:
			case OP_CALL:
			case OP_END_CALL_BLOCK:
			case OP_NEW_SECTION:
				/* nothing to be done */
				break;
			case OP_COPY:
			case OP_COPY_FROM_ATTACHED:
			case OP_NEGATE:
			{	/* unary */
				assert(ll_size(line->args) == 1);
				struct line_desc *arg = ll_shift_ptr(line->args);
				if (arg->lineno.line <= 0)  printf("oops! %p\n", (void *)line);
				assert(arg->lineno.line > 0);
				ll_push_lineno(line->args, arg->lineno);
				break;
			}
			case OP_ADD:
			case OP_SUBTRACT:
			case OP_MULTIPLY:
			case OP_DIVIDE:
			case OP_INTEGER_DIVIDE:
			case OP_IF_ZERO_GOTO:
			case OP_IF_NONZERO_GOTO:
			case OP_IF_POSITIVE_GOTO:
			case OP_IF_NEGATIVE_GOTO:
			case OP_IF_NONNEGATIVE_GOTO:
			case OP_IF_NONPOSITIVE_GOTO:
			{	/* binary */
				assert(ll_size(line->args) == 2);
				struct line_desc *left = ll_shift_ptr(line->args);
				struct line_desc *right = ll_shift_ptr(line->args);
				assert(left->lineno.line > 0);  assert(right->lineno.line > 0);
				ll_push_lineno(line->args, left->lineno);
				ll_push_lineno(line->args, right->lineno);
				break;
			}
			case OP_ASSIGN:
			case OP_SYMTAB:
			case OP_IF_CHECKED_COPY_ELSE_COPY:
				/* already handled in resolve_symtab */
				break;
			/* no case for OP_RECONCILE: shouldn't exist here */
			case OP_FN_ARG:
			{
				assert(ll_size(line->args) == 4);
				struct line_desc *value = ll_shift_ptr(line->args);
				struct symtab_node *context = ll_shift_ptr(line->args);
				char *id = ll_shift_ptr(line->args);
				int argno = ll_shift_int(line->args);
				assert(value->lineno.line > 0);
				struct symtab_entry *entry = symtab_search(context, id);
				assert(entry != 0);  assert(entry->form != 0);
				line->op = OP_COPY;
				ll_push_lineno(line->args, value->lineno);
				line->qtyid = entry->first_arg_qtyid + (argno - 1);
				struct line_number enter_also = { entry->form, argno, '\0' };
				line->enter_also.lineno = enter_also;
				continue;  /* skip enter_also (and goto_after) stuff */
			}
			case OP_FN_CALL:
			{
				assert(ll_size(line->args) == 2);
				struct symtab_node *context = ll_shift_ptr(line->args);
				char *id = ll_shift_ptr(line->args);
				struct symtab_entry *entry = symtab_search(context, id);
				assert(entry != 0);  assert(entry->form != 0);
				line->op = OP_CALL;
				ll_push_ptr(line->args, entry->form);
				break;
			}
			case OP_FN_RET:
			{
				assert(ll_size(line->args) == 2);
				struct symtab_node *context = ll_shift_ptr(line->args);
				char *id = ll_shift_ptr(line->args);
				struct symtab_entry *entry = symtab_search(context, id);
				assert(entry != 0);  assert(entry->ret != 0);
				assert(entry->ret->lineno.line > 0);
				line->op = OP_COPY_FROM_ATTACHED;
				ll_push_lineno(line->args, entry->ret->lineno);
				line->qtyid = entry->return_qtyid;
				break;
			}
			default:  /* this shouldn't happen */
				fprintf(stderr, "Forgot a case in resolve_pointers: %u\n",
						(unsigned int)line->op);
				exit(EXIT_FAILURE);
		}
		if (line->enter_also.lineptr != 0) {
			struct line_desc *enter_also = line->enter_also.lineptr;
			assert(enter_also->lineno.line > 0);
			line->enter_also.lineno = enter_also->lineno;
		} else {
			line->enter_also.lineno.form = 0;
			line->enter_also.lineno.line = 0;
			line->enter_also.lineno.subline = '\0';
		}
		if (line->goto_after.lineptr != 0) {
			struct line_desc *goto_after = line->goto_after.lineptr;
			assert(goto_after->lineno.line > 0);
			line->goto_after.lineno = goto_after->lineno;
		} else {
			line->goto_after.lineno.form = 0;
			line->goto_after.lineno.line = 0;
			line->goto_after.lineno.subline = '\0';
		}
	}
	ll_iterator_free(iter);
}

/* ========================================================================= */

static void remove_intermediate_instr(struct form_desc *form)
{
	/* go backwards! */
	struct ll_iter *iter = ll_reverse_iterator(form->lines);
	int instructions_after = 0;
	while (ll_has_next(iter)) {
		struct line_desc * const line = ll_this_ptr(iter);
		if (line->op == OP_ASSIGN || line->op == OP_SYMTAB
			|| (line->op == OP_NOP && line->qtyid == 0
				&& line->goto_after.lineno.form == 0 && instructions_after)
			|| (line->op == OP_NEW_SECTION && instructions_after < 5))
		{
			assert(ll_size(line->args) == 0);
			assert(line->qtyid == 0);
			assert(line->enter_also.lineno.form == 0);
			assert(line->goto_after.lineno.form == 0);
			ll_free(line->args);
			free(line);
			ll_remove(iter);
		} else {
			assert(line->op != OP_RECONCILE);
			assert(line->op != OP_GOTO);
			ll_next_ptr(iter);
			++instructions_after;
		}
	}
	ll_iterator_free(iter);
}

/* ========================================================================= */

void print_lineno(struct line_number lineno, const struct form_desc *form)
{
	if (lineno.form == 0) {
		printf("---");
		return;
	} else if (lineno.form != form) {
		if (lineno.form->name != 0)
			printf("%s:", lineno.form->name);
		else
			printf("%p:", (void *)lineno.form);
	}
	printf("%d", lineno.line);
	if (lineno.subline != '\0')
		printf("%c", lineno.subline);
}

static void print_args(const char *arg_format, struct linked_list *args,
		struct form_desc *form)
{
	struct ll_iter *iter = ll_iterator(args);
	for (const char *c = arg_format; *c != '\0'; ++c) {
		switch (*c) {
			case 'i':
				assert(ll_has_next(iter));
				printf("%d", ll_next_int(iter));
				break;
			case 'f':
				assert(ll_has_next(iter));
				printf("%f", ll_next_float(iter));
				break;
			case 'l':
				assert(ll_has_next(iter));
				print_lineno(ll_next_lineno(iter), form);
				break;
			case 'p':
				assert(ll_has_next(iter));
				printf("%p", ll_next_ptr(iter));
				break;
			default:
				printf("%c", *c);
				break;
		}
	}
	assert(!ll_has_next(iter));
	ll_iterator_free(iter);
}

void print_assembled_line_list(const char *prefix, struct linked_list *L)
{
	struct ll_iter *iter = ll_iterator(L);
	while (ll_has_next(iter)) {
		struct line_desc *line = ll_next_ptr(iter);
		struct form_desc *form = (struct form_desc *)line->lineno.form;
		printf("%s", prefix);
		print_lineno(line->lineno, 0);
		printf(": ");
		switch (line->op) {
			case OP_DOLLARS_CONSTANT:
				print_args("OP_DOLLARS_CONSTANT i", line->args, form);
				break;
			case OP_CENTS_CONSTANT:
				print_args("OP_CENTS_CONSTANT f", line->args, form);
				break;
			case OP_COPY:
				print_args("OP_COPY l", line->args, form);
				break;
			case OP_COPY_FROM_ATTACHED:
				print_args("OP_COPY_FROM_ATTACHED l", line->args, form);
				break;
			case OP_ADD:
				print_args("OP_ADD l, l", line->args, form);
				break;
			case OP_SUBTRACT:
				print_args("OP_SUBTRACT l, l", line->args, form);
				break;
			case OP_MULTIPLY:
				print_args("OP_MULTIPLY l, l", line->args, form);
				break;
			case OP_DIVIDE:
				print_args("OP_DIVIDE l, l", line->args, form);
				break;
			case OP_INTEGER_DIVIDE:
				print_args("OP_INTEGER_DIVIDE l, l", line->args, form);
				break;
			case OP_NEGATE:
				print_args("OP_NEGATE l", line->args, form);
				break;
			case OP_IF_ZERO_GOTO:
				print_args("OP_IF_ZERO_GOTO l, l", line->args, form);
				break;
			case OP_IF_NONZERO_GOTO:
				print_args("OP_IF_NONZERO_GOTO l, l", line->args, form);
				break;
			case OP_IF_POSITIVE_GOTO:
				print_args("OP_IF_POSITIVE_GOTO l, l", line->args, form);
				break;
			case OP_IF_NEGATIVE_GOTO:
				print_args("OP_IF_NEGATIVE_GOTO l, l", line->args, form);
				break;
			case OP_IF_NONNEGATIVE_GOTO:
				print_args("OP_IF_NONNEGATIVE_GOTO l, l", line->args, form);
				break;
			case OP_IF_NONPOSITIVE_GOTO:
				print_args("OP_IF_NONPOSITIVE_GOTO l, l", line->args, form);
				break;
			case OP_IF_CHECKED_COPY_ELSE_COPY:
				print_args("OP_IF_CHECKED_COPY_ELSE_COPY l, l, l", line->args,
						form);
				break;
			case OP_NOP:
				printf("OP_NOP");
				break;
			case OP_STOP:
				printf("OP_STOP");
				break;
			case OP_BEGIN_CALL_BLOCK:
				printf("OP_BEGIN_CALL_BLOCK");
				break;
			case OP_CALL:
				print_args("OP_CALL p", line->args, form);
				break;
			case OP_END_CALL_BLOCK:
				printf("OP_END_CALL_BLOCK");
				break;
			case OP_NEW_SECTION:
				printf("OP_NEW_SECTION");
				break;
			default:
				printf("Unknown opcode: %lu", (unsigned long)(line->op));
				break;
		}
		if (line->qtyid != 0)
			printf("; qtyid = %d", line->qtyid);
		if (line->goto_after.lineno.form != 0) {
			printf("; goto_lineno = ");
			print_lineno(line->goto_after.lineno, form);
		}
		if (line->enter_also.lineno.form != 0) {
			printf("; enter_also = ");
			print_lineno(line->enter_also.lineno, form);
		}
		printf("\n");
	}
	ll_iterator_free(iter);
}
