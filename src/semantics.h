#ifndef IRSCC_SEMANTICS_H
#define IRSCC_SEMANTICS_H

#include "ast.h"

#if defined(__cplusplus)
extern "C" {
#endif

/* WARNING: these functions are not thread-safe */

void sem_init(void);
void sem_release(void);
void sem_fn(struct ast_fn_node *fn);
void sem_decl(struct ast_decl_node *decl);
void sem_idxr(struct ast_idxr_node *idxr, spec_t specifiers);
struct ast_type *sem_dxr(struct ast_dxr_node *dxr, struct ast_type *outer);
void sem_stmt(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
void sem_expr(struct ast_expr_node *expr);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_SEMANTICS_H */
