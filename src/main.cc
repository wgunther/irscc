#include <sstream>
#include <iomanip>
#include <fstream>
#include "formElements.hh"
#include "latexConstants.hh"
#include "controller.hh"
#include "formdesc.h"
#include "compiler.h"
#include "llist.h"
#include "pstream.h"
using std::ostringstream;
using std::cerr; using std::endl;
string branchWording (const LineNumber& compare, const string& comparisonWording, const bool& continueOnSuccess, const LineNumber& whereTo) ;

int main (int argc, char** args) {
	Controller c{argc, args};
	// Settle input file:
	FILE *inFile = fopen(c.inputFile().c_str(), "r");
	if (inFile == NULL) {
		cerr << "File cannot be opened for some reason." << endl;
		cerr << "If you are trying to commit tax fraud, please stop." << endl;
		exit(1);
	}
	linked_list* forms = compile(inFile, c.isVerbose());
	fclose(inFile);
	ostream *out;
	string name = c.outputFile();
	if (c.toTex()) {
		if (name.find_last_of(".pdf") == name.length()-1) {
			name.erase(name.end()-4,name.end());
		}
		auto o = new redi::opstream {};
		o->open("pdftex --jobname " + name + ">/dev/null");
		if (!o->is_open()) {
			std::cerr << "The IRS requires a PDFTeX distribution." << std::endl;
			std::cerr << "(run with -c flag to get TeX output)" << std::endl;
			exit(1);
		}
		out = o;
	}
	else {
		auto o = new std::ofstream;
		o->open(name);
		out = o;
	}
	FormDesc::initalizeAvailableIdentifiers();
	latexConstant::headerOut(*out) << std::endl;
	auto formItr = ll_iterator(forms);
	while (ll_has_next(formItr)) { // Looping over forms.
		form_desc *formInfo = (form_desc*)ll_next_ptr(formItr);
		unique_ptr<Form> form;
		// Determine schedule/form/form1040
		if (formInfo->parent != NULL) {
			form.reset(new ScheduleNormal{formInfo});
		}
		else {
			string formName{formInfo->name};
			if (formName == "main")
				form.reset(new Form1040{formInfo});
			else 
				form.reset(new FormNormal{formInfo}); 
				form->setTitle("Net " + capitalizeFirst((string)formInfo->name));
		}
		// Use the natural section of form.
		FormSection *section = form->getSec();
		form->addSec(section);
		auto opItr = ll_iterator(formInfo->lines); // "row" iterator
		while (ll_has_next(opItr)) { // Looping over "rows"
			auto lineDescription = LineDescription{(line_desc*)ll_next_ptr(opItr),formInfo};
			ostringstream s;
			auto itr = ll_iterator(lineDescription->args);
			switch (lineDescription->op) {
				case OP_ADD:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto augend = LineNumber{ll_next_lineno(itr)} ;
					auto addend = LineNumber{ll_next_lineno(itr)};
					s << "Add " << augend << " to " << addend;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_SUBTRACT:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto minuend = LineNumber{ll_next_lineno(itr)};
					auto subtrahend = LineNumber{ll_next_lineno(itr)};
					s << "Subtract " << subtrahend << " from " << minuend;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_MULTIPLY:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto multipland = LineNumber{ll_next_lineno(itr)};
					auto multiplier = LineNumber{ll_next_lineno(itr)};
					s << "Multiply " << multipland << " by " << multiplier;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_DIVIDE:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto dividend = LineNumber{ll_next_lineno(itr)};
					auto divisor = LineNumber{ll_next_lineno(itr)};
					s << "Divide " << dividend << " by " << divisor;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_INTEGER_DIVIDE:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto dividend = LineNumber{ll_next_lineno(itr)};
					auto divisor = LineNumber{ll_next_lineno(itr)};
					s << "Divide " << dividend << " by  " << divisor
							<< ". Round down to the nearest dollar";
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_NEGATE:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto negatend = LineNumber{ll_next_lineno(itr)};
					s << "Negate " << negatend;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_COPY:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto lineInfo = LineNumber{ll_next_lineno(itr)};
					s << "Enter the amount from " << lineInfo;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_COPY_FROM_ATTACHED:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					auto lineInfo = LineNumber{ll_next_lineno(itr)};
					s << "Enter the amount from "; 
					if (form->F == lineInfo->form)
						s << "the supplemental copy of ";	
					s << lineInfo.longNumber();
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_DOLLARS_CONSTANT:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					int dollar = ll_next_int(itr);
					s << R"(Enter )";
					if (dollar == 0)
						s << "--0-- ";
					else
						s << "the amount \\$" << dollar;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_CENTS_CONSTANT:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					float cents = ll_next_float(itr);
					s << R"(Enter the amount \$)" << std::setprecision(2) << std::fixed << cents;
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_IF_ZERO_GOTO:
				{
					DisplayRow *row = new DisplayRow {lineDescription, formInfo};
					row->setMessage(branchWording(LineNumber{ll_next_lineno(itr)},"is zero",false,LineNumber{ll_next_lineno(itr)}));
					section->addElement(row);
					break;
				}
				case OP_IF_NONZERO_GOTO:
				{
					DisplayRow *row = new DisplayRow {lineDescription, formInfo};
					row->setMessage(branchWording(LineNumber{ll_next_lineno(itr)},"is zero",true,LineNumber{ll_next_lineno(itr)}));
					section->addElement(row);
					break;
				}
				case OP_IF_POSITIVE_GOTO:
				{
					DisplayRow *row = new DisplayRow {lineDescription, formInfo};
					row->setMessage(branchWording(LineNumber{ll_next_lineno(itr)},"is a loss or zero",true,LineNumber{ll_next_lineno(itr)}));
					section->addElement(row);
					break;
				}
				case OP_IF_NEGATIVE_GOTO:
				{
					DisplayRow *row = new DisplayRow {lineDescription, formInfo};
					row->setMessage(branchWording(LineNumber{ll_next_lineno(itr)},"is a profit or zero",true,LineNumber{ll_next_lineno(itr)}));
					section->addElement(row);
					break;
				}
				case OP_IF_NONNEGATIVE_GOTO:
				{
					DisplayRow *row = new DisplayRow {lineDescription, formInfo};
					row->setMessage(branchWording(LineNumber{ll_next_lineno(itr)},"is a profit or zero",false,LineNumber{ll_next_lineno(itr)}));
					section->addElement(row);
					break;
				}
				case OP_IF_NONPOSITIVE_GOTO:
				{
					DisplayRow *row = new DisplayRow {lineDescription, formInfo};
					row->setMessage(branchWording(LineNumber{ll_next_lineno(itr)},"is a loss or zero",false,LineNumber{ll_next_lineno(itr)}));
					section->addElement(row);
					break;
				}
				case OP_IF_CHECKED_COPY_ELSE_COPY:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					s << "If " << LineNumber{ll_next_lineno(itr)}
						<< " is checked, enter the amount from "
						<< LineNumber{ll_next_lineno(itr)}
						<< R"(. Otherwise, enter the amount from )"
						<< LineNumber{ll_next_lineno(itr)};
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_BEGIN_CALL_BLOCK:
				{
					FunctionArgsToggle *command = new FunctionArgsToggle{lineDescription, formInfo,true};
					section->addElement(command);
					break;	
				}
				case OP_END_CALL_BLOCK:
				{
					FunctionArgsToggle *command = new FunctionArgsToggle{lineDescription, formInfo,false};
					section->addElement(command);
					break;	
				}
				case OP_NEW_SECTION:
				{
					section = form->getSec();
					form->addSec(section);
					break;
				}
				case OP_CALL:
				{
					auto callingForm = FormDesc{(const form_desc*)ll_next_ptr(itr)};
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					s << "Enter the next available attachment sequence number. Attach ";
					if (form->F == callingForm)
						s << "a supplemental copy of ";
					s << callingForm << " using this attachment sequence number (see instructions)";
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				case OP_STOP:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					s << R"({\bf STOP.} Do not complete any more of this form, and return to the form you were working on)";
					row->setMessage(s.str());
					section->addElement(row);
					break;

				}
				case OP_NOP:
				{
					ExpressionRow *row = new ExpressionRow {lineDescription, formInfo};
					if (! lineDescription.isLabeled())
						s << R"(If you're pretty clap your hands)";
					row->setMessage(s.str());
					section->addElement(row);
					break;
				}
				default:
				{
					cerr << "If this doesn't work out like you expected, it's probably our fault, not yours" << endl;
					cerr << "(btw Brian, recieved OP code with integer value " << (int)lineDescription->op << ")" << endl;
				}
			}
			ll_iterator_free(itr);
		}
		ll_iterator_free(opItr);
		*out << *form;
	}
	ll_iterator_free(formItr);

	latexConstant::footerOut(*out) << std::endl;

	delete out;
	form_desc_list_free(forms);

	if (c.toTex()) { // Clean up log
		name.append(".log");
		remove(name.c_str());
	}
}

string branchWording (const LineNumber& compare, const string& comparisonWording, const bool& continueOnSuccess, const LineNumber& whereTo) {
	ostringstream s;
	static const string checkAndContinue{R"(check this box and continue to the next line\DotFill\checkbox)"};
	s << "If " << compare << " " << comparisonWording << ", ";
	if (continueOnSuccess) {
		s << checkAndContinue << R"(\par\noindent )";
		s << "Otherwise, go to " << whereTo;
	}
	else {
		s << "go to " << whereTo << R"(.\par\noindent )";
		s << "Otherwise, " << checkAndContinue;
	}
	return s.str();
}
