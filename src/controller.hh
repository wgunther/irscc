#include <vector>
#include <string>
#include <utility>
#include <iostream>
using std::vector;
using std::string;

class Controller {
	static const string defaultOutTex;
	static const string defaultOutPDF;
	struct flags {
		string in;
		string out;
		unsigned int defOut : 1;
		unsigned int defIn : 1;
		unsigned int c : 1; 
		unsigned int v : 1; 
		flags () :  in{}, defOut{0}, defIn{0}, c{0}, v{0} {}
	};
	flags f;
	public:
 		Controller (int c, char **a);
		bool toTex (void) { return !f.c; }
		bool isVerbose (void) { return f.v; }
		string defOutFile (void) { if (this->f.c) return this->defaultOutTex; else return defaultOutPDF; }
		string outputFile (void) { return this->f.defOut?this->f.out:this->defOutFile(); }
		string inputFile (void) { if (this->f.defIn) return this->f.in; std::cerr << "No input file specified." << std::endl; exit(1); }
};
