#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "assemble.h"
#include "compiler.h"
#include "formdesc.h"
#include "llist.h"
#include "semantics.h"
#include "translate.h"
#include "utility.h"

/* interface to parser */
extern FILE *yyin;
int yyparse(void);

unsigned int identifier_hash = 0U;

struct linked_list *current_translation_unit = 0;

struct linked_list *compile(FILE *file, int verbose)
{
	/* initialization */
	sem_init();  /* set up symbol table */

	/* lexing and parsing */
	current_translation_unit = ll_new();
	yyin = file;
	yyparse();
	if (verbose)  printf("=== Identifier hash: %u\n", identifier_hash);
	srand(identifier_hash);

	/* semantic analysis */
	struct ll_iter *iter = ll_iterator(current_translation_unit);
	while (ll_has_next(iter))
		sem_fn(ll_next_ptr(iter));
	ll_iterator_free(iter);

	/* translation */
	struct linked_list *form_list = ll_new();
	iter = ll_iterator(current_translation_unit);
	while (ll_has_next(iter))
		tr_fn(ll_next_ptr(iter), form_list);
	ll_iterator_free(iter);

	if (verbose) {
		printf("=== Translated code:\n");
		iter = ll_iterator(form_list);
		while (ll_has_next(iter)) {
			struct form_desc *form = ll_next_ptr(iter);
			printf("%p: %s", (void *)form, form->name ? form->name : "(null)");
			if (form->parent != 0)
				printf(" (parent %p)", (void *)form->parent);
			printf("\n");
			print_translated_line_list("  ", form->lines);
		}
		ll_iterator_free(iter);
	}

	/* assembly */
	assemble(form_list);

	if (verbose) {
		printf("=== Assembled code:\n");
		iter = ll_iterator(form_list);
		while (ll_has_next(iter)) {
			struct form_desc *form = ll_next_ptr(iter);
			printf("%p: %s", (void *)form, form->name ? form->name : "(null)");
			if (form->parent != 0)
				printf(" (parent %p)", (void *)form->parent);
			printf("\n");
			print_assembled_line_list("  ", form->lines);
		}
		ll_iterator_free(iter);
	}

	/* cleanup */
	sem_release();
	while (ll_size(current_translation_unit) > 0)
		ast_free_fn(ll_shift_ptr(current_translation_unit));
	ll_free(current_translation_unit);
	current_translation_unit = 0;

	return form_list;
}

void add_id_to_hash(const char *id)
{
	assert(id != 0);
	identifier_hash *= 31U;
	for (const char *c = id; *c; ++c) {
		identifier_hash = 31U * identifier_hash + (unsigned int)(*c);
	}
}
