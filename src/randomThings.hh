#ifndef IRSCC_RANDOMTHINGS
#define IRSCC_RANDOMTHINGS
#include <vector>
#include <string>
#include <utility>
#include <cstdlib>

constexpr double useQualifier = .75;
constexpr double additionalQualifier = .2; 

template <typename T>
std::vector<T>& shuffle (std::vector<T>& v) {
	int size = v.size();
	for (int i = 0; i < size-1; i++) {
		int num = i + rand()%(size-i);
		swap(v[i],v[num]);
	}
	return v;
}


template<typename T> 
T getRandom(const std::vector<T>& v) {
	auto s = v[ (int)rand()%v.size() ];
	return s;
}

std::string makeRandomLabel();
std::string makeRandomSectionTitle();
std::string makeRandomFormTitle();
std::string capitalizeFirst (std::string s);
#endif
