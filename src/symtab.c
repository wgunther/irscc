#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "formdesc.h"
#include "llist.h"
#include "rbtmap.h"
#include "symtab.h"
#include "translate.h"  /* for next_qtyid */
#include "utility.h"

struct symtab_node {
	struct rbtmap *map;            /* see rbtmap.h */
	struct symtab_node *parent;
	struct linked_list *children;  /* see llist.h */
};

/* The entry in the rbtmap with this key has as its value a pointer to the
   corresponding block (i.e., a pointer to a struct ast_stmt_node). */
static const char *BLOCK_KEY = "";

static int void_strcmp(const void *a, const void *b)
{
	return strcmp((const char *)a, (const char *)b);
}

struct symtab_node *symtab_new(void)
{
	struct symtab_node *node = checked_malloc(sizeof(*node));
	node->map = rbtmap_new(void_strcmp);
	node->parent = 0;
	node->children = ll_new();
	return node;
}

static void symtab_rbtmap_cleanup(void *key, void *value)
{
	/* don't free the value if the key is BLOCK_KEY */
	if (strcmp((const char *)key, BLOCK_KEY) != 0) {
		struct symtab_entry *entry = value;
		/* don't free entry->block: it's an alias */
		ast_free_type(entry->type);
		free(entry);
	}
	free(key);
}

static void symtab_free_node(struct symtab_node *node)
{
	assert(node != 0);
	rbtmap_free(node->map, symtab_rbtmap_cleanup);
	struct ll_iter *iter = ll_iterator(node->children);
	while (ll_has_next(iter))
		symtab_free_node(ll_next_ptr(iter));
	ll_iterator_free(iter);
	ll_free(node->children);
	free(node);
}

void symtab_free(struct symtab_node *node)
{
	assert(node != 0);
	while (node->parent != 0)  node = node->parent;
	symtab_free_node(node);
}

struct symtab_node *symtab_enter_block(struct symtab_node *context,
		struct ast_stmt_node *block)
{
	assert(context != 0);
	struct symtab_node *node = symtab_new();
	node->parent = context;
	ll_push_ptr(context->children, node);
	rbtmap_put(node->map, copy_string(BLOCK_KEY), block, 0);
	return node;
}

struct symtab_node *symtab_leave_block(struct symtab_node *context)
{
	assert(context != 0);  assert(context->parent != 0);
	return context->parent;
}

struct symtab_entry *symtab_search(struct symtab_node *context, const char *sym)
{
	assert(context != 0);  assert(sym != 0);
	struct symtab_entry *entry;
	for (entry = 0; entry == 0 && context != 0; context = context->parent)
		entry = rbtmap_get(context->map, sym);
	return entry;
}

struct symtab_entry *symtab_lookup(struct symtab_node *context, const char *sym)
{
	assert(context != 0);  assert(sym != 0);
	struct symtab_entry *entry = symtab_search(context, sym);
	if (entry == 0) {
		fprintf(stderr, "Error: undefined identifier '%s'\n", sym);
		exit(EXIT_FAILURE);
	}
	return entry;
}

void symtab_declare(struct symtab_node *context, const char *sym,
		struct ast_type *type)
{
	assert(context != 0);  assert(sym != 0);
	struct symtab_entry *entry = rbtmap_get(context->map, sym);
	if (entry != 0) {  /* already declared */
		if (!ast_type_equiv(type, entry->type)) {
			fprintf(stderr, "Error: duplicate declaration for variable '%s'\n"
					"    Previously declared as ", sym);
			ast_print_type(stderr, entry->type);
			fprintf(stderr, "\n    Redeclared as ");
			ast_print_type(stderr, type);
			fprintf(stderr, "\n");
			exit(EXIT_FAILURE);
		} else
			/* duplicate declaration, but equivalent type; just roll with it */
			return;
	}
	char *key = copy_string(sym);
	entry = checked_malloc(sizeof(*entry));
	entry->block = rbtmap_get(context->map, BLOCK_KEY);
	entry->type = type;
	entry->lineno.form = 0;
	entry->lineno.line = 0;
	entry->lineno.subline = '\0';
	if (type->meta == TYPE_FN) {
		entry->first_arg_qtyid = next_qtyid;
		next_qtyid += (int)ll_size(type->u.fn.args);
		entry->return_qtyid = next_qtyid;
		++next_qtyid;
	} else {
		entry->first_arg_qtyid = 0;
		entry->return_qtyid = 0;
	}
	entry->form = 0;
	entry->ret = 0;
	rbtmap_put(context->map, key, entry, symtab_rbtmap_cleanup);
}
