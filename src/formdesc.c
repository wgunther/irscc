#include <assert.h>
#include <stdlib.h>
#include "formdesc.h"
#include "llist.h"

void form_desc_list_free(struct linked_list *L)
{
	if (L == 0)  return;
	struct ll_iter *form_iter = ll_iterator(L);
	while (ll_has_next(form_iter)) {
		struct form_desc *form = ll_next_ptr(form_iter);
		free(form->name);
		struct ll_iter *line_iter = ll_iterator(form->lines);
		while (ll_has_next(line_iter)) {
			struct line_desc *line = ll_next_ptr(line_iter);
			assert(line->op != OP_ASSIGN && line->op != OP_SYMTAB &&
					line->op != OP_RECONCILE);
					/* therefore don't need to free individual arguments */
			ll_free(line->args);
			free(line);
		}
		ll_iterator_free(line_iter);
		ll_free(form->lines);
		free(form);
	}
	ll_iterator_free(form_iter);
	ll_free(L);
}
