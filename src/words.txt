"QUALIFIER
accumulated
active
additional
adjusted
aggregate
allowed
alternative
amortized
assessed
"conservative
cumulative
estimated
excess
exempt
experimental
foreign
gross
incurred
intangible
long-term
miscellaneous
net
non-cash
non-refundable
non-taxable
nonactive
nonpassive
ordinary
passive
professional
qualified
recaptured
refundable
reported
short-term
special
tangible
taxable
unadjusted
unallowed
unrecaptured
wholesale

"ATTRIBUTIVE NOUNS
"Be careful making something longer than 12 without hyphens.
aquaculture
beekeeping
business
"cattle
combat
conservation
construction
dairy
"dairy and cheesemaking
education
energy
factory
farm
fishing
"-> forestry
gambling
fruit
"-> gambling
forestry
grain
health
hunting
insurance
legal
"manufacturing
mining
mortgage
moving
music
operating
ranching
"-> research
zoo
salvage
sporting
storage
store
teaching
textile
travel
vegetable
vehicle
wildlife
"-> zoo
research

"TYPE
"Be careful making something longer than 12 without hyphens
allowance
assets
costs
credit
deduction
depletion
depreciation
earnings
"-> expenditures
taxes
equity
earnings
expenses
gifts
income
interest
liability
losses
pay
penalty
profit
rent
returns
royalties
sales
subsidies
tariffs
"-> taxes
expenditures
valuation

