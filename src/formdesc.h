#ifndef IRSCC_FORMDESC_H
#define IRSCC_FORMDESC_H

#if defined(__cplusplus)
extern "C" {
#endif

/*
	Opcodes and their arguments:

	OP_DOLLARS_CONSTANT <int>
	OP_CENTS_CONSTANT <float>
	OP_COPY <lineno>
		Copy amount from [line number]. This will always refer to a previous
		line on the current form.
	OP_COPY_FROM_ATTACHED <lineno>
		Copy amount from [line number], which will be on another form (or on a
		recursive instance of the current form). This is how function returns
		are implemented. This instruction will appear only inside a call block.
	OP_ADD <lineno>, <lineno>
	OP_SUBTRACT <lineno>, <lineno>
	OP_MULTIPLY <lineno>, <lineno>
	OP_DIVIDE <lineno>, <lineno>
	OP_INTEGER_DIVIDE <lineno>, <lineno>
	OP_NEGATE <lineno>
	OP_IF_ZERO_GOTO <lineno>, <lineno>
		If amount on [first line number] is zero, go to [second line number].
		Otherwise check this box and continue to next line.
	OP_IF_NONZERO_GOTO <lineno>, <lineno>
		If amount on [first line number] is nonzero, check this box and
		continue to next line.  Otherwise go to [second line number].
	OP_IF_POSITIVE_GOTO <lineno>, <lineno>
		If amount on [first line number] is positive, go to [second line
		number]. Otherwise check this box and continue to next line.
	OP_IF_NEGATIVE_GOTO <lineno>, <lineno>
		If amount on [first line number] is negative, go to [second line
		number]. Otherwise check this box and continue to next line.
	OP_IF_NONNEGATIVE_GOTO <lineno>, <lineno>
		If amount on [first line number] is nonnegative, go to [second line
		number]. Otherwise check this box and continue to next line.
	OP_IF_NONPOSITIVE_GOTO <lineno>, <lineno>
		If amount on [first line number] is nonpositive, go to [second line
		number]. Otherwise check this box and continue to next line.
	OP_IF_CHECKED_COPY_ELSE_COPY <lineno>, <lineno>, <lineno>
		If [first line number] is checked, copy amount from [second line
		number]. Otherwise copy amount from [third line number].
	OP_NOP
		No operation. An OP_NOP with a nonzero qtyid field is used to receive
		function arguments.
	OP_STOP
	OP_BEGIN_CALL_BLOCK
		Between OP_BEGIN_CALL_BLOCK and OP_CALL are a bunch of operations
		whose purpose is to transmit function arguments and the values of
		externally visible variables to a form or schedule. These operations
		will have a nonzero qtyid, and enter_also will be set to point to a
		line on the other form (which will be an OP_NOP with the same qtyid).
	OP_CALL <ptr>
		The pointer has type struct form_desc *. This opcode indicates that
		another form or schedule should be completed at this point. The
		description of this line should say something like, "Add 1 to the
		Attachment Sequence Number of this form. Enter the result here and in
		the Attachment Sequence Number blank of Form X." (Perhaps a line
		number of 0, with a non-null form pointer, should refer to the
		Attachment Sequence Number of that form?)
	OP_END_CALL_BLOCK
		Between OP_CALL and OP_END_CALL_BLOCK are a bunch of operations to
		copy the return value(s) of the function and modified values of
		externally visible variables from the form. These will always be
		OP_COPY_FROM_ATTACHED, perhaps with a nonzero qtyid. In the case of a
		recursive function or a loop, these line numbers will appear to refer
		to lines on the current form, but really they mean lines on the
		recursively called form.

	Poem for Will. A haiku:
		combination white
		red under ice upon stream
		walnut everything

	Opcodes used only in intermediate code:

	OP_ASSIGN <context>, <id>, <lineno>
	OP_SYMTAB <context>, <id>
	OP_RECONCILE <context>, <id>, <checkbox>, <stash>
		Terrible kludge: if qtyid is set for an OP_RECONCILE, then it has the
		opposite meaning (i.e., if checkbox is checked, then use the stashed
		value; otherwise use the current value).
	OP_GOTO <lineno>
	OP_FN_ARG <lineno>, <context>, <id>, <argno>
	OP_FN_CALL <context>, <id>
	OP_FN_RET <context>, <id>
*/

enum opcode {
	OP_DOLLARS_CONSTANT, OP_CENTS_CONSTANT, OP_COPY, OP_COPY_FROM_ATTACHED,
	OP_ADD, OP_SUBTRACT, OP_MULTIPLY, OP_DIVIDE, OP_INTEGER_DIVIDE, OP_NEGATE,
	OP_IF_ZERO_GOTO, OP_IF_NONZERO_GOTO, OP_IF_POSITIVE_GOTO,
	OP_IF_NEGATIVE_GOTO, OP_IF_NONNEGATIVE_GOTO, OP_IF_NONPOSITIVE_GOTO,
	OP_IF_CHECKED_COPY_ELSE_COPY, OP_NOP, OP_STOP, OP_BEGIN_CALL_BLOCK,
	OP_CALL, OP_END_CALL_BLOCK, OP_NEW_SECTION,
/* ==== the opcodes below this line are used in intermediate code only ==== */
	OP_ASSIGN, OP_SYMTAB, OP_RECONCILE, OP_GOTO, OP_FN_ARG, OP_FN_CALL,
	OP_FN_RET
};

struct form_desc {
	char *name;  /* function name in C source for forms, null for schedules */
	struct form_desc *parent;  /* null for forms, non-null for schedules */
	struct linked_list *lines;  /* list of pointers to struct line_desc */
};

struct line_number {
	const struct form_desc *form;
	int line;
	char subline;  /* 'a', 'b', 'c', etc., or '\0' if nothing */
};

struct line_desc {  /* see notes below */
	struct line_number lineno;
	enum opcode op;
	struct linked_list *args;
	int qtyid;
	union {
		struct line_desc *lineptr;  /* generated by translation */
		struct line_number lineno;  /* generated by assembly */
	} enter_also;
	union {
		struct line_desc *lineptr;  /* generated by translation */
		struct line_number lineno;  /* generated by assembly */
	} goto_after;
	int is_jump_target;  /* used during compilation */
};

/*
	Notes about members of struct line_desc:

	struct line_number lineno;
		The line number of this line.
	
	enum opcode op;
		The operation to perform on this line.
	
	struct linked_list *args;
		The arguments for the operation.

	int qtyid;
		If this is a positive integer, it is an identifier that should be
		associated with a descriptive name for the amount on this line, such
		as "Zoo expenditures". This identifier may be used on other forms and
		schedules to create an association between different lines. If there is
		no such identifier for this line, the value of qtyid will be 0.
	
	union {
		struct line_desc *lineptr;
		struct line_number lineno;
	} enter_also;
		A line where the result of this instruction should also be entered.
		This will always refer to a line on a different form (or a recursive
		call to the current form). This will only happen within a call block.
		The lineptr element of the union is generated in the translation stage
		and resolved to a line number in the assembly stage.
	
	union {
		struct line_desc *lineptr;
		struct line_number lineno;
	} goto_after;
		A line to jump to after this instruction has been completed. If set,
		this will always refer to a later line in the current form. The lineptr
		element of the union is generated in the translation stage and resolved
		to a line number in the assembly stage.
	
	Both enter_also and goto_after may be null, referring to no line number.
	To indicate this (for enter_also, for example):
		enter_also.lineno.form is a null pointer,
		enter_also.lineno.line is 0,
		enter_also.lineno.subline is '\0'.
*/

void form_desc_list_free(struct linked_list *L);

#if defined (__cplusplus)
}
#endif

#endif  /* IRSCC_FORMDESC_H */
