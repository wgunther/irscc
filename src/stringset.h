#ifndef IRSCC_STRINGSET_H
#define IRSCC_STRINGSET_H

#include <stddef.h>

#if defined(__cplusplus)
extern "C" {
#endif

struct stringset *stringset_new(void);
void stringset_free(struct stringset *s);
size_t stringset_size(struct stringset *s);
void stringset_add(struct stringset *s, const char *t);
struct stringset *stringset_copy(struct stringset *s);
void stringset_add_all(struct stringset *to, struct stringset *from);
struct stringset *stringset_intersection(struct stringset *s1,
		struct stringset *s2);
struct ss_iter *stringset_iterator(struct stringset *s);
int stringset_has_next(struct ss_iter *i);
const char *stringset_next(struct ss_iter *i);
void stringset_iterator_free(struct ss_iter *i);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_STRINGSET_H */
