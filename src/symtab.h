#ifndef IRSCC_SYMTAB_H
#define IRSCC_SYMTAB_H

#if defined(__cplusplus)
extern "C" {
#endif

#include "ast.h"
#include "formdesc.h"
#include "llist.h"

struct symtab_entry {
	struct ast_stmt_node *block;  /* this pointer is an alias */
	struct ast_type *type;
	struct line_number lineno;
/* ===== the fields below this line are meaningful only for functions ===== */
	int first_arg_qtyid;
	int return_qtyid;
	struct form_desc *form;
	struct line_desc *ret;
};

struct symtab_node *symtab_new(void);
void symtab_free(struct symtab_node *node);
struct symtab_node *symtab_enter_block(struct symtab_node *context,
		struct ast_stmt_node *block);
struct symtab_node *symtab_leave_block(struct symtab_node *context);
struct symtab_entry *symtab_search(struct symtab_node *context,
		const char *sym);  /* null if not found */
struct symtab_entry *symtab_lookup(struct symtab_node *context,
		const char *sym);  /* error if not found */
void symtab_declare(struct symtab_node *context, const char *sym,
		struct ast_type *type);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_SYMTAB_H */
