%{
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "compiler.h"
#include "llist.h"
#include "semantics.h"
#include "translate.h"
#include "utility.h"

/* interface to lexer */
extern int yylex(void);
extern int yylineno;

void yyerror(char *s, ...);  /* defined herein */

%}

%union {
	unsigned long integer;
	double floating;
	char *string;
	struct linked_list *llist;
	spec_t specifiers;
	struct ast_decl_node *decl_node;
	struct ast_idxr_node *idxr_node;
	struct ast_dxr_node *dxr_node;
	struct ast_expr_node *expr_node;
	struct ast_stmt_node *stmt_node;
	struct ast_fn_node *fn_node;
}

%token <integer>  TOK_INTEGER_CONSTANT
%token <floating> TOK_FLOATING_CONSTANT
%token <string>   TOK_IDENTIFIER

/* multi-character operators */
%token TOK_AND_AND TOK_AND_EQUALS TOK_ARROW TOK_ELLIPSIS TOK_EQUALS_EQUALS
%token TOK_GREATER_EQUALS TOK_LESS_EQUALS TOK_LSHIFT TOK_LSHIFT_EQUALS
%token TOK_MINUS_EQUALS TOK_MINUS_MINUS TOK_MODULO_EQUALS TOK_NOT_EQUALS
%token TOK_OR_EQUALS TOK_OR_OR TOK_PLUS_EQUALS TOK_PLUS_PLUS TOK_RSHIFT
%token TOK_RSHIFT_EQUALS TOK_SLASH_EQUALS TOK_STAR_EQUALS TOK_XOR_EQUALS

/* keywords */
%token TOK_AUTO TOK_BREAK TOK_CASE TOK_CHAR TOK_CONST TOK_CONTINUE TOK_DEFAULT
%token TOK_DO TOK_DOUBLE TOK_ELSE TOK_ENUM TOK_EXTERN TOK_FLOAT TOK_FOR
%token TOK_GOTO TOK_IF TOK_INT TOK_LONG TOK_REGISTER TOK_RETURN TOK_SHORT
%token TOK_SIGNED TOK_SIZEOF TOK_STATIC TOK_STRUCT TOK_SWITCH TOK_TYPEDEF
%token TOK_UNION TOK_UNSIGNED TOK_VOID TOK_VOLATILE TOK_WHILE

/* constants */
%token TOK_CHARACTER_CONSTANT TOK_STRING

/* ===== Types ============================================================= */

/* omitted: translation_unit */
%type <fn_node> external_declaration  /* TODO: this shouldn't be a fn_node */
%type <fn_node> function_definition
%type <decl_node> declaration
%type <llist> declaration_list
%type <specifiers> declaration_specifiers storage_class_specifier
%type <specifiers> type_specifier type_qualifier
/* omitted: struct_or_union_specifier struct_or_union struct_declaration_list */
%type <llist> init_declarator_list
%type <idxr_node> init_declarator
/* omitted: struct_declaration specifier_qualifier_list struct_declarator_list
	struct_declarator enum_specifier enumerator_list enumerator */
%type <dxr_node> declarator direct_declarator
/* omitted: pointer */
%type <specifiers> type_qualifier_list
%type <llist> parameter_type_list parameter_list
%type <decl_node> parameter_declaration;
/* omitted: identifier_list */
%type <expr_node> initializer
/* omitted: initializer_list type_name abstract_declarator
	direct_abstract_declarator typedef_name */
%type <stmt_node> statement labeled_statement expression_statement
%type <stmt_node> compound_statement
%type <llist> statement_list
%type <stmt_node> selection_statement iteration_statement jump_statement
%type <expr_node> expression assignment_expression conditional_expression
%type <expr_node> constant_expression logical_OR_expression
%type <expr_node> logical_AND_expression inclusive_OR_expression
%type <expr_node> exclusive_OR_expression AND_expression equality_expression
%type <expr_node> relational_expression shift_expression additive_expression
%type <expr_node> multiplicative_expression cast_expression unary_expression
%type <expr_node> postfix_expression primary_expression
%type <llist> argument_expression_list
%type <expr_node> constant

%%

/* Grammar from Section A13 of K&R, second edition */

translation_unit:
   external_declaration { ll_push_ptr(current_translation_unit, $1); }
 | translation_unit external_declaration
 		{ ll_push_ptr(current_translation_unit, $2); }
 ;

external_declaration:  /* referenced by translation_unit */
   function_definition { $$ = $1; }
 | declaration {
		fprintf(stderr, "External declarations are not supported yet. "
				"Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

function_definition:  /* referenced by external_declaration */
   declaration_specifiers declarator compound_statement
		{ $$ = ast_new_fn($1, $2, $3); }
 | declarator compound_statement {
		fprintf(stderr, "Warning: type specifier missing in function "
				"definition; assuming 'int'\n");
 		$$ = ast_new_fn(SPEC_INT, $1, $2);
 	}
/* K&R-style function definitions: */
 | declaration_specifiers declarator declaration_list compound_statement {
		fprintf(stderr, "K&R-style function definitions are not supported. "
				"Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | declarator declaration_list compound_statement {
		fprintf(stderr, "K&R-style function definitions are not supported. "
				"Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

declaration:  /* referenced by declaration_list, external_declaration */
   declaration_specifiers init_declarator_list ';'
	{ $$ = ast_new_decl($1, $2); }
/* This form is used only to declare a struct or union tag, or an enum: */
/* | declaration_specifiers ';' { $$ = ast_new_decl($1, ll_new()); } */
 ;

declaration_list:  /* referenced by compound_statement, function_definition */
   declaration                  { $$ = ll_new();  ll_push_ptr($$, $1); }
 | declaration_list declaration { $$ = $1;        ll_push_ptr($$, $2); }
 ;

declaration_specifiers:  /* referenced by declaration, function_definition,
                            parameter_declaration */
   storage_class_specifier declaration_specifiers { $$ = $1 | $2; }
 | storage_class_specifier { $$ = $1; }
 | type_specifier declaration_specifiers { $$ = $1 | $2; }
 | type_specifier { $$ = $1; }
 | type_qualifier declaration_specifiers { $$ = $1 | $2; }
 | type_qualifier { $$ = $1; }
 ;

storage_class_specifier:  /* referenced by declaration_specifiers */
   TOK_AUTO     { $$ = SPEC_AUTO;     }
 | TOK_REGISTER { $$ = SPEC_REGISTER; }
 | TOK_STATIC   { $$ = SPEC_STATIC;   }
 | TOK_EXTERN   { $$ = SPEC_EXTERN;   }
 | TOK_TYPEDEF  {
		/* $$ = SPEC_TYPEDEF; */
		fprintf(stderr, "typedef is not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

type_specifier:  /* referenced by declaration_specifiers,
                    specifier_qualifier_list */
   TOK_VOID     { $$ = SPEC_VOID;     }
 | TOK_CHAR     { $$ = SPEC_CHAR;     }
 | TOK_SHORT    { $$ = SPEC_SHORT;    }
 | TOK_INT      { $$ = SPEC_INT;      }
 | TOK_LONG     { $$ = SPEC_LONG;     }
 | TOK_FLOAT    { $$ = SPEC_FLOAT;    }
 | TOK_DOUBLE   { $$ = SPEC_DOUBLE;   }
 | TOK_SIGNED   { $$ = SPEC_SIGNED;   }
 | TOK_UNSIGNED { $$ = SPEC_UNSIGNED; }
 | struct_or_union_specifier {
		fprintf(stderr, "Structures and unions are not supported yet. "
				"Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | enum_specifier {
		fprintf(stderr, "Enumerations are not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
/* | typedef_name */
 ;

type_qualifier:  /* referenced by declaration_specifiers,
                    specifier_qualifier_list, type_qualifier_list */
   TOK_CONST    { $$ = SPEC_CONST;    }
 | TOK_VOLATILE { $$ = SPEC_VOLATILE; }
 ;

struct_or_union_specifier:  /* referenced by type_specifier */
   struct_or_union TOK_IDENTIFIER '{' struct_declaration_list '}'
 | struct_or_union '{' struct_declaration_list '}'
 | struct_or_union TOK_IDENTIFIER
 ;

struct_or_union:  /* referenced by struct_or_union_specifier */
   TOK_STRUCT
 | TOK_UNION
 ;

struct_declaration_list:  /* referenced by struct_or_union_specifier */
   struct_declaration
 | struct_declaration_list struct_declaration
 ;

init_declarator_list:  /* referenced by declaration */
   init_declarator                    { $$ = ll_new();  ll_push_ptr($$, $1); }
 | init_declarator_list ',' init_declarator { $$ = $1;  ll_push_ptr($$, $3); }
 ;

init_declarator:  /* referenced by init_declarator_list */
   declarator { $$ = ast_new_idxr($1, 0); }
 | declarator '=' initializer { $$ = ast_new_idxr($1, $3); }
 ;

struct_declaration:  /* referenced by struct_declaration_list */
   specifier_qualifier_list struct_declarator_list ';'
 ;

specifier_qualifier_list:  /* referenced by struct_declaration, type_name */
   type_specifier specifier_qualifier_list
 | type_specifier
 | type_qualifier specifier_qualifier_list
 | type_qualifier
 ;

struct_declarator_list:  /* referenced by struct_declaration */
   struct_declarator
 | struct_declarator_list ',' struct_declarator
 ;

struct_declarator:  /* referenced by struct_declarator_list */
   declarator
 | declarator ':' constant_expression
 | ':' constant_expression
 ;

enum_specifier:  /* referenced by type_specifier */
   TOK_ENUM TOK_IDENTIFIER '{' enumerator_list '}'
 | TOK_ENUM '{' enumerator_list '}'
 | TOK_ENUM TOK_IDENTIFIER
 ;

enumerator_list:  /* referenced by enum_specifier */
   enumerator
 | enumerator_list ',' enumerator
 ;

enumerator:  /* referenced by enumerator_list */
   TOK_IDENTIFIER
 | TOK_IDENTIFIER '=' constant_expression
 ;

declarator:  /* referenced by direct_declarator, function_definition,
                init_declarator, parameter_declaration, struct_declarator */
   direct_declarator { $$ = $1; }
/* This production from K&R is awkward to work with: */
/*     pointer direct_declarator                     */
/* The following pair of productions is better:      */
 | '*' declarator { $$ = ast_new_dxr_ptr(0, $2); }
 | '*' type_qualifier_list declarator { $$ = ast_new_dxr_ptr($2, $3); }
 ;

direct_declarator:  /* referenced by declarator */
   TOK_IDENTIFIER { $$ = ast_new_dxr_id($1);  free($1); }
 | '(' declarator ')' { $$ = $2; }
 | direct_declarator '[' constant_expression ']'
 		{ $$ = ast_new_dxr_arr($1, $3); }
 | direct_declarator '[' ']' {
		fprintf(stderr, "Array declarators without specified length are not "
				"supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | direct_declarator '(' parameter_type_list ')'
 		{ $$ = ast_new_dxr_fn($1, $3); }
 | direct_declarator '(' identifier_list ')' {
		fprintf(stderr, "K&R-style function declarators are not supported. "
				"Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | direct_declarator '(' ')' { $$ = ast_new_dxr_fn($1, ll_new()); }
 ;

/* INTENTIONALLY UNUSED: see declarator, abstract_declarator
pointer:
   '*' type_qualifier_list
 | '*'
 | '*' type_qualifier_list pointer
 | '*' pointer
 ;
*/

type_qualifier_list:  /* referenced by declarator, abstract_declarator
                         [in place of pointer] */
   type_qualifier { $$ = $1; }
 | type_qualifier_list type_qualifier { $$ = $1 | $2; }
 ;

parameter_type_list:  /* referenced by direct_declarator,
                         direct_abstract_declarator */
   parameter_list { $$ = $1; }
 | parameter_list ',' TOK_ELLIPSIS {
		fprintf(stderr, "Variadic functions are not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

parameter_list:  /* referenced by parameter_type_list */
   parameter_declaration              { $$ = ll_new();  ll_push_ptr($$, $1); }
 | parameter_list ',' parameter_declaration { $$ = $1;  ll_push_ptr($$, $3); }
 ;

parameter_declaration:  /* referenced by parameter_list */
   declaration_specifiers declarator {
		struct ast_idxr_node *idxr = ast_new_idxr($2, 0);
		struct linked_list *L = ll_new();
		ll_push_ptr(L, idxr);
		$$ = ast_new_decl($1, L);
	}
 | declaration_specifiers abstract_declarator {
		fprintf(stderr, "Parameter declarations without identifiers are not "
				"supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | declaration_specifiers {
		fprintf(stderr, "Parameter declarations without identifiers are not "
				"supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

identifier_list:  /* referenced by direct_declarator [for K&R-style function
                     declarators] */
   TOK_IDENTIFIER
 | identifier_list ',' TOK_IDENTIFIER
 ;

initializer:  /* referenced by init_declarator, initializer_list */
   assignment_expression { $$ = $1; }
 | '{' initializer_list '}' {
		fprintf(stderr, "Structure and union initializers are not supported "
				"yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | '{' initializer_list ',' '}' {
		fprintf(stderr, "Structure and union initializers are not supported "
				"yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

initializer_list:  /* referenced by initializer */
   initializer
 | initializer_list ',' initializer
 ;

type_name:  /* referenced by cast_expression, unary_expression [for sizeof] */
   specifier_qualifier_list abstract_declarator
 | specifier_qualifier_list
 ;

abstract_declarator:  /* referenced by direct_abstract_declarator,
                         parameter_declaration, type_name */
   direct_abstract_declarator
/* The following productions from K&R are awkward to work with: */
/*     pointer                                                  */
/*     pointer direct_abstract_declarator                       */
/* The following set of productions is better:                  */
 | '*'
 | '*' type_qualifier_list
 | '*' abstract_declarator
 | '*' type_qualifier_list abstract_declarator
 ;

direct_abstract_declarator:  /* referenced by abstract_declarator */
   '(' abstract_declarator ')'
 | direct_abstract_declarator '[' constant_expression ']'
 | '[' constant_expression ']'
 | direct_abstract_declarator '[' ']'
 | '[' ']'
 | direct_abstract_declarator '(' parameter_type_list ')'
 | '(' parameter_type_list ')'
 | direct_abstract_declarator '(' ')'
 | '(' ')'
 ;

/*
typedef_name:
   TOK_IDENTIFIER
 ;
*/

statement:  /* referenced by statement_list, iteration_statement,
               labeled_statement, selection_statement */
   labeled_statement    { $$ = $1; }
 | expression_statement { $$ = $1; }
 | compound_statement   { $$ = $1; }
 | selection_statement  { $$ = $1; }
 | iteration_statement  { $$ = $1; }
 | jump_statement       { $$ = $1; }
 ;

labeled_statement:  /* referenced by statement */
   TOK_IDENTIFIER ':' statement
	{ $$ = ast_new_labeled_stmt($1, $3);  free($1); }
 | TOK_CASE constant_expression ':' statement
	{ $$ = ast_new_case_stmt($2, $4); }
 | TOK_DEFAULT ':' statement { $$ = ast_new_default_stmt($3); }
 ;

expression_statement:  /* referenced by statement */
   expression ';' { $$ = ast_new_expr_stmt($1); }
 | ';'            { $$ = ast_new_empty_stmt();  }
 ;

compound_statement:  /* referenced by function_definition, statement */
   '{' declaration_list statement_list '}'
                            { $$ = ast_new_compound_stmt($2, $3);       }
 | '{' statement_list '}'   { $$ = ast_new_compound_stmt(ll_new(), $2); }
 | '{' declaration_list '}' { $$ = ast_new_compound_stmt($2, ll_new()); }
 | '{' '}'                  { $$ = ast_new_compound_stmt(ll_new(), ll_new()); }
 ;

statement_list:  /* referenced by compound_statement */
   statement                { $$ = ll_new();  ll_push_ptr($$, $1); }
 | statement_list statement { $$ = $1;        ll_push_ptr($$, $2); }
 ;

selection_statement:  /* referenced by statement */
   TOK_IF '(' expression ')' statement
	{ $$ = ast_new_if_stmt($3, $5); }
 | TOK_IF '(' expression ')' statement TOK_ELSE statement
	{ $$ = ast_new_if_else_stmt($3, $5, $7); }
 | TOK_SWITCH '(' expression ')' statement
	{ $$ = ast_new_switch_stmt($3, $5); }
 ;

iteration_statement:  /* referenced by statement */
   TOK_WHILE '(' expression ')' statement
	{ $$ = ast_new_while_stmt($3, $5); }
 | TOK_DO statement TOK_WHILE '(' expression ')' ';'
	{ $$ = ast_new_do_while_stmt($2, $5); }
 | TOK_FOR '(' expression ';' expression ';' expression ')' statement
	{ $$ = ast_new_for_stmt($3, $5, $7, $9); }
 | TOK_FOR '(' ';' expression ';' expression ')' statement
	{ $$ = ast_new_for_stmt(0, $4, $6, $8); }
 | TOK_FOR '(' expression ';' ';' expression ')' statement
	{ $$ = ast_new_for_stmt($3, 0, $6, $8); }
 | TOK_FOR '(' ';' ';' expression ')' statement
	{ $$ = ast_new_for_stmt(0, 0, $5, $7); }
 | TOK_FOR '(' expression ';' expression ';' ')' statement
	{ $$ = ast_new_for_stmt($3, $5, 0, $8); }
 | TOK_FOR '(' ';' expression ';' ')' statement
	{ $$ = ast_new_for_stmt(0, $4, 0, $7); }
 | TOK_FOR '(' expression ';' ';' ')' statement
	{ $$ = ast_new_for_stmt($3, 0, 0, $7); }
 | TOK_FOR '(' ';' ';' ')' statement
	{ $$ = ast_new_for_stmt(0, 0, 0, $6); }
 ;

jump_statement:  /* referenced by statement */
   TOK_GOTO TOK_IDENTIFIER ';' { $$ = ast_new_goto_stmt($2);  free($2); }
 | TOK_CONTINUE ';'            { $$ = ast_new_continue_stmt();          }
 | TOK_BREAK ';'               { $$ = ast_new_break_stmt();             }
 | TOK_RETURN expression ';'   { $$ = ast_new_return_expr_stmt($2);     }
 | TOK_RETURN ';'              { $$ = ast_new_return_stmt();            }
 ;

expression:  /* referenced by expression_statement, selection_statement,
                iteration_statement, jump_statement, conditional_expression,
                postfix_expression, primary_expression */
   assignment_expression { $$ = $1; }
 | expression ',' assignment_expression
	{ $$ = ast_new_binary_op(COMMA, $1, $3); }
 ;

assignment_expression:  /* referenced by initializer, expression,
                           argument_expression_list */
   conditional_expression { $$ = $1; }
/* XXX: the rules here beginning with TOK_IDENTIFIER are not K&R rules */
 | TOK_IDENTIFIER '=' assignment_expression
	{ $$ = ast_new_assign(ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_STAR_EQUALS assignment_expression
	{ $$ = ast_new_assign(STAR_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_SLASH_EQUALS assignment_expression
	{ $$ = ast_new_assign(SLASH_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_MODULO_EQUALS assignment_expression
	{ $$ = ast_new_assign(MODULO_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_PLUS_EQUALS assignment_expression
	{ $$ = ast_new_assign(PLUS_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_MINUS_EQUALS assignment_expression
	{ $$ = ast_new_assign(MINUS_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_LSHIFT_EQUALS assignment_expression
	{ $$ = ast_new_assign(LSHIFT_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_RSHIFT_EQUALS assignment_expression
	{ $$ = ast_new_assign(RSHIFT_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_AND_EQUALS assignment_expression
	{ $$ = ast_new_assign(AND_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_XOR_EQUALS assignment_expression
	{ $$ = ast_new_assign(XOR_ASSIGN, $1, $3);  free($1); }
 | TOK_IDENTIFIER TOK_OR_EQUALS assignment_expression
	{ $$ = ast_new_assign(OR_ASSIGN, $1, $3);  free($1); }
/* | unary_expression '=' assignment_expression */
/* | unary_expression TOK_STAR_EQUALS assignment_expression */
/* | unary_expression TOK_SLASH_EQUALS assignment_expression */
/* | unary_expression TOK_MODULO_EQUALS assignment_expression */
/* | unary_expression TOK_PLUS_EQUALS assignment_expression */
/* | unary_expression TOK_MINUS_EQUALS assignment_expression */
/* | unary_expression TOK_LSHIFT_EQUALS assignment_expression */
/* | unary_expression TOK_RSHIFT_EQUALS assignment_expression */
/* | unary_expression TOK_AND_EQUALS assignment_expression */
/* | unary_expression TOK_XOR_EQUALS assignment_expression */
/* | unary_expression TOK_OR_EQUALS assignment_expression */
 ;

conditional_expression:  /* referenced by assignment_expression,
                            conditional_expression, constant_expression */
   logical_OR_expression { $$ = $1; }
 | logical_OR_expression '?' expression ':' conditional_expression {
		fprintf(stderr, "Conditional operator ?: is not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

constant_expression:  /* referenced by struct_declarator, direct_declarator,
                         direct_abstract_declarator, enumerator,
                         labeled_statement */
   conditional_expression { $$ = $1; }
 ;

logical_OR_expression:  /* referenced by conditional_expression */
   logical_AND_expression { $$ = $1; }
 | logical_OR_expression TOK_OR_OR logical_AND_expression
	{ $$ = ast_new_binary_op(LOGIC_OR, $1, $3); }
 ;

logical_AND_expression:  /* referenced by logical_OR_expression */
   inclusive_OR_expression { $$ = $1; }
 | logical_AND_expression TOK_AND_AND inclusive_OR_expression
	{ $$ = ast_new_binary_op(LOGIC_AND, $1, $3); }
 ;

inclusive_OR_expression:  /* referenced by logical_AND_expression */
   exclusive_OR_expression { $$ = $1; }
 | inclusive_OR_expression '|' exclusive_OR_expression
	{ $$ = ast_new_binary_op(BIT_OR, $1, $3); }
 ;

exclusive_OR_expression:  /* referenced by inclusive_OR_expression */
   AND_expression { $$ = $1; }
 | exclusive_OR_expression '^' AND_expression
	{ $$ = ast_new_binary_op(BIT_XOR, $1, $3); }
 ;

AND_expression:  /* referenced by exclusive_OR_expression */
   equality_expression { $$ = $1; }
 | AND_expression '&' equality_expression
	{ $$ = ast_new_binary_op(BIT_AND, $1, $3); }
 ;

equality_expression:  /* referenced by AND_expression */
   relational_expression { $$ = $1; }
 | equality_expression TOK_EQUALS_EQUALS relational_expression
	{ $$ = ast_new_binary_op(EQUALS_EQUALS, $1, $3); }
 | equality_expression TOK_NOT_EQUALS relational_expression
	{ $$ = ast_new_binary_op(NOT_EQUALS, $1, $3); }
 ;

relational_expression:  /* referenced by equality_expression */
   shift_expression { $$ = $1; }
 | relational_expression '<' shift_expression
	{ $$ = ast_new_binary_op(LESS, $1, $3); }
 | relational_expression '>' shift_expression
	{ $$ = ast_new_binary_op(GREATER, $1, $3); }
 | relational_expression TOK_LESS_EQUALS shift_expression
	{ $$ = ast_new_binary_op(LESS_EQUALS, $1, $3); }
 | relational_expression TOK_GREATER_EQUALS shift_expression
	{ $$ = ast_new_binary_op(GREATER_EQUALS, $1, $3); }
 ;

shift_expression:  /* referenced by relational_expression */
   additive_expression { $$ = $1; }
 | shift_expression TOK_LSHIFT additive_expression
	{ $$ = ast_new_binary_op(LSHIFT, $1, $3); }
 | shift_expression TOK_RSHIFT additive_expression
	{ $$ = ast_new_binary_op(RSHIFT, $1, $3); }
 ;

additive_expression:  /* referenced by shift_expression */
   multiplicative_expression { $$ = $1; }
 | additive_expression '+' multiplicative_expression
 	{ $$ = ast_new_binary_op(BPLUS, $1, $3); }
 | additive_expression '-' multiplicative_expression
 	{ $$ = ast_new_binary_op(BMINUS, $1, $3); }
 ;

multiplicative_expression:  /* referenced by additive_expression */
   cast_expression { $$ = $1; }
 | multiplicative_expression '*' cast_expression
 	{ $$ = ast_new_binary_op(BSTAR, $1, $3); }
 | multiplicative_expression '/' cast_expression
 	{ $$ = ast_new_binary_op(SLASH, $1, $3); }
 | multiplicative_expression '%' cast_expression
 	{ $$ = ast_new_binary_op(MODULO, $1, $3); }
 ;

cast_expression:  /* referenced by multiplicative_expression,
                     unary_expression */
   unary_expression { $$ = $1; }
 | '(' type_name ')' cast_expression {
		fprintf(stderr, "Casts are not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

unary_expression:  /* referenced by assignment_expression, cast_expression */
   postfix_expression          { $$ = $1;                              }
/* XXX: the rules here ending with TOK_IDENTIFIER are not K&R rules */
 | TOK_PLUS_PLUS TOK_IDENTIFIER
 		{ $$ = ast_new_inc_dec(PRE_INC, $2);  free($2); }
 | TOK_MINUS_MINUS TOK_IDENTIFIER
 		{ $$ = ast_new_inc_dec(PRE_DEC, $2);  free($2); }
/* | TOK_PLUS_PLUS unary_expression */
/* | TOK_MINUS_MINUS unary_expression */
 | '&' cast_expression         { $$ = ast_new_unary_op(ADDR_OF,   $2); }
 | '*' cast_expression         { $$ = ast_new_unary_op(USTAR,     $2); }
 | '+' cast_expression         { $$ = ast_new_unary_op(UPLUS,     $2); }
 | '-' cast_expression         { $$ = ast_new_unary_op(UMINUS,    $2); }
 | '~' cast_expression         { $$ = ast_new_unary_op(BIT_NOT,   $2); }
 | '!' cast_expression         { $$ = ast_new_unary_op(LOGIC_NOT, $2); }
 | TOK_SIZEOF unary_expression { $$ = ast_new_unary_op(SIZEOF,    $2); }
 | TOK_SIZEOF '(' type_name ')' {
		fprintf(stderr, "sizeof is not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 ;

postfix_expression:  /* referenced by unary_expression */
   primary_expression { $$ = $1; }
 | postfix_expression '[' expression ']' {
		fprintf(stderr, "Array element access is not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
/* XXX: the rules here beginning with TOK_IDENTIFIER are not K&R rules */
 | TOK_IDENTIFIER '(' argument_expression_list ')'
 		{ $$ = ast_new_fn_call($1, $3);  free($1); }
 | TOK_IDENTIFIER '(' ')' { $$ = ast_new_fn_call($1, ll_new());  free($1); }
/* | postfix_expression '(' argument_expression_list ')' */
/* | postfix_expression '(' ')' */
 | postfix_expression '.' TOK_IDENTIFIER {
		fprintf(stderr, "Structure and union element access is not supported "
				"yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | postfix_expression TOK_ARROW TOK_IDENTIFIER {
		fprintf(stderr, "Indirect element access is not supported yet. "
				"Sorry!\n");
		exit(EXIT_FAILURE);
	}
/* XXX: the rules here beginning with TOK_IDENTIFIER are not K&R rules */
 | TOK_IDENTIFIER TOK_PLUS_PLUS
 		{ $$ = ast_new_inc_dec(POST_INC, $1);  free($1); }
 | TOK_IDENTIFIER TOK_MINUS_MINUS
 		{ $$ = ast_new_inc_dec(POST_DEC, $1);  free($1); }
/* | postfix_expression TOK_PLUS_PLUS */
/* | postfix_expression TOK_MINUS_MINUS */
 ;

primary_expression:  /* referenced by postfix_expression */
   TOK_IDENTIFIER { $$ = ast_new_ident($1);  free($1); }
 | constant { $$ = $1; }
 | TOK_STRING {
		fprintf(stderr, "String literals are not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | '(' expression ')' { $$ = $2; }
 ;

argument_expression_list:  /* referenced by postfix_expression */
   assignment_expression { $$ = ll_new();  ll_push_ptr($$, $1); }
 | argument_expression_list ',' assignment_expression
		{ $$ = $1;  ll_push_ptr($$, $3); }
 ;

constant:  /* referenced by primary_expression */
   TOK_INTEGER_CONSTANT  { $$ = ast_new_integer_constant($1);  }
 | TOK_CHARACTER_CONSTANT {
		fprintf(stderr, "Character constants are not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
 | TOK_FLOATING_CONSTANT { $$ = ast_new_floating_constant($1); }
/* | TOK_ENUMERATION_CONSTANT */
 ;

%%

#include "formdesc.h"
#include "llist.h"

void yyerror(char *s, ...)
{
	va_list ap;
	va_start(ap, s);
	fprintf(stderr, "%d: error: ", yylineno);
	vfprintf(stderr, s, ap);
	fprintf(stderr, "\n");
	va_end(ap);
	exit(EXIT_FAILURE);
}
