#ifndef IRSCC_TRANSLATE_H
#define IRSCC_TRANSLATE_H

#include "ast.h"
#include "formdesc.h"
#include "llist.h"

#if defined(__cplusplus)
extern "C" {
#endif

extern int next_qtyid;

/* WARNING: these functions are not thread-safe */

void tr_fn(struct ast_fn_node *fn, struct linked_list *form_list);

/* tr_stmt and tr_expr return lists of line descriptions */
struct linked_list *tr_stmt(struct ast_stmt_node *stmt);
struct linked_list *tr_expr(struct ast_expr_node *expr);

void print_translated_line_list(const char *prefix, struct linked_list *L);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_TRANSLATE_H */
