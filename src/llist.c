#include <assert.h>
#include <stdlib.h>
#include "formdesc.h"
#include "llist.h"
#include "utility.h"

/* ===== Structure definitions ============================================= */

struct linked_list {
	struct ll_node *head, *tail;
	size_t size;
};

struct ll_node {
	struct ll_node *prev, *next;
	union {
		int i;
		float f;
		struct line_number lineno;
		void *p;
	} content;
};

struct ll_iter {
	struct linked_list *L;
	int forward;
	struct ll_node *target;
};

/* ===== Allocation and deallocation ======================================= */

struct linked_list *ll_new(void)
{
	struct linked_list *L = checked_malloc(sizeof(*L));
	L->head = 0;
	L->tail = 0;
	L->size = 0;
	return L;
}

static struct ll_node *ll_new_node(void)
{
	struct ll_node *n = checked_malloc(sizeof(*n));
	n->prev = n->next = 0;
	return n;
}

static void ll_free_node(struct ll_node *n)
{
	while (n != 0) {
		struct ll_node *next = n->next;
		free(n);
		n = next;
	}
}

void ll_free(struct linked_list *L)
{
	ll_free_node(L->head);
	free(L);
}

/* ===== Size ============================================================== */

size_t ll_size(struct linked_list *L) { assert(L != 0);  return L->size; }

/* ===== Clear ============================================================= */

static void ll_pop(struct linked_list *L);  /* forward declaration */
void ll_clear(struct linked_list *L) { while (L->size > 0)  ll_pop(L); }

/* ===== Push ============================================================== */

static void ll_push(struct linked_list *L, struct ll_node *n)
{
	if (L->tail == 0) {
		L->head = L->tail = n;
		L->size = 1;
	} else {
		L->tail->next = n;
		n->prev = L->tail;
		L->tail = n;
		++L->size;
	}
}

void ll_push_int(struct linked_list *L, int item)
{
	struct ll_node *n = ll_new_node();
	n->content.i = item;
	ll_push(L, n);
}

void ll_push_float(struct linked_list *L, float item)
{
	struct ll_node *n = ll_new_node();
	n->content.f = item;
	ll_push(L, n);
}

void ll_push_lineno(struct linked_list *L, struct line_number item)
{
	struct ll_node *n = ll_new_node();
	n->content.lineno = item;
	ll_push(L, n);
}

void ll_push_ptr(struct linked_list *L, void *item)
{
	struct ll_node *n = ll_new_node();
	n->content.p = item;
	ll_push(L, n);
}

/* ===== Pop =============================================================== */

static void ll_pop(struct linked_list *L)
{
	struct ll_node *new_tail = L->tail->prev;
	if (new_tail == 0) {
		free(L->tail);
		L->head = L->tail = 0;
		L->size = 0;
	} else {
		new_tail->next = 0;
		free(L->tail);
		L->tail = new_tail;
		--L->size;
	}
}

int ll_pop_int(struct linked_list *L)
{
	int item = L->tail->content.i;
	ll_pop(L);
	return item;
}

float ll_pop_float(struct linked_list *L)
{
	float item = L->tail->content.f;
	ll_pop(L);
	return item;
}

struct line_number ll_pop_lineno(struct linked_list *L)
{
	struct line_number item = L->tail->content.lineno;
	ll_pop(L);
	return item;
}

void *ll_pop_ptr(struct linked_list *L)
{
	void *item = L->tail->content.p;
	ll_pop(L);
	return item;
}

/* ===== Unshift =========================================================== */

static void ll_unshift(struct linked_list *L, struct ll_node *n)
{
	if (L->head == 0) {
		L->head = L->tail = n;
		L->size = 1;
	} else {
		L->head->prev = n;
		n->next = L->head;
		L->head = n;
		++L->size;
	}
}

void ll_unshift_int(struct linked_list *L, int item)
{
	struct ll_node *n = ll_new_node();
	n->content.i = item;
	ll_unshift(L, n);
}

void ll_unshift_float(struct linked_list *L, float item)
{
	struct ll_node *n = ll_new_node();
	n->content.f = item;
	ll_unshift(L, n);
}

void ll_unshift_lineno(struct linked_list *L, struct line_number item)
{
	struct ll_node *n = ll_new_node();
	n->content.lineno = item;
	ll_unshift(L, n);
}

void ll_unshift_ptr(struct linked_list *L, void *item)
{
	struct ll_node *n = ll_new_node();
	n->content.p = item;
	ll_unshift(L, n);
}

/* ===== Shift ============================================================= */

static void ll_shift(struct linked_list *L)
{
	struct ll_node *new_head = L->head->next;
	if (new_head == 0) {
		free(L->head);
		L->head = L->tail = 0;
		L->size = 0;
	} else {
		new_head->prev = 0;
		free(L->head);
		L->head = new_head;
		--L->size;
	}
}

int ll_shift_int(struct linked_list *L)
{
	int item = L->head->content.i;
	ll_shift(L);
	return item;
}

float ll_shift_float(struct linked_list *L)
{
	float item = L->head->content.f;
	ll_shift(L);
	return item;
}

struct line_number ll_shift_lineno(struct linked_list *L)
{
	struct line_number item = L->head->content.lineno;
	ll_shift(L);
	return item;
}

void *ll_shift_ptr(struct linked_list *L)
{
	void *item = L->head->content.p;
	ll_shift(L);
	return item;
}

/* ===== Head access ======================================================= */

int ll_head_int(struct linked_list *L)      { return L->head->content.i;      }
float ll_head_float(struct linked_list *L)  { return L->head->content.f;      }
struct line_number ll_head_lineno(struct linked_list *L)
                                            { return L->head->content.lineno; }
void *ll_head_ptr(struct linked_list *L)    { return L->head->content.p;      }

/* ===== Tail access ======================================================= */

int ll_tail_int(struct linked_list *L)      { return L->tail->content.i;      }
float ll_tail_float(struct linked_list *L)  { return L->tail->content.f;      }
struct line_number ll_tail_lineno(struct linked_list *L)
                                            { return L->tail->content.lineno; }
void *ll_tail_ptr(struct linked_list *L)    { return L->tail->content.p;      }

/* ===== Copy and concatenate ============================================== */

struct linked_list *ll_copy(struct linked_list *L)
{
	if (L == 0)  return 0;
	struct linked_list *copy = ll_new();
	struct ll_node *node = L->head, *copy_prev = 0;
	while (node != 0) {
		struct ll_node *node_copy = ll_new_node();
		node_copy->prev = copy_prev;
		node_copy->content = node->content;
		if (copy_prev == 0)
			copy->head = node_copy;
		else
			copy_prev->next = node_copy;
		++copy->size;
		copy_prev = node_copy;
		node = node->next;
	}
	copy->tail = copy_prev;
	assert(copy->size == L->size);
	return copy;
}

void ll_concat_and_free(struct linked_list *master, struct linked_list *end)
{
	if (master == 0 || end == 0)  return;
	if (end->size > 0) {
		assert(
			(master->head == 0 && master->tail == 0 && master->size == 0)
				||
			(master->head != 0 && master->tail != 0 && master->size != 0)
		);
		if (master->size == 0) {
			master->head = end->head;
			master->tail = end->tail;
			master->size = end->size;
		} else {
			master->tail->next = end->head;
			end->head->prev = master->tail;
			master->tail = end->tail;
			master->size += end->size;
		}
	}
	free(end);
}

/* ===== Iterators ========================================================= */

struct ll_iter *ll_iterator(struct linked_list *L)
{
	assert(L != 0);
	struct ll_iter *iter = checked_malloc(sizeof(*iter));
	iter->L = L;
	iter->forward = 1;
	iter->target = L->head;
	return iter;
}

struct ll_iter *ll_reverse_iterator(struct linked_list *L)
{
	assert(L != 0);
	struct ll_iter *iter = checked_malloc(sizeof(*iter));
	iter->L = L;
	iter->forward = 0;
	iter->target = L->tail;
	return iter;
}

void ll_iterator_free(struct ll_iter *iter)
{
	free(iter);
}

int ll_has_next(struct ll_iter *iter)
{
	assert(iter != 0);
	return iter->target != 0;
}

int ll_next_int(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	int item = iter->target->content.i;
	iter->target = (iter->forward ? iter->target->next : iter->target->prev);
	return item;
}

float ll_next_float(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	float item = iter->target->content.f;
	iter->target = (iter->forward ? iter->target->next : iter->target->prev);
	return item;
}

struct line_number ll_next_lineno(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	struct line_number item = iter->target->content.lineno;
	iter->target = (iter->forward ? iter->target->next : iter->target->prev);
	return item;
}

void *ll_next_ptr(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	void *item = iter->target->content.p;
	iter->target = (iter->forward ? iter->target->next : iter->target->prev);
	return item;
}

int ll_this_int(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	return iter->target->content.i;
}

float ll_this_float(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	return iter->target->content.f;
}

struct line_number ll_this_lineno(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	return iter->target->content.lineno;
}

void *ll_this_ptr(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	return iter->target->content.p;
}

void ll_insert_ptr_before(struct ll_iter *iter, void *item)
{
	assert(iter != 0);
	if (iter->target == 0) {
		if (iter->forward)
			ll_push_ptr(iter->L, item);
		else
			ll_unshift_ptr(iter->L, item);
		return;
	}
	struct ll_node *n = ll_new_node();
	n->content.p = item;
	n->prev = iter->target->prev;
	n->next = iter->target;
	iter->target->prev = n;
	if (n->prev == 0)
		iter->L->head = n;
	else
		n->prev->next = n;
	++iter->L->size;
}

void ll_remove(struct ll_iter *iter)
{
	assert(iter != 0 && iter->target != 0);
	struct ll_node *this_node = iter->target;
	iter->target = (iter->forward ? this_node->next : this_node->prev);

	if (this_node->prev == 0)  /* removing head */
		iter->L->head = this_node->next;
	else
		this_node->prev->next = this_node->next;

	if (this_node->next == 0)  /* removing tail */
		iter->L->tail = this_node->prev;
	else
		this_node->next->prev = this_node->prev;

	free(this_node);
	--iter->L->size;
}
