#include <stdlib.h>
#include <string.h>
#include "rbtmap.h"
#include "stringset.h"
#include "utility.h"

struct stringset {
	struct rbtmap *rbt;
};

struct ss_iter {
	struct rbtmap_iter *iter;
};

static int void_strcmp(const void *a, const void *b)
{
	return strcmp((const char *)a, (const char *)b);
}

static void stringset_rbtmap_cleanup(void *key, void *value)
{
	free(key);  (void)value;
}

struct stringset *stringset_new(void)
{
	struct stringset *s = checked_malloc(sizeof(*s));
	s->rbt = rbtmap_new(void_strcmp);
	return s;
}

void stringset_free(struct stringset *s)
{
	if (s == 0)  return;
	rbtmap_free(s->rbt, stringset_rbtmap_cleanup);
	free(s);
}

size_t stringset_size(struct stringset *s)
{
	return (size_t)rbtmap_size(s->rbt);
}

void stringset_add(struct stringset *s, const char *t)
{
	if (s == 0)  return;
	rbtmap_put(s->rbt, copy_string(t), 0, stringset_rbtmap_cleanup);
}

struct stringset *stringset_copy(struct stringset *s)
{
	if (s == 0)  return 0;
	struct stringset *copy = stringset_new();
	stringset_add_all(copy, s);
	return copy;
}

void stringset_add_all(struct stringset *to, struct stringset *from)
{
	if (to == 0 || from == 0)  return;
	struct ss_iter *i = stringset_iterator(from);
	while (stringset_has_next(i))
		stringset_add(to, stringset_next(i));
	stringset_iterator_free(i);
}

struct stringset *stringset_intersection(struct stringset *s1,
		struct stringset *s2)
{
	if (s1 == 0 || s2 == 0)  return 0;
	struct stringset *intxn = stringset_new();
	struct ss_iter *i1 = stringset_iterator(s1);
	struct ss_iter *i2 = stringset_iterator(s2);
	if (stringset_has_next(i1) && stringset_has_next(i2)) {
		const char *t1 = stringset_next(i1);
		const char *t2 = stringset_next(i2);
		for (;;) {
			int cmp = strcmp(t1, t2);
			if (cmp < 0) {
				if (!stringset_has_next(i1))  break;
				t1 = stringset_next(i1);
			} else if (cmp > 0) {
				if (!stringset_has_next(i2))  break;
				t2 = stringset_next(i2);
			} else {
				stringset_add(intxn, t1);
				if (!stringset_has_next(i1) || !stringset_has_next(i2))  break;
				t1 = stringset_next(i1);
				t2 = stringset_next(i2);
			}
		}
	}
	stringset_iterator_free(i1);
	stringset_iterator_free(i2);
	return intxn;
}

struct ss_iter *stringset_iterator(struct stringset *s)
{
	if (s == 0)  return 0;
	struct ss_iter *i = checked_malloc(sizeof(*i));
	i->iter = rbtmap_iterator(s->rbt);
	return i;
}

int stringset_has_next(struct ss_iter *i)
{
	return i == 0 || rbtmap_has_next(i->iter);
}

const char *stringset_next(struct ss_iter *i)
{
	if (i == 0 || !rbtmap_has_next(i->iter))  return 0;
	struct rbtmap_entry entry = rbtmap_next(i->iter);
	return (const char *)entry.key;
}

void stringset_iterator_free(struct ss_iter *i)
{
	if (i == 0)  return;
	rbtmap_iterator_free(i->iter);
	free(i);
}
