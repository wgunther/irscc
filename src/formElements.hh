#include <string>
#include <sstream>
#include <memory>
#include <iostream>
#include <list>
#include <cstdlib>
#include <map>
#include <vector>
#include <cctype>
#include <algorithm>
#include "formdesc.h"
#include "randomThings.hh"
#include "llist.h"
using std::string;
using std::list;
using std::unique_ptr;
using std::move;
using std::swap;
using std::map;
using std::vector;
using std::ostream;
using std::endl;
using std::ostringstream;

class FormDesc {
	// This is a wraper to struct form_desc
	// in form_desc.h 
	private:
		// Static:
			static map<const form_desc*,string> formNameMap;
			static map<const form_desc*,string> scheduleNameMap;
			static vector<string> availableFormNames;
			static vector<string> availableScheduleNames;
		// Insance:
		const form_desc *formDesc;
	public:
		// Static:
			static void initalizeAvailableIdentifiers();
			static string lookupIdentifier(const form_desc*);
			static bool isForm(const form_desc*);
			static bool isSchedule(const form_desc*);
		// Instance:
			FormDesc (const form_desc *f) { formDesc = f; }
			FormDesc (const form_desc &f) { formDesc = &f; }
			~FormDesc () {} // Memory managed elsewhere.
			bool isForm();
			bool isSchedule();
			string formOrSchedule ();
			string identifier();
		// Overloads:
			// Pretend to be a form_desc pointer
			operator string();
			const form_desc* operator->() { return this->formDesc; }
			const form_desc& operator*() { return *(this->formDesc); }
			operator const form_desc*() { return this->formDesc; }
			bool operator==(const form_desc *s) { return s == this->formDesc; }
		// Other:
			// Overload wakkas
			friend ostream& operator<< (ostream&, FormDesc);
};
class LineNumber {
	// This is a wrapper to struct line_number
	// in form_desc.h 
	private:
		line_number lineNo;
	public:
		// Static:
		// Instance:
			LineNumber () {}
			LineNumber (line_number l) { lineNo = l; }
			~LineNumber () {} 
			string longNumber ();
			string shortNumber ();
			string simpleNumber ();
			FormDesc getForm () { return FormDesc{lineNo.form}; }
		// Overloads:
			operator string() { return this->shortNumber(); } 
			// Pretend to be a line_number pointer
			line_number* operator->() { return &lineNo; } 
			line_number& operator*() { return lineNo; }
			operator line_number() { return lineNo; }
			LineNumber operator=(line_number l) { return (lineNo = l); }
		// Other:
			// Overload wakkas: output shortNumber.
		friend ostream& operator<< (ostream&, LineNumber);
};
class LineDescription {
	// This is a wrapper to struct line_desc
	// in form_desc.h 
	private:
		// Static:
			static map<int,string> labelMap;
		// Instance: 
		line_desc *lineDesc;
		FormDesc formOn; // Refers to the form that the line being described is on.
	public:
		// Static:
			static string lookupLabel(int i);
			// ADD STATIC CLASSES TO PICK LABELS
		// Instance:
			LineDescription (line_desc *l, const form_desc *f): lineDesc{l}, formOn{f} {}
			~LineDescription () {} // Memory managed elsewhere.
			FormDesc getForm() { return formOn; }
			string getLineNumber();
			string getSimpleLineNumber();
			bool isLabeled();
			string getLabel();
			bool isEnterAlso();
			bool isEnterAlsoRecursive();
			bool isReturnRecursive();
			bool isGotoAfter();
			LineNumber enterAlsoLineNumber();
			LineNumber gotoAfterLineNumber();
		// Overloads:
			// Pretend to be a line_desc pointer
			line_desc* operator->() { return lineDesc; }
			line_desc& operator*() { return *lineDesc; }
			operator line_desc*() { return this->lineDesc; }
			operator line_desc() { return *(this->lineDesc); } 
};
// TeX OUTPUT CONTAINERS //
// FormElement Classes
class FormElement {
	protected:
		LineDescription lineDesc;
	public:
		FormElement(line_desc* l, const form_desc* f): lineDesc{l,f} { }
		virtual ~FormElement() {};
		virtual ostream& print(ostream&) = 0;
		friend ostream& operator<<(ostream&,FormElement&);
};
class FormRow : public FormElement {
	// Form rows actually print something on the form,
	// namely the result of message.
	// Form Rows also can all be labeled.
	private:
		typedef FormElement super;
	protected:
		string message;
		virtual string getLabel();
	public:
		FormRow(line_desc* l, const form_desc* f) : FormElement{l,f}, message{} { this->message.reserve(80); }
		virtual ~FormRow() {};
		virtual ostream& print(ostream&);
		void appendMessage(const string&);
		void setMessage(const string&);

};
class ExpressionRow : public FormRow {
	// Expression Rows can have "enteralso"'s
	private:
		typedef FormRow super;
	public:
		ExpressionRow(line_desc* l, const form_desc* f) : FormRow{l,f} {}
		virtual ~ExpressionRow () {}
		virtual ostream& print(ostream&);
	
};
class FunctionArgsToggle : public FormElement {
		bool indent;
	public:
		FunctionArgsToggle(line_desc* l, const form_desc* f, bool state)
			:FormElement{l,f},indent{state} {};
		~FunctionArgsToggle() {}
		ostream& print(ostream&);

};
class DisplayRow : public FormElement {
	// Row with no input expected (so the right number thingy is greyed out)
	protected:
		string message;
	public:
		DisplayRow(line_desc* l, const form_desc* f): FormElement{l,f} {}
		~DisplayRow() { }
		ostream& print(ostream&);
		void appendMessage(const string&);
		void setMessage(const string&);
};
// FormSection Classes
class FormSection {
	protected:
		list<unique_ptr<FormElement>> elements;
		string printHeader{};
		string printFooter{};
	public:
		int partNo = 1;
		FormSection() {}
		virtual ~FormSection() { };
		virtual void addElement (FormElement *r);
		virtual ostream& print (ostream&);
		friend ostream& operator<< (ostream&, FormSection&);
};
class GutterSection : public FormSection {
	private:
		typedef FormSection super;
	public:
		string header {capitalizeFirst(makeRandomSectionTitle())};
		string flavor;
		GutterSection() {}
		~GutterSection() {}
		ostream& print(ostream&);
};
class NormalSection : public FormSection {
	private:
		typedef FormSection super;
	public:
		string header {capitalizeFirst(makeRandomSectionTitle())};
		NormalSection() {}
		~NormalSection() {}
		ostream& print(ostream&);
};
// Form Classes
class Form {
	public:
		FormDesc F;
	protected:
		int catNo {1};
		string printHeader;
		string printFooter;
		list<unique_ptr<FormSection>> sections;	
		string title;
	public:
		Form(const form_desc* f) : F{f}, printHeader{}, printFooter{} {printHeader.reserve(80);printFooter.reserve(80); }
		virtual ~Form() {}; 
		virtual FormSection* getSec() = 0;
		virtual void addSec(FormSection* s);
		virtual ostream& print (ostream&);
		void setTitle(const string& s) { title = s; }
		friend ostream& operator<< (ostream&,Form&);
};
class Form1040 : public Form {
	typedef Form super;
	public:
		Form1040(const form_desc* f): Form{f} {}
		~Form1040() {}
		FormSection* getSec();
		ostream& print (ostream&);
};
class FormNormal : public Form {
	typedef Form super;
	public:
		FormNormal(const form_desc* f): Form{f} {this->setTitle(capitalizeFirst(makeRandomFormTitle()));}
		~FormNormal() {}
		FormSection* getSec();
		ostream& print (ostream&);
};
class ScheduleNormal : public Form {
	typedef Form super;
	string title;
	public:
		ScheduleNormal(const form_desc* f): Form{f} {this->setTitle(capitalizeFirst(makeRandomFormTitle()));}
		~ScheduleNormal() {}
		FormSection* getSec();
		ostream& print (ostream&);
		void setTitle(const string& s) { title = s; }
};
