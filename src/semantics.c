#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "llist.h"
#include "semantics.h"
#include "stringset.h"
#include "symtab.h"
#include "utility.h"

#define NOT_SUPPORTED(feature)  \
	do {  \
		fprintf(stderr, "Unsupported feature: %s. Sorry!\n", feature);  \
		exit(EXIT_FAILURE);  \
	} while (0)

static struct symtab_node *context = 0;

void sem_init(void)
{
	assert(context == 0);
	context = symtab_new();
}

void sem_release(void)
{
	assert(context != 0);
	symtab_free(context);
	context = 0;
}

/* ===== Specifiers and qualifiers ========================================= */

static void sem_spec(spec_t specifiers)
{
	assert(specifiers != 0);
	/* check storage-class specifiers */
	int storage_count = !!(specifiers & SPEC_AUTO)
			+ !!(specifiers & SPEC_REGISTER) + !!(specifiers & SPEC_STATIC)
			+ !!(specifiers & SPEC_EXTERN) + !!(specifiers & SPEC_TYPEDEF);
	if (storage_count > 1) {
		fprintf(stderr, "Error: multiple storage-class specifiers\n");
		exit(EXIT_FAILURE);
	} else if (storage_count > 0) {
		fprintf(stderr, "Warning: ignoring storage-class specifier\n");
	}
	/* check type specifiers */
	switch (specifiers & SPEC_MASK_TYPE) {
		case SPEC_VOID:
			break;
		case SPEC_CHAR:
			break;
		case SPEC_SIGNED | SPEC_CHAR:
			break;
		case SPEC_UNSIGNED | SPEC_CHAR:
			fprintf(stderr, "Warning: ignoring 'unsigned' specifier\n");
			break;
		case SPEC_SHORT:
		case SPEC_SIGNED | SPEC_SHORT:
		case SPEC_SHORT | SPEC_INT:
		case SPEC_SIGNED | SPEC_SHORT | SPEC_INT:
			break;
		case SPEC_UNSIGNED | SPEC_SHORT:
		case SPEC_UNSIGNED | SPEC_SHORT | SPEC_INT:
			fprintf(stderr, "Warning: ignoring 'unsigned' specifier\n");
			break;
		case SPEC_INT:
		case SPEC_SIGNED:
		case SPEC_SIGNED | SPEC_INT:
			break;
		case SPEC_UNSIGNED:
		case SPEC_UNSIGNED | SPEC_INT:
			fprintf(stderr, "Warning: ignoring 'unsigned' specifier\n");
			break;
		case SPEC_LONG:
		case SPEC_SIGNED | SPEC_LONG:
		case SPEC_LONG | SPEC_INT:
		case SPEC_SIGNED | SPEC_LONG | SPEC_INT:
			break;
		case SPEC_UNSIGNED | SPEC_LONG:
		case SPEC_UNSIGNED | SPEC_LONG | SPEC_INT:
			fprintf(stderr, "Warning: ignoring 'unsigned' specifier\n");
			break;
		case SPEC_FLOAT:
			break;
		case SPEC_DOUBLE:
			break;
		case SPEC_LONG | SPEC_DOUBLE:
			break;
		case 0:
			fprintf(stderr, "Error: missing type specifier\n");
			exit(EXIT_FAILURE);
		default:
			fprintf(stderr, "Error: invalid combination of type specifiers:");
			if (specifiers & SPEC_SIGNED)    fprintf(stderr, " signed");
			if (specifiers & SPEC_UNSIGNED)  fprintf(stderr, " unsigned");
			if (specifiers & SPEC_SHORT)     fprintf(stderr, " short");
			if (specifiers & SPEC_LONG)      fprintf(stderr, " long");
			if (specifiers & SPEC_VOID)      fprintf(stderr, " void");
			if (specifiers & SPEC_CHAR)      fprintf(stderr, " char");
			if (specifiers & SPEC_INT)       fprintf(stderr, " int");
			if (specifiers & SPEC_FLOAT)     fprintf(stderr, " float");
			if (specifiers & SPEC_DOUBLE)    fprintf(stderr, " double");
			fprintf(stderr, "\n");
			exit(EXIT_FAILURE);
	}
	/* check type qualifiers */
	if (specifiers & SPEC_CONST)
		fprintf(stderr, "Warning: ignoring 'const' specifier\n");
	if (specifiers & SPEC_VOLATILE)
		fprintf(stderr, "Warning: ignoring 'volatile' specifier\n");
}

static struct ast_type *type_from_specifiers(spec_t specifiers)
{
	sem_spec(specifiers);
	struct ast_type *type = checked_malloc(sizeof(*type));
	type->meta = TYPE_BASIC;
	if (ast_spec_is_integer(specifiers))
		type->u.basic = TYPE_INTEGER;
	else if (ast_spec_is_floating(specifiers))
		type->u.basic = TYPE_FLOATING;
	else {
		assert(specifiers & SPEC_VOID);
		type->u.basic = TYPE_VOID;
	}
	return type;
}

/* ===== Function definition nodes ========================================= */

static void sem_stmt_COMPOUND_extra_decls(struct ast_stmt_node *stmt,
		struct ast_fn_node *fn, struct linked_list *extra_decls);

void sem_fn(struct ast_fn_node *fn)
{
	assert(fn != 0);  assert(fn->dxr != 0);  assert(fn->dxr->type == DXR_FN);
	assert(fn->body != 0);  assert(fn->body->stmt_code == COMPOUND_STMT);
	struct ast_type *outer = type_from_specifiers(fn->specifiers);
	struct ast_type *dxr_type = sem_dxr(fn->dxr, outer);
	if (dxr_type->meta != TYPE_FN) {
		fprintf(stderr, "Error: function definition missing parameter list\n");
		exit(EXIT_FAILURE);
	}
	fn->symtab_context = context;
	symtab_declare(context, fn->dxr->id, dxr_type);
	sem_stmt_COMPOUND_extra_decls(fn->body, fn, fn->dxr->u.fn.args);
	if (!ast_type_is_void(dxr_type) && !fn->have_returned
			&& strcmp(fn->dxr->id, "main") != 0)
		fprintf(stderr, "Warning: control reaches the end of non-void "
				"function '%s'\n", fn->dxr->id);
}

/* ===== Declaration nodes ================================================= */

void sem_decl(struct ast_decl_node *decl)
{
	assert(decl != 0);
	struct ll_iter *iter = ll_iterator(decl->idxrs);
	while (ll_has_next(iter)) {
		struct ast_idxr_node * const idxr = ll_next_ptr(iter);
		sem_idxr(idxr, decl->specifiers);
		stringset_add_all(decl->vars_read, idxr->vars_read);
		stringset_add_all(decl->vars_written, idxr->vars_written);
	}
	ll_iterator_free(iter);
}

void sem_idxr(struct ast_idxr_node *idxr, spec_t specifiers)
{
	assert(idxr != 0);
	struct ast_type *outer = type_from_specifiers(specifiers);
	struct ast_type *dxr_type = sem_dxr(idxr->dxr, outer);
	symtab_declare(context, idxr->dxr->id, dxr_type);
	struct ast_expr_node * const init = idxr->init;
	if (init != 0) {
		sem_expr(init);
		/* TODO: proper type-checking */
		stringset_add_all(idxr->vars_read, init->vars_read);
		stringset_add_all(idxr->vars_written, init->vars_written);
	}
}

struct ast_type *sem_dxr(struct ast_dxr_node *dxr, struct ast_type *outer)
{
	assert(dxr != 0);
	struct ast_type *t;
	switch (dxr->type) {
		case DXR_ID:
			t = outer;
			break;
		case DXR_PTR:
			fprintf(stderr, "Pointers are not supported yet. Sorry!\n");
			exit(EXIT_FAILURE);
			break;
		case DXR_ARR:
			fprintf(stderr, "Arrays are not supported yet. Sorry!\n");
			exit(EXIT_FAILURE);
			break;
		case DXR_FN:
		{
			if (outer->meta != TYPE_BASIC) {
				fprintf(stderr, "Functions returning derived types are not "
						"supported yet. Sorry!\n");
				exit(EXIT_FAILURE);
			}
			t = checked_malloc(sizeof(*t));
			t->meta = TYPE_FN;
			t->u.fn.ret = outer;
			t->u.fn.args = ll_new();
			struct ll_iter *iter = ll_iterator(dxr->u.fn.args);
			while (ll_has_next(iter)) {
				struct ast_decl_node *arg_decl = ll_next_ptr(iter);
				assert(arg_decl != 0);  assert(arg_decl->idxrs != 0);
				assert(ll_size(arg_decl->idxrs) == 1);
				struct ast_idxr_node *arg_idxr = ll_head_ptr(arg_decl->idxrs);
				assert(arg_idxr->dxr != 0);
				if (arg_idxr->init != 0) {
					fprintf(stderr, "Error: function parameters cannot have "
							"initializers\n");
					exit(EXIT_FAILURE);
				}
				struct ast_type *o = type_from_specifiers(arg_decl->specifiers);
				struct ast_type *arg_type = sem_dxr(arg_idxr->dxr, o);
				if (arg_type->meta != TYPE_BASIC) {
					fprintf(stderr, "Functions with parameters of derived "
							"types are not supported yet. Sorry!\n");
					exit(EXIT_FAILURE);
				}
				ll_push_ptr(t->u.fn.args, arg_type);
			}
			ll_iterator_free(iter);
			break;
		}
	}
	return t;
}

/* ===== Statement nodes =================================================== */

static void sem_stmt_EMPTY(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_CONTINUE(struct ast_stmt_node *stmt,
		struct ast_fn_node *fn);
static void sem_stmt_BREAK(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_RETURN(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_GOTO(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_DEFAULT(struct ast_stmt_node *stmt,
		 struct ast_fn_node *fn);
static void sem_stmt_EXPR(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_RETURN_EXPR(struct ast_stmt_node *stmt,
		 struct ast_fn_node *fn);
static void sem_stmt_LABELED(struct ast_stmt_node *stmt,
		 struct ast_fn_node *fn);
static void sem_stmt_CASE(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_IF(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_SWITCH(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_WHILE(struct ast_stmt_node *stmt, struct ast_fn_node *fn);
static void sem_stmt_DO_WHILE(struct ast_stmt_node *stmt,
		 struct ast_fn_node *fn);
static void sem_stmt_IF_ELSE(struct ast_stmt_node *stmt,
		 struct ast_fn_node *fn);
static void sem_stmt_COMPOUND(struct ast_stmt_node *stmt,
		 struct ast_fn_node *fn);
static void sem_stmt_FOR(struct ast_stmt_node *stmt, struct ast_fn_node *fn);

#define CHECK_HAVE_RETURNED                                                   \
	do {                                                                      \
		if (fn->have_returned) {                                              \
			fprintf(stderr, "In function '%s':\n    'return' statement is "   \
					"not supported yet except as the very last statement\n"   \
					"    in a function, not nested inside any other "         \
					"statement. Sorry!\n", fn->dxr->id);                      \
			exit(EXIT_FAILURE);                                               \
		}                                                                     \
	} while (0)

void sem_stmt(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
{
	assert(stmt != 0);
	CHECK_HAVE_RETURNED;
	stmt->fn = fn;
	stmt->symtab_context = context;
	switch (stmt->stmt_code) {
		case EMPTY_STMT:        sem_stmt_EMPTY(stmt, fn);        break;
		case CONTINUE_STMT:     sem_stmt_CONTINUE(stmt, fn);     break;
		case BREAK_STMT:        sem_stmt_BREAK(stmt, fn);        break;
		case RETURN_STMT:       sem_stmt_RETURN(stmt, fn);       break;
		case GOTO_STMT:         sem_stmt_GOTO(stmt, fn);         break;
		case DEFAULT_STMT:      sem_stmt_DEFAULT(stmt, fn);      break;
		case EXPR_STMT:         sem_stmt_EXPR(stmt, fn);         break;
		case RETURN_EXPR_STMT:  sem_stmt_RETURN_EXPR(stmt, fn);  break;
		case LABELED_STMT:      sem_stmt_LABELED(stmt, fn);      break;
		case CASE_STMT:         sem_stmt_CASE(stmt, fn);         break;
		case IF_STMT:           sem_stmt_IF(stmt, fn);           break;
		case SWITCH_STMT:       sem_stmt_SWITCH(stmt, fn);       break;
		case WHILE_STMT:        sem_stmt_WHILE(stmt, fn);        break;
		case DO_WHILE_STMT:     sem_stmt_DO_WHILE(stmt, fn);     break;
		case IF_ELSE_STMT:      sem_stmt_IF_ELSE(stmt, fn);      break;
		case COMPOUND_STMT:     sem_stmt_COMPOUND(stmt, fn);     break;
		case FOR_STMT:          sem_stmt_FOR(stmt, fn);          break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in sem_stmt\n");
			exit(EXIT_FAILURE);
	}
}

static void sem_stmt_EMPTY(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
{
	(void)stmt;  (void)fn;
}

static void sem_stmt_CONTINUE(struct ast_stmt_node *stmt,
		struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("continue"); }

static void sem_stmt_BREAK(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("break"); }

static void sem_stmt_RETURN(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
{
	(void)stmt;
	assert(fn != 0);  assert(fn->dxr != 0);  assert(fn->dxr->id != 0);
	struct symtab_node *context = fn->symtab_context;
	struct symtab_entry *entry = symtab_search(context, fn->dxr->id);
	assert(entry != 0);  assert(entry->type != 0);
	if (!ast_type_is_void(entry->type)) {
		fprintf(stderr, "Error: bare 'return' statement in non-void "
				"function '%s'\n", fn->dxr->id);
		exit(EXIT_FAILURE);
	}
	fn->have_returned = 1;
}

static void sem_stmt_GOTO(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("goto"); }

static void sem_stmt_DEFAULT(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("default"); }
	/* don't forget vars_read and vars_written when this is implemented */

static void sem_stmt_EXPR(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
{
	(void)fn;
	sem_expr(stmt->u.expr);
	stringset_add_all(stmt->vars_read, stmt->u.expr->vars_read);
	stringset_add_all(stmt->vars_written, stmt->u.expr->vars_written);
}

static void sem_stmt_RETURN_EXPR(struct ast_stmt_node *stmt,
		struct ast_fn_node *fn)
{
	assert(fn != 0);  assert(fn->dxr != 0);  assert(fn->dxr->id != 0);
	struct symtab_node *context = fn->symtab_context;
	struct symtab_entry *entry = symtab_search(context, fn->dxr->id);
	assert(entry != 0);  assert(entry->type != 0);
	assert(entry->type->meta = TYPE_FN);  assert(entry->type->u.fn.ret != 0);
	sem_expr(stmt->u.expr);
	assert(stmt->u.expr->type != 0);
	if (!ast_type_equiv(stmt->u.expr->type, entry->type->u.fn.ret)) {
		fprintf(stderr, "Error in function '%s':\n    Type of expression "
				"returned does not match declared return type.\n    Declared "
				"return type: ", fn->dxr->id);
		ast_print_type(stderr, entry->type->u.fn.ret);
		fprintf(stderr, "\n    Type of expression returned: ");
		ast_print_type(stderr, stmt->u.expr->type);
		fprintf(stderr, "\n");
		exit(EXIT_FAILURE);
	}
	stringset_add_all(stmt->vars_read, stmt->u.expr->vars_read);
	stringset_add_all(stmt->vars_written, stmt->u.expr->vars_written);
	fn->have_returned = 1;
}

static void sem_stmt_LABELED(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("labeled statements"); }
	/* don't forget vars_read and vars_written when this is implemented */

static void sem_stmt_CASE(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("case"); }
	/* don't forget vars_read and vars_written when this is implemented */

static void sem_stmt_IF(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
{
	struct ast_expr_node * const cond = stmt->u.cond_stmt.cond;
	struct ast_stmt_node * const body = stmt->u.cond_stmt.body;
	sem_expr(cond);
	sem_stmt(body, fn);
	CHECK_HAVE_RETURNED;
	stringset_add_all(stmt->vars_read, cond->vars_read);
	stringset_add_all(stmt->vars_read, body->vars_read);
	stringset_add_all(stmt->vars_written, cond->vars_written);
	stringset_add_all(stmt->vars_written, body->vars_written);
}

static void sem_stmt_SWITCH(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("switch"); }
	/* don't forget vars_read and vars_written when this is implemented */

static void sem_stmt_WHILE(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
{
	struct ast_expr_node * const cond = stmt->u.cond_stmt.cond;
	struct ast_stmt_node * const body = stmt->u.cond_stmt.body;
	sem_expr(cond);
	sem_stmt(body, fn);
	CHECK_HAVE_RETURNED;
	stringset_add_all(stmt->vars_read, cond->vars_read);
	stringset_add_all(stmt->vars_read, body->vars_read);
	stringset_add_all(stmt->vars_written, cond->vars_written);
	stringset_add_all(stmt->vars_written, body->vars_written);
}

static void sem_stmt_DO_WHILE(struct ast_stmt_node *stmt,
		struct ast_fn_node *fn)
{
	struct ast_expr_node * const cond = stmt->u.cond_stmt.cond;
	struct ast_stmt_node * const body = stmt->u.cond_stmt.body;
	sem_expr(cond);
	sem_stmt(body, fn);
	CHECK_HAVE_RETURNED;
	stringset_add_all(stmt->vars_read, cond->vars_read);
	stringset_add_all(stmt->vars_read, body->vars_read);
	stringset_add_all(stmt->vars_written, cond->vars_written);
	stringset_add_all(stmt->vars_written, body->vars_written);
}

static void sem_stmt_IF_ELSE(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
{
	struct ast_expr_node * const cond = stmt->u.if_else.cond;
	struct ast_stmt_node * const if_body = stmt->u.if_else.if_body;
	struct ast_stmt_node * const else_body = stmt->u.if_else.else_body;
	sem_expr(cond);
	sem_stmt(if_body, fn);
	sem_stmt(else_body, fn);
	CHECK_HAVE_RETURNED;
	stringset_add_all(stmt->vars_read, cond->vars_read);
	stringset_add_all(stmt->vars_read, if_body->vars_read);
	stringset_add_all(stmt->vars_read, else_body->vars_read);
	stringset_add_all(stmt->vars_written, cond->vars_written);
	stringset_add_all(stmt->vars_written, if_body->vars_written);
	stringset_add_all(stmt->vars_written, else_body->vars_written);
}

/* compound statement with a list of additional declarations to be processed
   first (for function parameters) */
static void sem_stmt_COMPOUND_extra_decls(struct ast_stmt_node *stmt,
		struct ast_fn_node *fn, struct linked_list *extra_decls)
{
	stmt->fn = fn;  /* because we might have bypassed sem_stmt */
	/* enter block */
	context = stmt->symtab_context = symtab_enter_block(context, stmt);
	/* process extra declarations */
	struct ll_iter *iter;
	if (extra_decls) {
		iter = ll_iterator(extra_decls);
		while (ll_has_next(iter))
			sem_decl(ll_next_ptr(iter));
		ll_iterator_free(iter);
	}
	/* process declarations at top of compound statement */
	iter = ll_iterator(stmt->u.cpd.decls);
	while (ll_has_next(iter))
		sem_decl(ll_next_ptr(iter));
	ll_iterator_free(iter);
	/* process statements */
	struct stringset *vars_read_inside = stringset_new();
	struct stringset *vars_written_inside = stringset_new();
	iter = ll_iterator(stmt->u.cpd.stmts);
	while (ll_has_next(iter)) {
		struct ast_stmt_node * const child = ll_next_ptr(iter);
		sem_stmt(child, fn);
		stringset_add_all(vars_read_inside, child->vars_read);
		stringset_add_all(vars_written_inside, child->vars_written);
	}
	ll_iterator_free(iter);
	/* identify variables that are read and written --- but ONLY variables that
	   are visible outside this block */
	struct ss_iter *var_iter = stringset_iterator(vars_read_inside);
	while (stringset_has_next(var_iter)) {
		const char * const id = stringset_next(var_iter);
		const struct symtab_entry *entry = symtab_lookup(context, id);
		if (entry->block != stmt)
			stringset_add(stmt->vars_read, id);
	}
	stringset_iterator_free(var_iter);
	var_iter = stringset_iterator(vars_written_inside);
	while (stringset_has_next(var_iter)) {
		const char * const id = stringset_next(var_iter);
		const struct symtab_entry *entry = symtab_lookup(context, id);
		if (entry->block != stmt)
			stringset_add(stmt->vars_written, id);
	}
	stringset_iterator_free(var_iter);
	/* leave block */
	context = symtab_leave_block(context);
}

static void sem_stmt_COMPOUND(struct ast_stmt_node *stmt,
		struct ast_fn_node *fn)
{
	sem_stmt_COMPOUND_extra_decls(stmt, fn, 0);
}

static void sem_stmt_FOR(struct ast_stmt_node *stmt, struct ast_fn_node *fn)
	{ (void)stmt;  (void)fn;  NOT_SUPPORTED("for"); }
	/* don't forget vars_read and vars_written when this is implemented */

/* ===== Expression nodes ================================================== */

static void sem_expr_INTEGER_CONSTANT(struct ast_expr_node *expr);
static void sem_expr_FLOATING_CONSTANT(struct ast_expr_node *expr);
static void sem_expr_IDENTIFIER(struct ast_expr_node *expr);
static void sem_expr_UNARY_OP(struct ast_expr_node *expr);
	static void sem_expr_ADDR_OF(struct ast_expr_node *expr);
	static void sem_expr_USTAR(struct ast_expr_node *expr);
	static void sem_expr_UPLUS(struct ast_expr_node *expr);
	static void sem_expr_UMINUS(struct ast_expr_node *expr);
	static void sem_expr_BIT_NOT(struct ast_expr_node *expr);
	static void sem_expr_LOGIC_NOT(struct ast_expr_node *expr);
	static void sem_expr_SIZEOF(struct ast_expr_node *expr);
static void sem_expr_INC_DEC(struct ast_expr_node *expr);
	static void sem_expr_POST_INC(struct ast_expr_node *expr);
	static void sem_expr_POST_DEC(struct ast_expr_node *expr);
	static void sem_expr_PRE_INC(struct ast_expr_node *expr);
	static void sem_expr_PRE_DEC(struct ast_expr_node *expr);
static void sem_expr_BINARY_OP(struct ast_expr_node *expr);
	static void sem_expr_BSTAR(struct ast_expr_node *expr);
	static void sem_expr_SLASH(struct ast_expr_node *expr);
	static void sem_expr_MODULO(struct ast_expr_node *expr);
	static void sem_expr_BPLUS(struct ast_expr_node *expr);
	static void sem_expr_BMINUS(struct ast_expr_node *expr);
	static void sem_expr_LSHIFT(struct ast_expr_node *expr);
	static void sem_expr_RSHIFT(struct ast_expr_node *expr);
	static void sem_expr_LESS(struct ast_expr_node *expr);
	static void sem_expr_LESS_EQUALS(struct ast_expr_node *expr);
	static void sem_expr_GREATER(struct ast_expr_node *expr);
	static void sem_expr_GREATER_EQUALS(struct ast_expr_node *expr);
	static void sem_expr_EQUALS_EQUALS(struct ast_expr_node *expr);
	static void sem_expr_NOT_EQUALS(struct ast_expr_node *expr);
	static void sem_expr_BIT_AND(struct ast_expr_node *expr);
	static void sem_expr_BIT_XOR(struct ast_expr_node *expr);
	static void sem_expr_BIT_OR(struct ast_expr_node *expr);
	static void sem_expr_LOGIC_AND(struct ast_expr_node *expr);
	static void sem_expr_LOGIC_OR(struct ast_expr_node *expr);
	static void sem_expr_COMMA(struct ast_expr_node *expr);
static void sem_expr_ASSIGNMENT(struct ast_expr_node *expr);
	static void sem_expr_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_PLUS_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_MINUS_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_STAR_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_SLASH_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_MODULO_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_AND_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_XOR_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_OR_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_LSHIFT_ASSIGN(struct ast_expr_node *expr);
	static void sem_expr_RSHIFT_ASSIGN(struct ast_expr_node *expr);
static void sem_expr_FUNCTION_CALL(struct ast_expr_node *expr);

void sem_expr(struct ast_expr_node *expr)
{
	assert(expr != 0);
	expr->symtab_context = context;
	switch (expr->expr_code) {
		case INTEGER_CONSTANT:   sem_expr_INTEGER_CONSTANT(expr);   break;
		case FLOATING_CONSTANT:  sem_expr_FLOATING_CONSTANT(expr);  break;
		case IDENTIFIER:         sem_expr_IDENTIFIER(expr);         break;
		case UNARY_OP:           sem_expr_UNARY_OP(expr);           break;
		case INC_DEC:            sem_expr_INC_DEC(expr);            break;
		case BINARY_OP:          sem_expr_BINARY_OP(expr);          break;
		case ASSIGNMENT:         sem_expr_ASSIGNMENT(expr);         break;
		case FUNCTION_CALL:      sem_expr_FUNCTION_CALL(expr);      break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in sem_expr\n");
			exit(EXIT_FAILURE);
	}
}

static void sem_expr_INTEGER_CONSTANT(struct ast_expr_node *expr)
{
	expr->type = ast_integer_type();
}

static void sem_expr_FLOATING_CONSTANT(struct ast_expr_node *expr)
{
	expr->type = ast_floating_type();
}

static void sem_expr_IDENTIFIER(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.id);
	expr->type = entry->type;
	stringset_add(expr->vars_read, expr->u.id);
}

static void sem_expr_UNARY_OP(struct ast_expr_node *expr)
{
	struct ast_expr_node * const arg = expr->u.unary.arg;
	sem_expr(arg);
	stringset_add_all(expr->vars_read, arg->vars_read);
	stringset_add_all(expr->vars_written, arg->vars_written);
	switch (expr->u.unary.op) {
		case ADDR_OF:    sem_expr_ADDR_OF(expr);    break;
		case USTAR:      sem_expr_USTAR(expr);      break;
		case UPLUS:      sem_expr_UPLUS(expr);      break;
		case UMINUS:     sem_expr_UMINUS(expr);     break;
		case BIT_NOT:    sem_expr_BIT_NOT(expr);    break;
		case LOGIC_NOT:  sem_expr_LOGIC_NOT(expr);  break;
		case SIZEOF:     sem_expr_SIZEOF(expr);     break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in sem_expr_UNARY_OP\n");
			exit(EXIT_FAILURE);
	}
}

static void sem_expr_INC_DEC(struct ast_expr_node *expr)
{
	stringset_add(expr->vars_read, expr->u.inc_dec.id);
	stringset_add(expr->vars_written, expr->u.inc_dec.id);
	switch (expr->u.inc_dec.op) {
		case POST_INC:   sem_expr_POST_INC(expr);   break;
		case POST_DEC:   sem_expr_POST_DEC(expr);   break;
		case PRE_INC:    sem_expr_PRE_INC(expr);    break;
		case PRE_DEC:    sem_expr_PRE_DEC(expr);    break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in sem_expr_INC_DEC_OP\n");
			exit(EXIT_FAILURE);
	}
}

static void sem_expr_BINARY_OP(struct ast_expr_node *expr)
{
	struct ast_expr_node * const left = expr->u.binary.left;
	struct ast_expr_node * const right = expr->u.binary.right;
	sem_expr(left);
	sem_expr(right);
	stringset_add_all(expr->vars_read, left->vars_read);
	stringset_add_all(expr->vars_read, right->vars_read);
	stringset_add_all(expr->vars_written, left->vars_written);
	stringset_add_all(expr->vars_written, right->vars_written);
	switch (expr->u.binary.op) {
		case BSTAR:           sem_expr_BSTAR(expr);           break;
		case SLASH:           sem_expr_SLASH(expr);           break;
		case MODULO:          sem_expr_MODULO(expr);          break;
		case BPLUS:           sem_expr_BPLUS(expr);           break;
		case BMINUS:          sem_expr_BMINUS(expr);          break;
		case LSHIFT:          sem_expr_LSHIFT(expr);          break;
		case RSHIFT:          sem_expr_RSHIFT(expr);          break;
		case LESS:            sem_expr_LESS(expr);            break;
		case LESS_EQUALS:     sem_expr_LESS_EQUALS(expr);     break;
		case GREATER:         sem_expr_GREATER(expr);         break;
		case GREATER_EQUALS:  sem_expr_GREATER_EQUALS(expr);  break;
		case EQUALS_EQUALS:   sem_expr_EQUALS_EQUALS(expr);   break;
		case NOT_EQUALS:      sem_expr_NOT_EQUALS(expr);      break;
		case BIT_AND:         sem_expr_BIT_AND(expr);         break;
		case BIT_XOR:         sem_expr_BIT_XOR(expr);         break;
		case BIT_OR:          sem_expr_BIT_OR(expr);          break;
		case LOGIC_AND:       sem_expr_LOGIC_AND(expr);       break;
		case LOGIC_OR:        sem_expr_LOGIC_OR(expr);        break;
		case COMMA:           sem_expr_COMMA(expr);           break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in sem_expr_BINARY_OP\n");
			exit(EXIT_FAILURE);
	}
}

static void sem_expr_ASSIGNMENT(struct ast_expr_node *expr)
{
	struct ast_expr_node * const value = expr->u.assign.value;
	sem_expr(value);
	if (expr->u.assign.op != ASSIGN)
		stringset_add(expr->vars_read, expr->u.assign.id);
	stringset_add_all(expr->vars_read, value->vars_read);
	stringset_add(expr->vars_written, expr->u.assign.id);
	stringset_add_all(expr->vars_written, value->vars_written);
	switch (expr->u.assign.op) {
		case ASSIGN:         sem_expr_ASSIGN(expr);         break;
		case PLUS_ASSIGN:    sem_expr_PLUS_ASSIGN(expr);    break;
		case MINUS_ASSIGN:   sem_expr_MINUS_ASSIGN(expr);   break;
		case STAR_ASSIGN:    sem_expr_STAR_ASSIGN(expr);    break;
		case SLASH_ASSIGN:   sem_expr_SLASH_ASSIGN(expr);   break;
		case MODULO_ASSIGN:  sem_expr_MODULO_ASSIGN(expr);  break;
		case AND_ASSIGN:     sem_expr_AND_ASSIGN(expr);     break;
		case XOR_ASSIGN:     sem_expr_XOR_ASSIGN(expr);     break;
		case OR_ASSIGN:      sem_expr_OR_ASSIGN(expr);      break;
		case LSHIFT_ASSIGN:  sem_expr_LSHIFT_ASSIGN(expr);  break;
		case RSHIFT_ASSIGN:  sem_expr_RSHIFT_ASSIGN(expr);  break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in sem_expr_ASSIGNMENT\n");
			exit(EXIT_FAILURE);
	}
}

static void sem_expr_ADDR_OF(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("unary &"); }

static void sem_expr_USTAR(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("unary *"); }

static void sem_expr_UPLUS(struct ast_expr_node *expr)
{
	expr->type = expr->u.unary.arg->type;
}

static void sem_expr_UMINUS(struct ast_expr_node *expr)
{
	expr->type = expr->u.unary.arg->type;
}

static void sem_expr_BIT_NOT(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("~ operator"); }

static void sem_expr_LOGIC_NOT(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("! operator"); }

static void sem_expr_SIZEOF(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("sizeof"); }

static void sem_expr_POST_INC(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Post-increment is not supported yet. Try a pre-increment "
			"instead. Sorry!\n");
	exit(EXIT_FAILURE);
}

static void sem_expr_POST_DEC(struct ast_expr_node *expr)
{
	(void)expr;
	fprintf(stderr, "Post-decrement is not supported yet. Try a pre-decrement "
			"instead. Sorry!\n");
	exit(EXIT_FAILURE);
}

static void sem_expr_PRE_INC(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.inc_dec.id);
	expr->type = entry->type;
}

static void sem_expr_PRE_DEC(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.inc_dec.id);
	expr->type = entry->type;
}

static void sem_expr_BSTAR(struct ast_expr_node *expr)
{
	if (ast_type_is_floating(expr->u.binary.left->type)
			|| ast_type_is_floating(expr->u.binary.right->type))
		expr->type = ast_floating_type();
	else
		expr->type = ast_integer_type();
}

static void sem_expr_SLASH(struct ast_expr_node *expr)
{
	if (ast_type_is_floating(expr->u.binary.left->type)
			|| ast_type_is_floating(expr->u.binary.right->type))
		expr->type = ast_floating_type();
	else
		expr->type = ast_integer_type();
}

static void sem_expr_MODULO(struct ast_expr_node *expr)
{
	if (ast_type_is_floating(expr->u.binary.left->type)
			|| ast_type_is_floating(expr->u.binary.right->type))
	{
		fprintf(stderr, "Error: can't use %% operator with floating-point "
				"arguments\n");
		exit(EXIT_FAILURE);
	}
	expr->type = ast_integer_type();
}

static void sem_expr_BPLUS(struct ast_expr_node *expr)
{
	if (ast_type_is_floating(expr->u.binary.left->type)
			|| ast_type_is_floating(expr->u.binary.right->type))
		expr->type = ast_floating_type();
	else
		expr->type = ast_integer_type();
}

static void sem_expr_BMINUS(struct ast_expr_node *expr)
{
	if (ast_type_is_floating(expr->u.binary.left->type)
			|| ast_type_is_floating(expr->u.binary.right->type))
		expr->type = ast_floating_type();
	else
		expr->type = ast_integer_type();
}

static void sem_expr_LSHIFT(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("<<"); }

static void sem_expr_RSHIFT(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED(">>"); }

static void sem_expr_LESS(struct ast_expr_node *expr)
{
	expr->type = ast_integer_type();
}

static void sem_expr_LESS_EQUALS(struct ast_expr_node *expr)
{
	expr->type = ast_integer_type();
}

static void sem_expr_GREATER(struct ast_expr_node *expr)
{
	expr->type = ast_integer_type();
}

static void sem_expr_GREATER_EQUALS(struct ast_expr_node *expr)
{
	expr->type = ast_integer_type();
}

static void sem_expr_EQUALS_EQUALS(struct ast_expr_node *expr)
{
	expr->type = ast_integer_type();
}

static void sem_expr_NOT_EQUALS(struct ast_expr_node *expr)
{
	expr->type = ast_integer_type();
}

static void sem_expr_BIT_AND(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("bitwise AND"); }

static void sem_expr_BIT_XOR(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("bitwise XOR"); }

static void sem_expr_BIT_OR(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("bitwise OR"); }

static void sem_expr_LOGIC_AND(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("&&"); }

static void sem_expr_LOGIC_OR(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("||"); }

static void sem_expr_COMMA(struct ast_expr_node *expr)
{
	expr->type = expr->u.binary.right->type;
}

static void sem_expr_ASSIGN(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.assign.id);
	expr->type = entry->type;
}

static void sem_expr_PLUS_ASSIGN(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.assign.id);
	expr->type = entry->type;
}

static void sem_expr_MINUS_ASSIGN(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.assign.id);
	expr->type = entry->type;
}

static void sem_expr_STAR_ASSIGN(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.assign.id);
	expr->type = entry->type;
}

static void sem_expr_SLASH_ASSIGN(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.assign.id);
	expr->type = entry->type;
}

static void sem_expr_MODULO_ASSIGN(struct ast_expr_node *expr)
{
	struct symtab_entry *entry = symtab_lookup(context, expr->u.assign.id);
	expr->type = entry->type;
}

static void sem_expr_AND_ASSIGN(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("&="); }

static void sem_expr_XOR_ASSIGN(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("^="); }

static void sem_expr_OR_ASSIGN(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("|="); }

static void sem_expr_LSHIFT_ASSIGN(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED("<<="); }

static void sem_expr_RSHIFT_ASSIGN(struct ast_expr_node *expr)
	{ (void)expr;  NOT_SUPPORTED(">>="); }

static void sem_expr_FUNCTION_CALL(struct ast_expr_node *expr)
{
	if (strcmp(expr->u.fn_call.id, "main") == 0) {
		fprintf(stderr, "Recursive 'main' is not supported yet. Sorry!\n");
		exit(EXIT_FAILURE);
	}
	struct symtab_entry *entry = symtab_lookup(context, expr->u.fn_call.id);
	if (entry->type->meta != TYPE_FN) {
		fprintf(stderr, "Error: '%s' is not a function\n", expr->u.fn_call.id);
		exit(EXIT_FAILURE);
	}
	expr->type = entry->type->u.fn.ret;
	struct ll_iter *iter = ll_iterator(expr->u.fn_call.args);
	while (ll_has_next(iter)) {
		struct ast_expr_node * const arg = ll_next_ptr(iter);
		sem_expr(arg);
		/* TODO: type-check arguments (must be assignable to parameter types) */
		stringset_add_all(expr->vars_read, arg->vars_read);
		stringset_add_all(expr->vars_written, arg->vars_written);
	}
	ll_iterator_free(iter);
}
