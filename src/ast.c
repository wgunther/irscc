#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ast.h"
#include "llist.h"
#include "stringset.h"
#include "utility.h"

/* Storage-class specifiers, type specifiers, and type qualifiers */

int ast_spec_is_integer(spec_t specifiers)
{
	return
	   (specifiers & SPEC_CHAR)
	|| (specifiers & SPEC_SHORT)
	|| (specifiers & SPEC_INT)
	|| ((specifiers & SPEC_LONG) && !(specifiers & SPEC_DOUBLE))
	|| (specifiers & SPEC_SIGNED)
	|| (specifiers & SPEC_UNSIGNED);
}

int ast_spec_is_floating(spec_t specifiers)
{
	return (specifiers & SPEC_FLOAT) || (specifiers & SPEC_DOUBLE);
}

/* Types */

static const struct ast_type integer_type =
		{ TYPE_BASIC, { .basic = TYPE_INTEGER } };
static const struct ast_type floating_type =
		{ TYPE_BASIC, { .basic = TYPE_FLOATING } };
static const struct ast_type void_type =
		{ TYPE_BASIC, { .basic = TYPE_VOID } };

const struct ast_type *ast_integer_type(void)  { return &integer_type;  }
const struct ast_type *ast_floating_type(void) { return &floating_type; }
const struct ast_type *ast_void_type(void)     { return &void_type;     }

void ast_free_type(struct ast_type *type)
{
	if (type == 0)  return;
	switch (type->meta) {
		case TYPE_BASIC:
			/* nothing to be done */
			break;
		case TYPE_FN:
			ast_free_type(type->u.fn.ret);
			while (ll_size(type->u.fn.args) > 0)
				ast_free_type(ll_shift_ptr(type->u.fn.args));
			ll_free(type->u.fn.args);
			break;
	}
	free(type);
}

int ast_type_is_integer(const struct ast_type *type)
{
	assert(type != 0);
	return type->meta == TYPE_BASIC && type->u.basic == TYPE_INTEGER;
}

int ast_type_is_floating(const struct ast_type *type)
{
	assert(type != 0);
	return type->meta == TYPE_BASIC && type->u.basic == TYPE_FLOATING;
}

int ast_type_is_void(const struct ast_type *type)
{
	assert(type != 0);
	return type->meta == TYPE_BASIC && type->u.basic == TYPE_VOID;
}

int ast_type_equiv(const struct ast_type *a, const struct ast_type *b)
{
	assert(a != 0);  assert(b != 0);
	if (a->meta != b->meta)  return 0;
	switch (a->meta) {
		case TYPE_BASIC:
			return a->u.basic == b->u.basic;
		case TYPE_FN:
		{
			if (!ast_type_equiv(a->u.fn.ret, b->u.fn.ret))  return 0;
			int result = 1;
			struct ll_iter *a_iter = ll_iterator(a->u.fn.args);
			struct ll_iter *b_iter = ll_iterator(b->u.fn.args);
			while (ll_has_next(a_iter)) {
				if (!ll_has_next(b_iter)) { result = 0;  goto CLEANUP; }
				if (!ast_type_equiv(ll_next_ptr(a_iter), ll_next_ptr(b_iter)))
					{ result = 0;  goto CLEANUP; }
			}
			if (ll_has_next(b_iter))  result = 0;
			CLEANUP:
			ll_iterator_free(a_iter);
			ll_iterator_free(b_iter);
			return result;
		}
		default:  /* this shouldn't happen */
			fprintf(stderr, "Unknown metatype: %ld\n", (long)a->meta);
			exit(EXIT_FAILURE);
	}
}

void ast_print_type(FILE *stream, const struct ast_type *type)
{
	if (type == 0)
		fprintf(stream, "<null>");
	else switch (type->meta) {
		case TYPE_BASIC:
			switch (type->u.basic) {
				case TYPE_INTEGER:   fprintf(stream, "integer");   break;
				case TYPE_FLOATING:  fprintf(stream, "floating");  break;
				case TYPE_VOID:      fprintf(stream, "void");      break;
				default:  /* this shouldn't happen */
					fprintf(stream, "unknown basic type <%ld>",
							(long)(type->u.basic));
					break;
			}
			break;
		case TYPE_FN:
			fprintf(stream, "function (");
			if (type->u.fn.args == 0)
				fprintf(stream, "<null>");
			else {
				int first_arg = 1;
				struct ll_iter *iter = ll_iterator(type->u.fn.args);
				while (ll_has_next(iter)) {
					if (first_arg)  first_arg = 0;
					else            fprintf(stream, ", ");
					ast_print_type(stream, ll_next_ptr(iter));
				}
				ll_iterator_free(iter);
			}
			fprintf(stream, ") returning ");
			ast_print_type(stream, type->u.fn.ret);
			break;
		default:  /* this shouldn't happen */
			fprintf(stream, "unknown metatype <%ld>", (long)(type->meta));
			break;
	}
}

/* Function definition nodes */

struct ast_fn_node *ast_new_fn(spec_t specifiers, struct ast_dxr_node *dxr,
		struct ast_stmt_node *body)
{
	assert(dxr != 0);  assert(body != 0);
	struct ast_fn_node *node = checked_malloc(sizeof(*node));
	node->specifiers = specifiers;
	node->dxr = dxr;
	node->body = body;
	node->symtab_context = 0;
	node->have_returned = 0;
	return node;
}

void ast_free_fn(struct ast_fn_node *node)
{
	if (node == 0)  return;
	assert(node->dxr != 0);  assert(node->body != 0);
	ast_free_dxr(node->dxr);
	ast_free_stmt(node->body);
	free(node);
}

/* Declaration nodes */

struct ast_decl_node *ast_new_decl(spec_t specifiers, struct linked_list *idxrs)
{
	assert(idxrs != 0);
	struct ast_decl_node *node = checked_malloc(sizeof(*node));
	node->specifiers = specifiers;
	node->idxrs = idxrs;
	node->vars_read = stringset_new();
	node->vars_written = stringset_new();
	return node;
}

void ast_free_decl(struct ast_decl_node *node)
{
	if (node == 0)  return;
	assert(node->idxrs != 0);
	while (ll_size(node->idxrs) > 0)
		ast_free_idxr(ll_shift_ptr(node->idxrs));
	ll_free(node->idxrs);
	stringset_free(node->vars_read);
	stringset_free(node->vars_written);
	free(node);
}

struct ast_idxr_node *ast_new_idxr(struct ast_dxr_node *dxr,
		struct ast_expr_node *init)
{
	assert(dxr != 0);  /* but init can be null */
	struct ast_idxr_node *node = checked_malloc(sizeof(*node));
	node->dxr = dxr;
	node->init = init;
	node->vars_read = stringset_new();
	node->vars_written = stringset_new();
	return node;
}

void ast_free_idxr(struct ast_idxr_node *node)
{
	if (node == 0)  return;
	assert(node->dxr != 0);
	ast_free_dxr(node->dxr);
	if (node->init != 0)  ast_free_expr(node->init);
	stringset_free(node->vars_read);
	stringset_free(node->vars_written);
	free(node);
}

struct ast_dxr_node *ast_new_dxr_id(const char *id)
{
	assert(id != 0);
	struct ast_dxr_node *node = checked_malloc(sizeof(*node));
	node->type = DXR_ID;
	node->id = node->u.id = copy_string(id);
	return node;
}

struct ast_dxr_node *ast_new_dxr_ptr(spec_t quals, struct ast_dxr_node *dxr)
{
	assert(dxr != 0);
	struct ast_dxr_node *node = checked_malloc(sizeof(*node));
	node->type = DXR_PTR;
	node->u.ptr.quals = quals;
	node->u.ptr.dxr = dxr;
	node->id = dxr->id;
	return node;
}

struct ast_dxr_node *ast_new_dxr_arr(struct ast_dxr_node *dxr,
		struct ast_expr_node *length)
{
	assert(length != 0);  assert(dxr != 0);
	struct ast_dxr_node *node = checked_malloc(sizeof(*node));
	node->type = DXR_ARR;
	node->u.arr.dxr = dxr;
	node->u.arr.length = length;
	node->id = dxr->id;
	return node;
}

struct ast_dxr_node *ast_new_dxr_fn(struct ast_dxr_node *dxr,
		struct linked_list *args)
{
	assert(args != 0);  assert(dxr != 0);
	struct ast_dxr_node *node = checked_malloc(sizeof(*node));
	node->type = DXR_FN;
	node->u.fn.dxr = dxr;
	node->u.fn.args = args;
	node->id = dxr->id;
	return node;
}

void ast_free_dxr(struct ast_dxr_node *node)
{
	if (node == 0)  return;
	switch (node->type) {
		case DXR_ID:
			free(node->u.id);
			break;
		case DXR_PTR:
			ast_free_dxr(node->u.ptr.dxr);
			break;
		case DXR_ARR:
			ast_free_expr(node->u.arr.length);
			ast_free_dxr(node->u.arr.dxr);
			break;
		case DXR_FN:
			while (ll_size(node->u.fn.args) > 0)
				ast_free_dxr(ll_shift_ptr(node->u.fn.args));
			ll_free(node->u.fn.args);
			ast_free_dxr(node->u.fn.dxr);
			break;
	}
	/* node->id is an alias: don't free it */
	free(node);
}

/* Statement nodes */

static struct ast_stmt_node *ast_new_stmt(void)
{
	struct ast_stmt_node *node = checked_malloc(sizeof(*node));
	node->fn = 0;
	node->symtab_context = 0;
	node->vars_read = stringset_new();
	node->vars_written = stringset_new();
	return node;
}

struct ast_stmt_node *ast_new_empty_stmt(void)
{
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = EMPTY_STMT;
	return node;
}

struct ast_stmt_node *ast_new_continue_stmt(void)
{
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = CONTINUE_STMT;
	return node;
}

struct ast_stmt_node *ast_new_break_stmt(void)
{
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = BREAK_STMT;
	return node;
}

struct ast_stmt_node *ast_new_return_stmt(void)
{
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = RETURN_STMT;
	return node;
}

struct ast_stmt_node *ast_new_goto_stmt(const char *label)
{
	assert(label != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = GOTO_STMT;
	node->u.label = copy_string(label);
	return node;
}

struct ast_stmt_node *ast_new_default_stmt(struct ast_stmt_node *body)
{
	assert(body != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = DEFAULT_STMT;
	node->u.body = body;
	return node;
}

struct ast_stmt_node *ast_new_expr_stmt(struct ast_expr_node *expr)
{
	assert(expr != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = EXPR_STMT;
	node->u.expr = expr;
	return node;
}

struct ast_stmt_node *ast_new_return_expr_stmt(struct ast_expr_node *expr)
{
	assert(expr != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = RETURN_EXPR_STMT;
	node->u.expr = expr;
	return node;
}

struct ast_stmt_node *ast_new_labeled_stmt(const char *label,
		struct ast_stmt_node *stmt)
{
	assert(label != 0);  assert(stmt != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = LABELED_STMT;
	node->u.lbl_stmt.label = copy_string(label);
	node->u.lbl_stmt.stmt = stmt;
	return node;
}

struct ast_stmt_node *ast_new_case_stmt(struct ast_expr_node *expr,
		struct ast_stmt_node *body)
{
	assert(expr != 0);  assert(body != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = CASE_STMT;
	node->u.cond_stmt.cond = expr;
	node->u.cond_stmt.body = body;
	return node;
}

struct ast_stmt_node *ast_new_if_stmt(struct ast_expr_node *cond,
		struct ast_stmt_node *body)
{
	assert(cond != 0);  assert(body != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = IF_STMT;
	node->u.cond_stmt.cond = cond;
	node->u.cond_stmt.body = body;
	return node;
}

struct ast_stmt_node *ast_new_switch_stmt(struct ast_expr_node *expr,
		struct ast_stmt_node *body)
{
	assert(expr != 0);  assert(body != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = SWITCH_STMT;
	node->u.cond_stmt.cond = expr;
	node->u.cond_stmt.body = body;
	return node;
}

struct ast_stmt_node *ast_new_while_stmt(struct ast_expr_node *cond,
		struct ast_stmt_node *body)
{
	assert(cond != 0);  assert(body != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = WHILE_STMT;
	node->u.cond_stmt.cond = cond;
	node->u.cond_stmt.body = body;
	return node;
}

struct ast_stmt_node *ast_new_do_while_stmt(struct ast_stmt_node *body,
		struct ast_expr_node *cond)
{
	assert(body != 0);  assert(cond != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = DO_WHILE_STMT;
	node->u.cond_stmt.cond = cond;
	node->u.cond_stmt.body = body;
	return node;
}

struct ast_stmt_node *ast_new_if_else_stmt(struct ast_expr_node *cond,
		struct ast_stmt_node *if_body, struct ast_stmt_node *else_body)
{
	assert(cond != 0);  assert(if_body != 0);  assert(else_body != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = IF_ELSE_STMT;
	node->u.if_else.cond = cond;
	node->u.if_else.if_body = if_body;
	node->u.if_else.else_body = else_body;
	return node;
}

struct ast_stmt_node *ast_new_compound_stmt(struct linked_list *decls,
		struct linked_list *stmts)
{
	assert(decls != 0);  assert(stmts != 0);
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = COMPOUND_STMT;
	node->u.cpd.decls = decls;
	node->u.cpd.stmts = stmts;
	return node;
}

struct ast_stmt_node *ast_new_for_stmt(struct ast_expr_node *init,
		struct ast_expr_node *cond, struct ast_expr_node *update,
		struct ast_stmt_node *body)
{
	assert(body != 0);  /* other arguments can be null */
	struct ast_stmt_node *node = ast_new_stmt();
	node->stmt_code = FOR_STMT;
	node->u.floop.init = init;
	node->u.floop.cond = cond;
	node->u.floop.update = update;
	node->u.floop.body = body;
	return node;
}

void ast_free_stmt(struct ast_stmt_node *node)
{
	if (node == 0)  return;  /* must allow this for FOR_STMT recursion */
	switch (node->stmt_code) {
		case EMPTY_STMT:
		case CONTINUE_STMT:
		case BREAK_STMT:
		case RETURN_STMT:
			/* nothing to be done */
			break;
		case GOTO_STMT:
			free(node->u.label);
			break;
		case DEFAULT_STMT:
			ast_free_stmt(node->u.body);
			break;
		case EXPR_STMT:
		case RETURN_EXPR_STMT:
			ast_free_expr(node->u.expr);
			break;
		case LABELED_STMT:
			free(node->u.lbl_stmt.label);
			ast_free_stmt(node->u.lbl_stmt.stmt);
			break;
		case CASE_STMT:
		case IF_STMT:
		case SWITCH_STMT:
		case WHILE_STMT:
		case DO_WHILE_STMT:
			ast_free_expr(node->u.cond_stmt.cond);
			ast_free_stmt(node->u.cond_stmt.body);
			break;
		case IF_ELSE_STMT:
			ast_free_expr(node->u.if_else.cond);
			ast_free_stmt(node->u.if_else.if_body);
			ast_free_stmt(node->u.if_else.else_body);
			break;
		case COMPOUND_STMT:
		{
			struct ll_iter *iter = ll_iterator(node->u.cpd.decls);
			while (ll_has_next(iter))
				ast_free_decl(ll_next_ptr(iter));
			ll_iterator_free(iter);
			iter = ll_iterator(node->u.cpd.stmts);
			while (ll_has_next(iter))
				ast_free_stmt(ll_next_ptr(iter));
			ll_iterator_free(iter);
			break;
		}
		case FOR_STMT:
			ast_free_expr(node->u.floop.init);
			ast_free_expr(node->u.floop.cond);
			ast_free_expr(node->u.floop.update);
			ast_free_stmt(node->u.floop.body);
			break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Forgot a case in ast_free_stmt_node\n");
			exit(EXIT_FAILURE);
	}
	if (node->vars_read != 0)     stringset_free(node->vars_read);
	if (node->vars_written != 0)  stringset_free(node->vars_written);
	free(node);
}

/* Expression nodes */

static struct ast_expr_node *ast_new_expr(void)
{
	struct ast_expr_node *node = checked_malloc(sizeof(*node));
	node->vars_read = stringset_new();
	node->vars_written = stringset_new();
	return node;
}

struct ast_expr_node *ast_new_integer_constant(unsigned long n)
{
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = INTEGER_CONSTANT;
	node->u.n = n;
	return node;
}

struct ast_expr_node *ast_new_floating_constant(double d)
{
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = FLOATING_CONSTANT;
	node->u.d = d;
	return node;
}

struct ast_expr_node *ast_new_ident(const char *id)
{
	assert(id != 0);
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = IDENTIFIER;
	node->u.id = copy_string(id);
	return node;
}

struct ast_expr_node *ast_new_unary_op(enum ast_unary_op op,
		struct ast_expr_node *arg)
{
	assert(arg != 0);
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = UNARY_OP;
	node->u.unary.op = op;
	node->u.unary.arg = arg;
	return node;
}

struct ast_expr_node *ast_new_inc_dec(enum ast_inc_dec_op op,
		const char *id)
{
	assert(id != 0);
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = INC_DEC;
	node->u.inc_dec.op = op;
	node->u.inc_dec.id = copy_string(id);
	return node;
}

struct ast_expr_node *ast_new_binary_op(enum ast_binary_op op,
		struct ast_expr_node *left, struct ast_expr_node *right)
{
	assert(left != 0);  assert(right != 0);
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = BINARY_OP;
	node->u.binary.op = op;
	node->u.binary.left = left;
	node->u.binary.right = right;
	return node;
}

struct ast_expr_node *ast_new_assign(enum ast_assign_op op, const char *id,
		struct ast_expr_node *value)
{
	assert(id != 0);  assert(value != 0);
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = ASSIGNMENT;
	node->u.assign.op = op;
	node->u.assign.id = copy_string(id);
	node->u.assign.value = value;
	return node;
}

struct ast_expr_node *ast_new_fn_call(const char *id, struct linked_list *args)
{
	assert(id != 0);  assert(args != 0);
	struct ast_expr_node *node = ast_new_expr();
	node->expr_code = FUNCTION_CALL;
	node->u.fn_call.id = copy_string(id);
	node->u.fn_call.args = args;
	return node;
}

void ast_free_expr(struct ast_expr_node *n)
{
	if (n == 0)  return;
	switch (n->expr_code) {
		case INTEGER_CONSTANT:
		case FLOATING_CONSTANT:
			/* nothing to do */
			break;
		case IDENTIFIER:
			free(n->u.id);
			break;
		case UNARY_OP:
			ast_free_expr(n->u.unary.arg);
			break;
		case INC_DEC:
			free(n->u.inc_dec.id);
			break;
		case BINARY_OP:
			ast_free_expr(n->u.binary.left);
			ast_free_expr(n->u.binary.right);
			break;
		case ASSIGNMENT:
			free(n->u.assign.id);
			ast_free_expr(n->u.assign.value);
			break;
		case FUNCTION_CALL:
			free(n->u.fn_call.id);
			assert(n->u.fn_call.args != 0);
			while (ll_size(n->u.fn_call.args) > 0)
				ast_free_expr(ll_shift_ptr(n->u.fn_call.args));
			ll_free(n->u.fn_call.args);
			break;
		default:  /* this shouldn't happen */
			fprintf(stderr, "Missed a case in ast_free_expr_node\n");
			exit(EXIT_FAILURE);
	}
	if (n->vars_read != 0)     stringset_free(n->vars_read);
	if (n->vars_written != 0)  stringset_free(n->vars_written);
	free(n);
}
