#ifndef IRSCC_AST_H
#define IRSCC_AST_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stdio.h>
#include "llist.h"
#include "stringset.h"

/* Storage-class specifiers, type specifiers, and type qualifiers */

typedef unsigned short spec_t;

#define SPEC_AUTO      ((spec_t)(1 <<  0))
#define SPEC_REGISTER  ((spec_t)(1 <<  1))
#define SPEC_STATIC    ((spec_t)(1 <<  2))
#define SPEC_EXTERN    ((spec_t)(1 <<  3))
#define SPEC_TYPEDEF   ((spec_t)(1 <<  4))
#define SPEC_VOID      ((spec_t)(1 <<  5))
#define SPEC_CHAR      ((spec_t)(1 <<  6))
#define SPEC_SHORT     ((spec_t)(1 <<  7))
#define SPEC_INT       ((spec_t)(1 <<  8))
#define SPEC_LONG      ((spec_t)(1 <<  9))
#define SPEC_FLOAT     ((spec_t)(1 << 10))
#define SPEC_DOUBLE    ((spec_t)(1 << 11))
#define SPEC_SIGNED    ((spec_t)(1 << 12))
#define SPEC_UNSIGNED  ((spec_t)(1 << 13))
#define SPEC_CONST     ((spec_t)(1 << 14))
#define SPEC_VOLATILE  ((spec_t)(1 << 15))

#define SPEC_MASK_STORAGE    ((spec_t)(  \
	SPEC_AUTO | SPEC_REGISTER | SPEC_STATIC | SPEC_EXTERN | SPEC_TYPEDEF  \
))
#define SPEC_MASK_TYPE       ((spec_t)(  \
	SPEC_VOID | SPEC_CHAR | SPEC_SHORT | SPEC_INT | SPEC_LONG | SPEC_FLOAT |  \
	SPEC_DOUBLE | SPEC_SIGNED | SPEC_UNSIGNED  \
))
#define SPEC_MASK_TYPE_QUAL  ((spec_t)(  \
	SPEC_CONST | SPEC_VOLATILE  \
))

int ast_spec_is_integer(spec_t specifiers);
int ast_spec_is_floating(spec_t specifiers);

/* Types */

enum ast_metatype { TYPE_BASIC, TYPE_FN };

enum ast_basic_type { TYPE_INTEGER, TYPE_FLOATING, TYPE_VOID };

struct ast_type {
	enum ast_metatype meta;
	union {
		enum ast_basic_type basic;
		struct {
			struct ast_type *ret;
			struct linked_list *args;
		} fn;
	} u;
};

const struct ast_type *ast_integer_type(void);
const struct ast_type *ast_floating_type(void);
const struct ast_type *ast_void_type(void);
void ast_free_type(struct ast_type *type);
int ast_type_is_integer(const struct ast_type *type);
int ast_type_is_floating(const struct ast_type *type);
int ast_type_is_void(const struct ast_type *type);
int ast_type_equiv(const struct ast_type *a, const struct ast_type *b);
void ast_print_type(FILE *stream, const struct ast_type *type);

/* Function definition nodes */

struct ast_fn_node {
	spec_t specifiers;
	struct ast_dxr_node *dxr;
	struct ast_stmt_node *body;
	struct symtab_node *symtab_context;  /* this pointer is an alias */
	int have_returned;  /* TODO: this is a temporary kludge */
};

struct ast_fn_node *ast_new_fn(spec_t specifiers, struct ast_dxr_node *dxr,
		struct ast_stmt_node *body);
void ast_free_fn(struct ast_fn_node *node);

/* Declaration nodes */

struct ast_decl_node {  /* declaration */
	spec_t specifiers;
	struct linked_list *idxrs;
	struct stringset *vars_read;
	struct stringset *vars_written;
};

struct ast_decl_node *ast_new_decl(spec_t specifiers,
		struct linked_list *idxrs);
void ast_free_decl(struct ast_decl_node *node);

struct ast_idxr_node {  /* init-declarator */
	struct ast_dxr_node *dxr;
	struct ast_expr_node *init;
	struct stringset *vars_read;
	struct stringset *vars_written;
};

struct ast_idxr_node *ast_new_idxr(struct ast_dxr_node *dxr,
		struct ast_expr_node *init);
void ast_free_idxr(struct ast_idxr_node *node);

enum ast_dxr_type { DXR_ID, DXR_PTR, DXR_ARR, DXR_FN };

struct ast_dxr_node {  /* declarator */
	enum ast_dxr_type type;
	union {
		char *id;
		struct {
			spec_t quals;
			struct ast_dxr_node *dxr;
		} ptr;
		struct {
			struct ast_dxr_node *dxr;
			struct ast_expr_node *length;
		} arr;
		struct {
			struct ast_dxr_node *dxr;
			struct linked_list *args;  /* elts: ptrs to struct ast_decl_node */
		} fn;
	} u;
	const char *id;  /* this pointer is an alias */
};

struct ast_dxr_node *ast_new_dxr_id(const char *id);
struct ast_dxr_node *ast_new_dxr_ptr(spec_t quals, struct ast_dxr_node *dxr);
struct ast_dxr_node *ast_new_dxr_arr(struct ast_dxr_node *dxr,
		struct ast_expr_node *length);
struct ast_dxr_node *ast_new_dxr_fn(struct ast_dxr_node *dxr,
		struct linked_list *args);
void ast_free_dxr(struct ast_dxr_node *node);

/* Statement nodes */

enum ast_stmt_code {
	EMPTY_STMT, CONTINUE_STMT, BREAK_STMT, RETURN_STMT, GOTO_STMT,
	DEFAULT_STMT, EXPR_STMT, RETURN_EXPR_STMT, LABELED_STMT, CASE_STMT,
	IF_STMT, SWITCH_STMT, WHILE_STMT, DO_WHILE_STMT, IF_ELSE_STMT,
	COMPOUND_STMT, FOR_STMT
};

struct ast_stmt_node {
	enum ast_stmt_code stmt_code;
	union {
		/* EMPTY_STMT, CONTINUE_STMT, BREAK_STMT, RETURN_STMT: nothing */
		char *label;                     /* GOTO_STMT */
		struct ast_stmt_node *body;      /* DEFAULT_STMT */
		struct ast_expr_node *expr;      /* EXPR_STMT, RETURN_EXPR_STMT */
		struct {
			char *label;
			struct ast_stmt_node *stmt;
		} lbl_stmt;                      /* LABELED_STMT */
		struct {
			struct ast_expr_node *cond;
			struct ast_stmt_node *body;  /* CASE_STMT, IF_STMT, SWITCH_STMT, */
		} cond_stmt;                     /* WHILE_STMT, DO_WHILE_STMT        */
		struct {
			struct ast_expr_node *cond;
			struct ast_stmt_node *if_body, *else_body;
		} if_else;                       /* IF_ELSE_STMT */
		struct {
			struct linked_list *decls, *stmts;
		} cpd;                           /* COMPOUND_STMT */
		struct {
			struct ast_expr_node *init, *cond, *update;
			struct ast_stmt_node *body;
		} floop;                         /* FOR_STMT */
	} u;
	struct ast_fn_node *fn;              /* this pointer is an alias */
	struct symtab_node *symtab_context;  /* this pointer is an alias */
	struct stringset *vars_read;
	struct stringset *vars_written;
};

struct ast_stmt_node *ast_new_empty_stmt(void);
struct ast_stmt_node *ast_new_continue_stmt(void);
struct ast_stmt_node *ast_new_break_stmt(void);
struct ast_stmt_node *ast_new_return_stmt(void);
struct ast_stmt_node *ast_new_goto_stmt(const char *label);
struct ast_stmt_node *ast_new_default_stmt(struct ast_stmt_node *body);
struct ast_stmt_node *ast_new_expr_stmt(struct ast_expr_node *expr);
struct ast_stmt_node *ast_new_return_expr_stmt(struct ast_expr_node *expr);
struct ast_stmt_node *ast_new_labeled_stmt(const char *label,
		struct ast_stmt_node *stmt);
struct ast_stmt_node *ast_new_case_stmt(struct ast_expr_node *expr,
		struct ast_stmt_node *body);
struct ast_stmt_node *ast_new_if_stmt(struct ast_expr_node *cond,
		struct ast_stmt_node *body);
struct ast_stmt_node *ast_new_switch_stmt(struct ast_expr_node *expr,
		struct ast_stmt_node *body);
struct ast_stmt_node *ast_new_while_stmt(struct ast_expr_node *cond,
		struct ast_stmt_node *body);
struct ast_stmt_node *ast_new_do_while_stmt(struct ast_stmt_node *body,
		struct ast_expr_node *cond);
struct ast_stmt_node *ast_new_if_else_stmt(struct ast_expr_node *cond,
		struct ast_stmt_node *if_body, struct ast_stmt_node *else_body);
struct ast_stmt_node *ast_new_compound_stmt(struct linked_list *decls,
		struct linked_list *stmts);
struct ast_stmt_node *ast_new_for_stmt(struct ast_expr_node *init,
		struct ast_expr_node *cond, struct ast_expr_node *update,
		struct ast_stmt_node *body);
void ast_free_stmt(struct ast_stmt_node *node);

/* Expression nodes */

enum ast_expr_code {
	INTEGER_CONSTANT, FLOATING_CONSTANT, IDENTIFIER, UNARY_OP, INC_DEC,
	BINARY_OP, ASSIGNMENT, FUNCTION_CALL
};

enum ast_unary_op {
	ADDR_OF, USTAR, UPLUS, UMINUS, BIT_NOT, LOGIC_NOT, SIZEOF
};

enum ast_inc_dec_op {
	POST_INC, POST_DEC, PRE_INC, PRE_DEC
};

enum ast_binary_op {
	BSTAR, SLASH, MODULO, BPLUS, BMINUS, LSHIFT, RSHIFT, LESS, LESS_EQUALS,
	GREATER, GREATER_EQUALS, EQUALS_EQUALS, NOT_EQUALS, BIT_AND, BIT_XOR,
	BIT_OR, LOGIC_AND, LOGIC_OR, COMMA
};

enum ast_assign_op {
	ASSIGN, PLUS_ASSIGN, MINUS_ASSIGN, STAR_ASSIGN, SLASH_ASSIGN,
	MODULO_ASSIGN, AND_ASSIGN, XOR_ASSIGN, OR_ASSIGN, LSHIFT_ASSIGN,
	RSHIFT_ASSIGN
};

struct ast_expr_node {
	enum ast_expr_code expr_code;
	const struct ast_type *type;  /* this pointer is an alias */
	union {
		unsigned long n;  /* INTEGER_CONSTANT */
		double d;         /* FLOATING_CONSTANT */
		char *id;         /* IDENTIFIER */
		struct {
			enum ast_unary_op op;
			struct ast_expr_node *arg;
		} unary;          /* UNARY_OP */
		struct {
			enum ast_inc_dec_op op;
			char *id;
		} inc_dec;        /* INC_DEC */
		struct {
			enum ast_binary_op op;
			struct ast_expr_node *left, *right;
		} binary;         /* BINARY_OP */
		struct {
			enum ast_assign_op op;
			char *id;
			struct ast_expr_node *value;
		} assign;         /* ASSIGNMENT */
		struct {
			char *id;
			struct linked_list *args;
		} fn_call;        /* FUNCTION_CALL */
	} u;
	struct symtab_node *symtab_context;  /* this pointer is an alias */
	struct stringset *vars_read;
	struct stringset *vars_written;
};

struct ast_expr_node *ast_new_integer_constant(unsigned long n);
struct ast_expr_node *ast_new_floating_constant(double d);
struct ast_expr_node *ast_new_ident(const char *id);
struct ast_expr_node *ast_new_unary_op(enum ast_unary_op op,
		struct ast_expr_node *arg);
struct ast_expr_node *ast_new_inc_dec(enum ast_inc_dec_op op,
		const char *id);
struct ast_expr_node *ast_new_binary_op(enum ast_binary_op op,
		struct ast_expr_node *left, struct ast_expr_node *right);
struct ast_expr_node *ast_new_assign(enum ast_assign_op op, const char *id,
		struct ast_expr_node *value);
struct ast_expr_node *ast_new_fn_call(const char *id, struct linked_list *args);
void ast_free_expr(struct ast_expr_node *n);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_AST_H */
