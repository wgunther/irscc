#include "controller.hh"
const string Controller::defaultOutTex = "a.tex";
const string Controller::defaultOutPDF = "a";
void displayUsage () {
	using std::endl;
	std::cout << "Usage: irscc [switches] [--] file" << endl
							<< "Options:" << endl
							<< "  -c         \t\tCompile, do not pipe to pdfTeX (output is TeX file)" << endl
							<< "  -v         \t\tVerbose output" << endl
							<< "  -h         \t\tYou already found it" << endl
							<< "  -o <file>\t\tPlace the output into <file>" << endl;
}


Controller::Controller (int c, char **a) : f{} { 
	bool scanO {false};
	bool noFlags {false};
	for (int i = 1; i < c; ++i) {
		string arg{a[i]};
		//Flag
		if (arg[0] == '-' && noFlags==false) {
			int j = 1;
			if (arg.size() == 2 && arg[1] == '-') {
				noFlags=true;
				continue;
			}
			while (arg[j] != '\0') {
				char called = arg[j];
				switch (called) {
					case 'c': {
						f.c = 1;
						break;
					}
					case 'v': {
						f.v = 1;
						break;
					}
					case 'h': {
							displayUsage();
							exit(0);
							break;
					}
					case 'o' : {
						scanO = true;
						break;
					}
					default : {
						std::cerr<<"Warning: unrecognized command line option: "
							<<called<<std::endl;
						displayUsage();
					}
				}
				j++;
			}
		}
		else {
			// Not a flag.
			if (scanO == true) {
				f.out = arg;
				f.defOut = 1;
				scanO = false;
				continue;
			}
			f.in = arg;
			f.defIn = 1;
		}
	}
}

