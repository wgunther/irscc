#include "formElements.hh"

// Utility Functions:
void simpleCentExpRow(ostream& out,const string& rowNum, const string& Desc, bool ruled) {
	out << R"(\simpleCentExpRow[\rowWidth] RowNum={)"
		<< rowNum << "} Desc={" << Desc
		<< R"(} Color={\primaryColor} Ruled={)"
		<< std::to_string(ruled) << R"(}\par)";
}
void simpleNoRightNumberRow(ostream& out,const string& rowNum, const string& Desc, bool ruled) {
	out << R"(\simpleNoRightNumberRow[\rowWidth] RowNum={)"
		<< rowNum << "} Desc={" << Desc
		<< R"(} Color={\primaryColor} Ruled={)" << std::to_string(ruled)
		<< R"(}\par)";
}


// FormDesc Implementations:
map<const form_desc*,string> FormDesc::formNameMap;
map<const form_desc*,string> FormDesc::scheduleNameMap;
vector<string> FormDesc::availableFormNames{8999};
vector<string> FormDesc::availableScheduleNames{702};
void FormDesc::initalizeAvailableIdentifiers() {
	// Initializes availableFormNames and availableScheduleNames
	// using 1000-9999 (-1040) and AA-ZZ+A-Z respectively
	// using Fischer-Yates
	int j = 0;
	for (int i = 0; i < 8998; i++) {
		if (i == 1040) continue;
		availableFormNames[j++] = std::to_string(i);
	}
	shuffle(availableFormNames);
	j=0;
	for (char firstLetter = 'A'; firstLetter <= 'Z'; firstLetter++) {
		string x{firstLetter};
		availableScheduleNames[j++] = x;
		for (char secondLetter = 'A'; secondLetter <= 'Z'; secondLetter++) {
			string y{secondLetter};
			availableScheduleNames[j++] = x+y;
		}
	}
	shuffle(availableScheduleNames);
}
string FormDesc::lookupIdentifier(const form_desc* d) {
	if (FormDesc::isForm(d)) {
		if (formNameMap.count(d)) {
			// look up cached name
			return formNameMap[d];
		} else {
			if (string{d->name} == "main") {
				// Special case: give main name 1040
				formNameMap[d] = "1040";
				return formNameMap[d];
			}
			// generate a fresh name
			auto formName = availableFormNames.back();
			availableFormNames.pop_back();
			formNameMap[d] = formName;
			return formName;
		}
	} else {
		if (scheduleNameMap.count(d)) {
			// look up cached name
			return scheduleNameMap[d];
		} else {
			// generate a fresh name
			auto scheduleName = availableScheduleNames.back();
			availableScheduleNames.pop_back();
			scheduleNameMap[d] = scheduleName;
			return scheduleName;
		}
	}
}
bool FormDesc::isForm(const form_desc* d) {
	// parent is null => not a schedule.
	return d->parent == NULL;
}
bool FormDesc::isSchedule(const form_desc *d) {
	// parent => schedule
	return d->parent != NULL;
}
bool FormDesc::isForm() {
	// method version of above
	return FormDesc::isForm(*this);
}
bool FormDesc::isSchedule() {
	// method version of above
	return FormDesc::isSchedule(*this);
}
string FormDesc::formOrSchedule () {
	return this->isForm() ? "Form" : "Schedule";
}
string FormDesc::identifier() {
	// method version of lookupIdentifier
	return FormDesc::lookupIdentifier(this->formDesc);
}
ostream& operator<< (ostream& out, FormDesc d) {
	// "Form 1040" or "Schedule~A" outputted to stream
	if (d.isForm())
		out << "Form ";
	else 
		out << "Schedule~";
	out << d.identifier();
	return out;
}
FormDesc::operator string() {
	// "Form 1040" or "Schedule~A" returned
	string s;
	s.reserve(13);
	if (this->isForm())
		s.append("Form ");
	else
		s.append("Schedule~");	
	s.append(this->identifier());
	return s;
}

// LineNumber Implementations:
string LineNumber::simpleNumber () {
	// "6a" returned
	ostringstream s;
	s << (*this)->line;
	if ((*this)->subline > 0)
		 s << (*this)->subline;
	return s.str();
}
string LineNumber::longNumber () {
	// "Form 1040, line 6a" returned
	auto formDesc = FormDesc{(*this)->form};
	ostringstream s;
	s << formDesc << ", " << this->shortNumber();
	return s.str();
}
string LineNumber::shortNumber () {
	// "line 6a" returned
	auto formDesc = FormDesc{(*this)->form};
	ostringstream s;
	s << "line~" << this->simpleNumber();
	return s.str();
}
ostream& operator<< (ostream& out, LineNumber l) {
	// "line 6a" to ourstream
	out << l.shortNumber();
	return out;
}
// LineDescription Implementations:
map<int,string> LineDescription::labelMap{};
string LineDescription::lookupLabel(int i) {
	// map for label and qtyids
	if (labelMap.count(i))
		// retrieved cached
		return labelMap[i];
	else
		// make new label
		return (labelMap[i] = makeRandomLabel());
}
string LineDescription::getLineNumber() {
	// "line 6" returned if from same form.
	// "Form 6823, line 8b" if different form.
	auto line =  LineNumber{(*this)->lineno};
	if (line->form == (const form_desc*)this->formOn) {
		return line.shortNumber();
	} else {
		return line.longNumber();
	}
}
string LineDescription::getSimpleLineNumber() {
	// "6b" returned
	auto line =  LineNumber{(*this)->lineno};
	return line.simpleNumber();
}

bool LineDescription::isLabeled() {
	// check to see if there's a qtyid (i.e. a label) 
	return (*this)->qtyid != 0;
}
string LineDescription::getLabel() {
	// method version of lookupLabel
	return LineDescription::lookupLabel((*this)->qtyid);
}
bool LineDescription::isEnterAlso() {
	// check to is if there is an enter also
	// linenumber 0 means no linenumber.
	return (*this)->enter_also.lineno.line != 0; 
}
bool LineDescription::isEnterAlsoRecursive() {
	// checks to see if enter also line number is
	// on the same form. 
	return(this->isEnterAlso() && this->enterAlsoLineNumber().getForm()==this->getForm());
}
bool LineDescription::isReturnRecursive() {
	//XXX: very dependent on stuff I probably shouldn't be looking at.
	// For an OP_COPY_FROM_ATTACHED, checks to see if the form
	// you are copying from is the same as the form you are on. 
	return (*this)->op == OP_COPY_FROM_ATTACHED && this->formOn == ll_head_lineno((*this)->args).form;
}
bool LineDescription::isGotoAfter() {
	// checks to see if there is a goto_after
	// line number 0 means there isn't.
	return (*this)->goto_after.lineno.line != 0;
}
LineNumber LineDescription::enterAlsoLineNumber() {
	// get access to enter also line number.
	return LineNumber{(*this)->enter_also.lineno};
}
LineNumber LineDescription::gotoAfterLineNumber() {
	// get access to enter also gotoafters
	return LineNumber{(*this)->goto_after.lineno};
}


// FormElement Implementations:
// FormElement Base Class Implementations
ostream& operator<<(ostream& out,FormElement& e) {
	// just calls print function on the element. NBD
	return e.print(out);
}

// FormRow Implementations:
void FormRow::appendMessage(const string& s) {
	this->message.append(s);
}
void FormRow::setMessage(const string& s) {
	this->message = s;
}
string FormRow::getLabel () {
	// FormRows have associated line descriptions, which may be labeled
	// this just retrieves thatlabel. 
	return this->lineDesc.getLabel();
}
ostream& FormRow::print(ostream& out) {
	// Checks to see if there is label: if so, it adds it to message
	// Otherwise, it just prints the message in a row. 
	if (this->lineDesc.isLabeled()) {
		string formattedLabel{};
		string rawLabel{};
		formattedLabel.reserve(30);
		rawLabel.reserve(30);
		formattedLabel.append(R"({\helvetEightBold )");
		if (this->lineDesc.isEnterAlsoRecursive())
			rawLabel.append("revised ");
		if (this->lineDesc.isReturnRecursive())
			rawLabel.append("preliminary ");
		rawLabel.append(this->getLabel()); 
		formattedLabel.append(capitalizeFirst(std::move(rawLabel)));
		if (this->message.size() == 0)
			formattedLabel.append("} ");
		else 
			formattedLabel.append(".} ");
		this->message = (std::move(formattedLabel) + this->message);
	}
	this->message.append(R"(\DotFill)");
	simpleCentExpRow(out,this->lineDesc.getSimpleLineNumber(),this->message,true);
	return out;
}

// ExpressionRow Implementations:
ostream& ExpressionRow::print(ostream& out) {
	// Checks to see if there is an enterAlso: if so, it adds it to message.
	// Otherwise, it just prints the message in a row.
	if (this->lineDesc.isEnterAlso()) {
		// handles when the line is an `enterAlso'
		switch (this->lineDesc->op) {
			// special cases wording.
			//XXX: this depends on the wording from main.cc
			//of a few of these things!
			case OP_COPY:
			case OP_DOLLARS_CONSTANT:
			case OP_COPY_FROM_ATTACHED:
			case OP_CENTS_CONSTANT:
				this->appendMessage(" here and on ");
				break;
			default:	
				this->appendMessage(". Enter here and on ");
		}
		if (this->lineDesc.isEnterAlsoRecursive())
			// for recursive things, add the `supplemental' phrasing
			this->appendMessage("the supplemental copy of ");
		this->appendMessage(this->lineDesc.enterAlsoLineNumber().longNumber());
	}
	if (this->lineDesc.isGotoAfter()) {
		// handles when the line is a `gotoAfter'
		string s;
		s.reserve(10);
		s.append(". Then go to ");
		s.append(this->lineDesc.gotoAfterLineNumber().shortNumber());
		this->appendMessage(std::move(s));	
	}
	return super::print(out);
}

// FunctionArgsToggle Implementations:
ostream& FunctionArgsToggle::print(ostream& out) {
	// Indents or doesn't. Ya know. Depending on what you want.
	if (indent)
		out << R"(\shouldIndent=1\relax )";
	else
		out << R"(\shouldIndent=0\relax )";
	return out;
}
// DisplayRow implementations
void DisplayRow::appendMessage(const string& s) {
	this->message.append(s);
}
void DisplayRow::setMessage(const string& s) {
	this->message = s;
}
ostream& DisplayRow::print(ostream& out) {
	simpleNoRightNumberRow(out,this->lineDesc.getSimpleLineNumber(),this->message,true);
	return out;	
}

// FormSection Implementations:
// FormSection Base Class Implementations:
void FormSection::addElement (FormElement *e) {
	this->elements.push_back(unique_ptr<FormElement>{e});
}
ostream& FormSection::print (ostream& out) {
	out << this->printHeader << endl;
	unsigned long i = 0;
	for (auto& element : elements) {
		out << *element;
		if (i > 4 && elements.size()-4 > i)
			// This is so page breaks aren't in the middle probably
			out << R"(\filbreak)";
		out << endl;	
		i++;
	}
	out << printFooter << endl;
	return out;
}
ostream& operator<< (ostream& out, FormSection& f) {
	return f.print(out);
}

// GutterSection Implementations:
ostream& GutterSection::print (ostream& out) {
	this->printHeader.append(R"(\hrule\gutterSection[.9in] MarginTitle={)");
	this->printHeader.append(this->header);
	this->printHeader.append(R"(} FlavorText={)");
	this->printHeader.append(this->flavor);
	this->printHeader.append("}");
	this->printFooter.append(R"(\endsection)");
	return super::print(out);
}

// NormalSection Implementations:
ostream& NormalSection::print (ostream& out) {
	this->printHeader.append(R"(\hrule\partSection PartNo={)");
	this->printHeader.append(std::to_string(this->partNo));
	this->printHeader.append(R"(} PartLabel={)");
	this->printHeader.append(this->header);
	this->printHeader.append("}");
	this->printFooter.append(R"(\endsection)");
	return super::print(out);
}


// Form Implementations:
// Form Base Class Implementations:
void Form::addSec(FormSection* s) {
	this->sections.push_back(unique_ptr<FormSection>{s});
}
ostream& Form::print (ostream& out) {
	out << printHeader << endl;
	int part = 1; // Keeps track of part numbers.
	for (auto& sec : sections) {
		sec->partNo = part++;
		out << *sec;
		if (part != 1) 
			out << R"(\vskip -.4pt)"; // for \hrules stuff
		// page breaks at end of sectins are good
		out << R"(\filbreak)" << endl; 
	}
	out << printFooter << endl;
	return out;
}
ostream& operator<< (ostream& out, Form& f) {
	return f.print(out);
}

// Form1040 Implementations:
FormSection* Form1040::getSec() {
	return new GutterSection{};
}
ostream& Form1040::print (ostream& out) {
	printHeader.append(R"(\newTenFortyForm{\blue})");
	printFooter.append(R"(\signHere{\blue})");
	return super::print(out);
}

// FormNormal Implementations
FormSection* FormNormal::getSec() {
	return new NormalSection{};
}
ostream& FormNormal::print (ostream& out) {
	this->printHeader.append(R"(\newFormForm Num={)");
	this->printHeader.append(this->F.identifier());
	this->printHeader.append("} CatNo={");
	this->printHeader.append(std::to_string(this->catNo));
	this->printHeader.append("} Title={"); 
	this->printHeader.append(this->title);
	this->printHeader.append(R"(}\relax)");
	return super::print(out);
}

// ScheduleNormal Implementations
FormSection* ScheduleNormal::getSec() {
	return new NormalSection{};
}
ostream& ScheduleNormal::print (ostream& out) {
	this->printHeader.append(R"(\newSchedule Num={)");
	this->printHeader.append(this->F.identifier());
	this->printHeader.append("} CatNo={");
	this->printHeader.append(std::to_string(this->catNo));;
	this->printHeader.append("} Title={");
	this->printHeader.append(this->title);
	this->printHeader.append("} Parent={");
	this->printHeader.append((string)FormDesc{this->F->parent});
	this->printHeader.append(R"(}\relax)");
	return super::print(out);
}
