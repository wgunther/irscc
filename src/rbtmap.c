/*	Red-black tree.  Brian Kell, January/June 2013, March 2014.

	Each node has a color, red or black. "External nodes" (conceptually,
	sentinel nodes representing empty subtrees) are black. An edge to a black
	node is called a black edge. The black-length of a path is the number of
	black edges on that path. A path from a specified node to an external node
	is called an external path for the specified node. A red-black tree
	satisfies the following conditions:
	1.	The root is black.
	2.	No red node has a red child.
	3.	The black-length of all external paths from a given node u is the same;
		this value is called the black-height of u.
	
	In drawings of trees below, Black nodes, including those that are possibly
	null, are in [Brackets], and Red nodes are in (Round parentheses). Nodes of
	uncertain color are in {braces}. Red nodes are generally drawn on the same
	horizontal level as their parents, so that black-height is shown
	graphically.

	References:
	*	Baase and Van Gelder, Computer Algorithms, third edition, section 6.4.
	*	Cormen, Leiserson, Rivest, and Stein, Introduction to Algorithms, third
		edition, chapter 13.
*/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "rbtmap.h"

#define RED    0
#define BLACK  1

struct rbtmap_node {
	void *key;
	void *value;
	int color;
	struct rbtmap_node *left, *right, *parent;
};

struct rbtmap {
	int size;
	struct rbtmap_node *root;
	int (*compare)(const void *, const void *);
};

struct rbtmap_iter {
	struct rbtmap *rbtmap;
	struct rbtmap_node *next_node;
};

/* Private functions */
static void rbtmap_node_free(struct rbtmap_node *n,
		void (*cleanup)(void *, void *));
static struct rbtmap_node *rbtmap_new_node(void *key, void *value, int color,
		struct rbtmap_node *parent);
static void rbtmap_insert_fixup(struct rbtmap *rbtmap, struct rbtmap_node *n);
static void rbtmap_left_rotate(struct rbtmap *rbtmap, struct rbtmap_node *n);
static void rbtmap_right_rotate(struct rbtmap *rbtmap, struct rbtmap_node *n);
static void rbtmap_node_delete(struct rbtmap *rbtmap, struct rbtmap_node *n,
		void (*cleanup)(void *, void *));
static void rbtmap_transplant(struct rbtmap *rbtmap, struct rbtmap_node *old,
		struct rbtmap_node *new);
static struct rbtmap_node *rbtmap_minimum(struct rbtmap *rbtmap,
		struct rbtmap_node *n);
static struct rbtmap_node *rbtmap_successor(struct rbtmap *rbtmap,
		struct rbtmap_node *n);
static void rbtmap_delete_fixup(struct rbtmap *rbtmap, struct rbtmap_node *n,
		int left_fixup);

struct rbtmap *rbtmap_new(int (*compare)(const void *, const void *))
{
	struct rbtmap *rbtmap;
	rbtmap = malloc(sizeof(*rbtmap));
	if (rbtmap == 0) {
		fprintf(stderr, "Out of memory\n");
		exit(EXIT_FAILURE);
	}
	rbtmap->size = 0;
	rbtmap->root = 0;
	rbtmap->compare = compare;
	return rbtmap;
}

void rbtmap_free(struct rbtmap *rbtmap, void (*cleanup)(void *, void *))
{
	if (rbtmap != 0) {
		rbtmap_node_free(rbtmap->root, cleanup);
		free(rbtmap);
	}
}

static void rbtmap_node_free(struct rbtmap_node *n,
		void (*cleanup)(void *, void *))
{
	if (n != 0) {
		if (cleanup != 0)  cleanup(n->key, n->value);
		rbtmap_node_free(n->left, cleanup);
		rbtmap_node_free(n->right, cleanup);
		free(n);
	}
}

void rbtmap_put(struct rbtmap *rbtmap, void *key, void *value,
		void (*cleanup)(void *, void *))
{
	struct rbtmap_node *node, *p;
	int cmp;
	if (rbtmap == 0 || key == 0)  return;
	p = rbtmap->root;
	if (p == 0) {  /* rbtmap is empty */
		node = rbtmap_new_node(key, value, BLACK, 0);
		rbtmap->root = node;
		rbtmap->size = 1;
	} else {
		for (;;) {  /* search for insertion location */
			cmp = rbtmap->compare(key, p->key);
			if (cmp < 0) {            /* key < p->key */
				if (p->left == 0) {
					node = rbtmap_new_node(key, value, RED, p);
					p->left = node;
					rbtmap_insert_fixup(rbtmap, node);
					++rbtmap->size;
					break;
				} else
					p = p->left;
			} else if (cmp > 0) {     /* key > p->key */
				if (p->right == 0) {
					node = rbtmap_new_node(key, value, RED, p);
					p->right = node;
					rbtmap_insert_fixup(rbtmap, node);
					++rbtmap->size;
					break;
				} else
					p = p->right;
			} else {                  /* key == p->key */
				if (cleanup != 0)  cleanup(p->key, p->value);
				p->key = key;
				p->value = value;
				break;
			}
		}
	}
}

static struct rbtmap_node *rbtmap_new_node(void *key, void *value, int color,
		struct rbtmap_node *parent)
{
	struct rbtmap_node *node = malloc(sizeof(*node));
	if (node == 0) { fprintf(stderr, "Out of memory\n");  exit(EXIT_FAILURE); }
	node->key = key;
	node->value = value;
	node->color = color;
	node->left = node->right = 0;
	node->parent = parent;
	return node;
}

/* Reestablishes the condition that no red node has a red child. The node n is
   red and may have a red parent; if so we need to fix it. */
static void rbtmap_insert_fixup(struct rbtmap *rbtmap, struct rbtmap_node *n)
{
	struct rbtmap_node *p, *g, *u;  /* parent, grandparent, uncle */
	assert(rbtmap != 0);  assert(rbtmap->root != 0);  assert(n != 0);
	assert(n->parent != 0);  assert(n->color == RED);
	FIXUP_NODE:
	p = n->parent;
	if (p->color == BLACK)  return;  /* done */
	g = p->parent;  assert(g != 0);  /* because p is red so it can't be root */
	if (p == g->left) {  /* parent is grandparent's left child */
		u = g->right;
		if (u != 0 && u->color == RED) {  /* uncle is red */
			/*	                                        v
				             v                         (g)
				  (n)<-(p)<-[g]->(u)                  /   \
				  / \    \       / \    =>    (n)<-[p]     [u]
				[A] [B]  [C]   [D] [E]        / \    \     / \
				                            [A] [B]  [C] [D] [E]
				or
				                                       v
				            v                   ______(g)
				   v-------[g]->(u)            /         \
				  (p)->(n)      / \    =>    [p]->(n)     [u]
				  /    / \     /   \         /    / \     / \
				[A]  [B] [C] [D]   [E]     [A]  [B] [C] [D] [E]
			*/
			p->color = BLACK;
			u->color = BLACK;
			if (g->parent != 0) {  /* grandparent is not the root */
				g->color = RED;
				n = g;  goto FIXUP_NODE;  /* redo fixup for grandparent */
			}
		} else {  /* uncle is black (possibly null) */
			if (n == p->left) {
				/*	             v                 v
					  (n)<-(p)<-[g]    =>    (n)<-[p]->(g)
					  / \    \    \          / \       / \
					[A] [B]  [C]  [u]      [A] [B]   [C] [u]
				*/
				rbtmap_right_rotate(rbtmap, g);
				p->color = BLACK;
				g->color = RED;
			} else {
				/*	            v
					   v-------[g]                    v               v
					  (p)->(n)   \    =>   (p)<-(n)<-[g]   =>   (p)<-[n]->(g)
					  /    / \    \        / \    \    \        / \       / \
					[A]  [B] [C]  [u]    [A] [B]  [C]  [u]    [A] [B]   [C] [u]
				*/
				rbtmap_left_rotate(rbtmap, p);
				rbtmap_right_rotate(rbtmap, g);
				n->color = BLACK;
				g->color = RED;
			}
		}
	} else {  /* parent is grandparent's right child */
		u = g->left;
		if (u != 0 && u->color == RED) {  /* uncle is red */
			/*	                                   v
				        v                         (g)
				  (u)<-[g]->(p)->(n)             /   \
				  / \       /    / \    =>    [u]     [p]->(n)
				[A] [B]   [C]  [D] [E]        / \     /    / \
				                            [A] [B] [C]  [D] [E]
				or
				                                  v
				         v                       (g)______
				   (u)<-[g]-------v             /         \
				   / \      (n)<-(p)    =>   [u]     (n)<-[p]
				  /   \     / \    \         / \     / \    \
				[A]   [B] [C] [D]  [E]     [A] [B] [C] [D]  [E]
			*/
			p->color = BLACK;
			u->color = BLACK;
			if (g->parent != 0) {  /* grandparent is not the root */
				g->color = RED;
				n = g;  goto FIXUP_NODE;  /* redo fixup for grandparent */
			}
		} else {  /* uncle is black (possibly null) */
			if (n == p->right) {
				/*	   v                           v
					  [g]->(p)->(n)    =>    (g)<-[p]->(n)
					  /    /    / \          / \       / \
					[u]  [A]  [B] [C]      [u] [A]   [B] [C]
				*/
				rbtmap_left_rotate(rbtmap, g);
				p->color = BLACK;
				g->color = RED;
			} else {
				/*	    v
					   [g]-------v          v                         v
					   /   (n)<-(p)   =>   [g]->(n)->(p)   =>   (g)<-[n]->(p)
					  /    / \    \        /    /    / \        / \       / \
					[u]  [A] [B]  [C]    [u]  [A]  [B] [C]    [u] [A]   [B] [C]
				*/
				rbtmap_right_rotate(rbtmap, p);
				rbtmap_left_rotate(rbtmap, g);
				n->color = BLACK;
				g->color = RED;
			}
		}
	}
	assert(rbtmap->root->color == BLACK);
}

static void rbtmap_left_rotate(struct rbtmap *rbtmap, struct rbtmap_node *n)
{
	/*	     v                          v
		    {n}                        {r}
		   /   \                      /   \
		{l}     {r}       =>       {n}     {rr}
		        / \                / \
		     {rl} {rr}           {l} {rl}
	*/
	struct rbtmap_node * const p = (assert(n != 0), n->parent);
	struct rbtmap_node * const r = n->right;
	struct rbtmap_node * const rl = r->left;
	assert(rbtmap != 0);  assert(n->right != 0);
	n->right = rl;
	if (rl != 0)  rl->parent = n;
	r->left = n;
	n->parent = r;
	r->parent = p;
	if (p == 0)
		rbtmap->root = r;
	else if (p->left == n)
		p->left = r;
	else
		p->right = r;
}

static void rbtmap_right_rotate(struct rbtmap *rbtmap, struct rbtmap_node *n)
{
	/*	        v                     v
		       {n}                   {l}
		      /   \                 /   \
		   {l}     {r}    =>    {ll}     {n}
		   / \                           / \
		{ll} {lr}                     {lr} {r}
	*/
	struct rbtmap_node * const p = (assert(n != 0), n->parent);
	struct rbtmap_node * const l = n->left;
	struct rbtmap_node * const lr = l->right;
	assert(rbtmap != 0);  assert(n->left != 0);
	n->left = lr;
	if (lr != 0)  lr->parent = n;
	l->right = n;
	n->parent = l;
	l->parent = p;
	if (p == 0)
		rbtmap->root = l;
	else if (p->left == n)
		p->left = l;
	else
		p->right = l;
}

void *rbtmap_get(struct rbtmap *rbtmap, const void *key)
{
	struct rbtmap_node *n;
	int cmp;
	if (rbtmap == 0 || key == 0)  return 0;
	for (n = rbtmap->root; n != 0; ) {
		cmp = rbtmap->compare(key, n->key);
		if (cmp == 0)      /* key == n->key */
			return n->value;
		else if (cmp < 0)  /* key < n->key */
			n = n->left;
		else               /* key > n->key */
			n = n->right;
	}
	return 0;
}

void rbtmap_delete(struct rbtmap *rbtmap, void *key,
		void (*cleanup)(void *, void *))
{
	struct rbtmap_node *n;
	int cmp;
	if (rbtmap == 0 || key == 0)  return;
	for (n = rbtmap->root; n != 0; ) {
		cmp = rbtmap->compare(key, n->key);
		if (cmp == 0)      /* key == n->key */
			rbtmap_node_delete(rbtmap, n, cleanup);
		else if (cmp < 0)  /* key < n->key */
			n = n->left;
		else               /* key > n->key */
			n = n->right;
	}
}

static void rbtmap_node_delete(struct rbtmap *rbtmap, struct rbtmap_node *n,
		void (*cleanup)(void *, void *))
{
	struct rbtmap_node * const l = (assert(n != 0), n->left);
	struct rbtmap_node * const r = n->right;
	struct rbtmap_node *marked, *repl;
	assert(rbtmap != 0);
	if (cleanup != 0)  cleanup(n->key, n->value);
	if (l == 0) {  /* replace n with its right child (possibly null) */
		marked = n;
		repl = r;
	} else if (r == 0) {  /* replace n with its left child */
		marked = n;
		repl = l;
	} else {  /* logically delete n: copy data, structurally delete succ(n) */
		marked = rbtmap_minimum(rbtmap, r);
		assert(marked != 0);  assert(marked->left == 0);
		n->key = marked->key;
		n->value = marked->value;
		repl = marked->right;  /* possibly null */
	}
	/* replace marked node with its designated replacement */
	rbtmap_transplant(rbtmap, marked, repl);
	if (marked->color == BLACK) {
		if (repl != 0 && repl->color == RED)  /* transfer blackness to repl */
			repl->color = BLACK;
		else if (marked->parent != 0)  /* parent is now unbalanced */
			rbtmap_delete_fixup(rbtmap, marked->parent,
					repl == marked->parent->left);
			/* Note: The test (repl == marked->parent->left), to see whether we
			   deleted a left child, is correct even if repl == 0, because if
			   we are here then we deleted a black node, so the parent must
			   have had black-height at least 2 (including the external nodes),
			   so the deleted node must have had a (non-null) sibling. */
	}
	free(marked);
	--rbtmap->size;
	assert(rbtmap->root == 0 || rbtmap->root->color == BLACK);
}

static void rbtmap_transplant(struct rbtmap *rbtmap, struct rbtmap_node *old,
		struct rbtmap_node *new)
{
	struct rbtmap_node * const p = (assert(old != 0), old->parent);
	assert(rbtmap != 0);
	if (p == 0)
		rbtmap->root = new;
	else if (old == p->left)
		p->left = new;
	else
		p->right = new;
	if (new != 0)
		new->parent = p;
}

static struct rbtmap_node *rbtmap_minimum(struct rbtmap *rbtmap,
		struct rbtmap_node *n)
{
	assert(rbtmap != 0);  assert(n != 0);
	while (n->left != 0)
		n = n->left;
	return n;
}

static struct rbtmap_node *rbtmap_successor(struct rbtmap *rbtmap,
		struct rbtmap_node *n)
{
	struct rbtmap_node *s;
	assert(rbtmap != 0);  assert(n != 0);
	if (n->right != 0)
		return rbtmap_minimum(rbtmap, n->right);
	s = n->parent;
	while (s != 0 && n == s->right) {
		n = s;
		s = s->parent;
	}
	return s;
}

/*	Preconditions:
	If (left_fixup), then
	  *	n->left is either null or black;
	  *	the left subtree of n has black-height one less than its right subtree;
	  *	therefore, n->right is not null.
	Otherwise
	  *	n->right is either null or black;
	  *	the right subtree of n has black-height one less than its left subtree;
	  *	therefore, n->left is not null.
*/
static void rbtmap_delete_fixup(struct rbtmap *rbtmap, struct rbtmap_node *n,
		int left_fixup)
{
	struct rbtmap_node *l, *r, *rl, *rr, *ll, *lr;
	assert(rbtmap != 0);  assert(n != 0);
	if (!left_fixup)  goto RIGHT_FIXUP;
LEFT_FIXUP:
	assert(n->left == 0 || n->left->color == BLACK);  assert(n->right != 0);
	l = n->left;  r = n->right;  rl = r->left;  rr = r->right;
	if (r->color == RED) {
		/*	   v                         v
			  [n]->(r)             (n)<-[r]
			  /    / \      =>     / \    \
			[l] [rl] [rr]        [l] [rl] [rr]
		*/
		rbtmap_left_rotate(rbtmap, n);
		r->color = BLACK;
		n->color = RED;
		assert(rl != 0);  /* because bh(r) was strictly greater than bh(l) */
		r = rl;
		rl = r->left;
		rr = r->right;
		/* fall through to following cases */
	}
	if (rl != 0 && rl->color == RED) {
		/*	   v                         v
			  {n}______                 {rl} : same color as n was previously
			  /        \               /    \
			[l]  (rl)<-[r]    =>    [n]      [r]
			     /  \    \          / \      / \
			   [A]  [B]  {C}      [l] [A]  [B] {C}
		*/
		rbtmap_right_rotate(rbtmap, r);
		rbtmap_left_rotate(rbtmap, n);
		rl->color = n->color;
		n->color = BLACK;
	} else if (n->color == RED) {
		/*	     v
			    (n)                    v
			   /   \             (n)<-[r]
			[l]     [r]    =>    / \    \
			        / \        [l] [rl] {A}
			     [rl] {A}
		*/
		rbtmap_left_rotate(rbtmap, n);
	} else if (rr != 0 && rr->color == RED) {
		/*	     v                         v
			    [n]                       [r]
			   /   \                     /   \
			[l]     [r]->(rr)   =>    [n]     [rr]
			        /                 / \
			     [rl]               [l] [rl]
		*/
		rbtmap_left_rotate(rbtmap, n);
		rr->color = BLACK;
	} else {
		/*	     v
			    [n]                v
			   /   \              [n]->(r)
			[l]     [r]     =>    /    / \
			        / \         [l] [rl] [rr]
			     [rl] [rr]
		*/
		r->color = RED;
		rr = n;  /* save the value of n; ignore the variable name "rr" here */
		n = n->parent;
		if (n != 0) {  /* do fixup on the parent */
			if (rr == n->left)
				goto LEFT_FIXUP;
			else
				goto RIGHT_FIXUP;
		}
	}
	return;
RIGHT_FIXUP:
	assert(n->right == 0 || n->right->color == BLACK);  assert(n->left != 0);
	l = n->left;  r = n->right;  ll = l->left;  lr = l->right;
	if (l->color == RED) {
		/*	         v               v
			   (l)<-[n]             [l]->(n)
			   / \    \     =>      /    / \
			[ll] [lr] [r]        [ll] [lr] [r]
		*/
		rbtmap_right_rotate(rbtmap, n);
		l->color = BLACK;
		n->color = RED;
		assert(lr != 0);  /* because bh(l) was strictly greater than bh(r) */
		l = lr;
		ll = l->left;
		lr = l->right;
		/* fall through to following cases */
	}
	if (lr != 0 && lr->color == RED) {
		/*	            v                  v
			     ______{n}                {lr} : same color as n was previously
			    /        \               /    \
			  [l]->(lr)  [r]    =>    [l]      [n]
			  /    /  \               / \      / \
			{A}  [B]  [C]           {A} [B]  [C] [r]
		*/
		rbtmap_left_rotate(rbtmap, l);
		rbtmap_right_rotate(rbtmap, n);
		lr->color = n->color;
		n->color = BLACK;
	} else if (n->color == RED) {
		/*	       v
			      (n)                v
			     /   \              [l]->(n)
			  [l]     [r]    =>     /    / \
			  / \                 {A} [lr] [r]
			{A} [lr]
		*/
		rbtmap_right_rotate(rbtmap, n);
	} else if (ll != 0 && ll->color == RED) {
		/*	           v                     v
			          [n]                   [l]
			         /   \                 /   \
			(ll)<-[l]     [r]    =>    [ll]     [n]
			        \                           / \
			        [lr]                     [lr] [r]
		*/
		rbtmap_right_rotate(rbtmap, n);
		ll->color = BLACK;
	} else {
		/*	        v
			       [n]                      v
			      /   \               (l)<-[n]
			   [l]     [r]    =>      / \    \
			   / \                 [ll] [lr] [r]
			[ll] [lr]
		*/
		l->color = RED;
		ll = n;  /* save the value of n; ignore the variable name "ll" here */
		n = n->parent;
		if (n != 0) {  /* do fixup on the parent */
			if (ll == n->left)
				goto LEFT_FIXUP;
			else
				goto RIGHT_FIXUP;
		}
	}
}

int rbtmap_size(struct rbtmap *rbtmap)
{
	if (rbtmap == 0)  return -1;
	return rbtmap->size;
}

struct rbtmap_iter *rbtmap_iterator(struct rbtmap *rbtmap)
{
	struct rbtmap_iter *iter;
	if (rbtmap == 0)  return 0;
	iter = malloc(sizeof(*iter));
	if (iter == 0) { fprintf(stderr, "Out of memory\n"); exit(EXIT_FAILURE); }
	iter->rbtmap = rbtmap;
	if (rbtmap->root == 0)
		iter->next_node = 0;
	else
		iter->next_node = rbtmap_minimum(rbtmap, rbtmap->root);
	return iter;
}

int rbtmap_has_next(struct rbtmap_iter *iter)
{
	return (iter != 0 && iter->next_node != 0);
}

struct rbtmap_entry rbtmap_next(struct rbtmap_iter *iter)
{
	struct rbtmap_entry entry;
	if (iter == 0 || iter->next_node == 0) {
		entry.key = 0;
		entry.value = 0;
	} else {
		entry.key = iter->next_node->key;
		entry.value = iter->next_node->value;
		iter->next_node = rbtmap_successor(iter->rbtmap, iter->next_node);
	}
	return entry;
}

void rbtmap_iterator_free(struct rbtmap_iter *iter)
{
	free(iter);
}
