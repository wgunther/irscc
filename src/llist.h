#ifndef IRSCC_LLIST_H
#define IRSCC_LLIST_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <stddef.h>
#include "formdesc.h"

struct linked_list *ll_new(void);
void ll_free(struct linked_list *L);

size_t ll_size(struct linked_list *L);
void ll_clear(struct linked_list *L);

void ll_push_int(struct linked_list *L, int item);
void ll_push_float(struct linked_list *L, float item);
void ll_push_lineno(struct linked_list *L, struct line_number item);
void ll_push_ptr(struct linked_list *L, void *item);

int ll_pop_int(struct linked_list *L);
float ll_pop_float(struct linked_list *L);
struct line_number ll_pop_lineno(struct linked_list *L);
void *ll_pop_ptr(struct linked_list *L);

void ll_unshift_int(struct linked_list *L, int item);
void ll_unshift_float(struct linked_list *L, float item);
void ll_unshift_lineno(struct linked_list *L, struct line_number item);
void ll_unshift_ptr(struct linked_list *L, void *item);

int ll_shift_int(struct linked_list *L);
float ll_shift_float(struct linked_list *L);
struct line_number ll_shift_lineno(struct linked_list *L);
void *ll_shift_ptr(struct linked_list *L);

int ll_head_int(struct linked_list *L);
float ll_head_float(struct linked_list *L);
struct line_number ll_head_lineno(struct linked_list *L);
void *ll_head_ptr(struct linked_list *L);

int ll_tail_int(struct linked_list *L);
float ll_tail_float(struct linked_list *L);
struct line_number ll_tail_lineno(struct linked_list *L);
void *ll_tail_ptr(struct linked_list *L);

struct linked_list *ll_copy(struct linked_list *L);
void ll_reverse(struct linked_list *L);
void ll_concat_and_free(struct linked_list *master, struct linked_list *end);

/* Iterators */

struct ll_iter *ll_iterator(struct linked_list *L);
struct ll_iter *ll_reverse_iterator(struct linked_list *L);
void ll_iterator_free(struct ll_iter *iter);

int ll_has_next(struct ll_iter *iter);

int ll_next_int(struct ll_iter *iter);
float ll_next_float(struct ll_iter *iter);
struct line_number ll_next_lineno(struct ll_iter *iter);
void *ll_next_ptr(struct ll_iter *iter);

int ll_this_int(struct ll_iter *iter);
float ll_this_float(struct ll_iter *iter);
struct line_number ll_this_lineno(struct ll_iter *iter);
void *ll_this_ptr(struct ll_iter *iter);

void ll_insert_ptr_before(struct ll_iter *iter, void *item);

void ll_remove(struct ll_iter *iter);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_LLIST_H */
