#ifndef IRSCC_ASSEMBLE_H
#define IRSCC_ASSEMBLE_H

#include "formdesc.h"
#include "llist.h"

#if defined(__cplusplus)
extern "C" {
#endif

/* argument is a linked list of pointers to form_desc */
void assemble(struct linked_list *L);

void print_lineno(struct line_number lineno, const struct form_desc *form);
void print_assembled_line_list(const char *prefix, struct linked_list *L);

#if defined(__cplusplus)
}
#endif

#endif  /* IRSCC_ASSEMBLE_H */
