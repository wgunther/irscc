int gcd(int a, int b)
{
	int d;
	if (b < a)
		d = gcd(b, a);
	else {
		int r = b % a;
		if (r)
			d = gcd(r, a);
		else
			d = a;
	}
	return d;
}

int main()
{
	int m = 45, n = 105;
	int g = gcd(m, n);
	int leastfac = 0;
	int test = 2;
	while (g >= test) {
		if (g % test == 0) {
			leastfac = test;
			test = g;
		}
		++test;
	}
	return leastfac;
}
